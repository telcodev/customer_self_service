<?php

include ('dbFunctions.php');
$operator = new DatabaseFunctionsClass();

if (isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['hash']) && !empty($_GET['hash'])) {

    $res = $operator->verifyAccount($_GET['email'], $_GET['hash']);
    $res ? header("location: login.html?account_verification_success") :  header("location: login.html?account_verification_failed"); // Redirecting To Other Page

} else {
    // Invalid approach
    echo '<div class="statusmsg">Invalid approach, please use the link that has been send to your email.</div>';
}
