<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}






// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];


// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}




//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);







//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);







//get the further's group info from ibs in order to get the data left

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsDataInfo = json_decode($json, true);







//get user info from our db

$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];
?>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <link rel="stylesheet" href="../../plugins/bower_components/dropify/dist/css/dropify.min.css">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>  

    <body class="fix-sidebar">
        <!-- Preloader            -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <?php
            require './_nav.php';
            ?>
            <!-- Page Content -->
            <div id="page-wrapper">

                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Upload Document</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="home.php">Home</a></li>
                                <li class="active">Upload Document</li>
                            </ol>
                        </div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- page content begins here -->


                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Document Upload</h3>
                            <p class="text-muted m-b-30 font-13"> Fill in the details about your document</p>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action = 'upload_doc_redirect.php' method = 'post' enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Document Title</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter the name here." name='doc_title'>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Document Description</label>
                                            <input type ='text' class="form-control" id="exampleInputEmail1" placeholder="Enter the desciption here." name = 'doc_description'>
                                        </div>
                                        <div class = 'form-group'>
                                            <label for="exampleInputEmail1">Choose File</label>
                                            <input type="file" id="input-file-now" class="dropify" name ='doc' /> </div>


                                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                        <button class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- page content ends here -->
                    <!-- .right-side bar -->

                    <!-- /.right-side bar -->
                </div>
                <!-- /.container-fluid -->


                 
                
                <?php
                    require './_notifyier.php'; require './_footer.php';
                    ?>

            </div>



            <!-- /#wrapper -->


            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <script src="../../plugins/bower_components/toast-master/js/jquery.toast.js"></script>
            <script src="../js/toastr.js"></script>

          

            <!-- Sweet-Alert  -->
            <script src="../../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
            <script src="../../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>


            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <!-- jQuery file upload -->
            <script src="../../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
            <script>
                $(document).ready(function () {
                    // Basic
                    $('.dropify').dropify();

                    // Translated
                    $('.dropify-fr').dropify({
                        messages: {
                            default: 'Glissez-déposez un fichier ici ou cliquez',
                            replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                            remove: 'Supprimer',
                            error: 'Désolé, le fichier trop volumineux'
                        }
                    });

                    // Used events
                    var drEvent = $('#input-file-events').dropify();

                    drEvent.on('dropify.beforeClear', function (event, element) {
                        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                    });

                    drEvent.on('dropify.afterClear', function (event, element) {
                        alert('File deleted');
                    });

                    drEvent.on('dropify.errors', function (event, element) {
                        console.log('Has Errors');
                    });

                    var drDestroy = $('#input-file-to-destroy').dropify();
                    drDestroy = drDestroy.data('dropify')
                    $('#toggleDropify').on('click', function (e) {
                        e.preventDefault();
                        if (drDestroy.isDropified()) {
                            drDestroy.destroy();
                        } else {
                            drDestroy.init();
                        }
                    })
                });
            </script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

        </div>
    </body>
</html>
