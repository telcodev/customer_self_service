<?php

$target_dir = "../documents/";
$target_file = $target_dir . basename($_FILES["doc"]["name"]);
$uploadOk = 1;
$docFileType = pathinfo($target_file, PATHINFO_EXTENSION);
$name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["doc"]['name']);
$description = $_POST['doc_description'];
$title = $_POST['doc_title'];
$date = date('Y-m-d');



if (isset($title)) {
    // ensure a safe filename
    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $title);
}

$_FILES['doc']['name'] = $name;
$target_file = $target_dir . basename($_FILES["doc"]["name"] . '.' . $docFileType);
$halfpath = substr($target_file, 2);
$absolutePath = 'https://selfservice.telco.co.zw/portal' . $halfpath;

if (file_exists($target_file)) {
    header('location: home.php?notify=80');
    $uploadOk = 0;
}

if ($_FILES["doc"]["size"] > 25000000) {
    header('location: home.php?notify=80');
    $uploadOk = 0;
}


// Allow certain file formats
if ($docFileType != "doc" && $docFileType != "pdf" && $docFileType != "ods" && $docFileType != "xls") {
    header('location: home.php?notify=80');
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {

    header('location: home.php?notify=80');

// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["doc"]["tmp_name"], $target_file)) {

        include ("../dbconnect.php");
// Selecting Database
        $db = mysql_select_db($dbname, $conn);
        $sql = "INSERT INTO documents (doc_name, doc_description, doc_path, user_id, status, date_uploaded, uploaded_by) VALUES ('" . $name . "','" . $description . "','" . $absolutePath . "','','new','$date','" . $_SESSION['user_id'] . "')";
        mysql_query($sql);

        header('location: home.php?notify=83');
    } else {

        header('location: home.php?notify=80');
    }
}