<?php

include ('../dbFunctions.php');
$operator = new DatabaseFunctionsClass();

session_start();

$operator->destroySession($_SESSION['session_id']);



session_unset();



session_destroy();



header('Refresh: 0; URL = ../login.html');
exit();
?>