<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {


// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}






// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];


// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}




//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);







//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);







//get the further's group info from ibs in order to get the data left

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsDataInfo = json_decode($json, true);







//get user info from our db

$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];
?>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">  
        <link href="../../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fix-sidebar">
        <!-- Preloader            -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <?php
            require './_nav.php';
            ?>
            <!-- Page Content -->
            <div id="page-wrapper">

                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Manage Clients</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                            <ol class="breadcrumb">  
                                <li><a href="home.php">Home</a></li>
                                <li class="active">Manage Clients</li>
                            </ol>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- page content begins here -->

                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Data Table</h3><span>

                                <button class = 'btn btn-secondary pull-right' onclick="getTableContent();">Export to Excel</button>

                            </span>
                            <p class="text-muted m-b-30">Here are all the clients</p>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="display:none;">USER ID</th>
                                            <th> <input id="checkbox-mark-all" type="checkbox"> </th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>Email</th>
                                            <th>Account Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $operator->populateUsersOnManageClientsPage(); ?>   

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                    <!-- page content ends here -->
                    <!-- .right-side bar -->

                    <!-- /.right-side bar -->
                </div>
                <!-- /.container-fluid -->


                 

                
                <?php
                    require './_notifyier.php'; require './_footer.php';
                    ?>
            </div>


            <!-- /#wrapper -->


            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <script src="../../plugins/bower_components/toast-master/js/jquery.toast.js"></script>
            <script src="../js/toastr.js"></script>



            <!-- Sweet-Alert  -->
            <script src="../../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
            <script src="../../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js">
            </script>

            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <script src="../../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
            <script>
                                    $(document).ready(function () {
                                        $('#myTable').DataTable();
                                        $(document).ready(function () {
                                            var table = $('#example').DataTable({
                                                "columnDefs": [
                                                    {"visible": false, "targets": 2}
                                                ],
                                                "order": [[2, 'asc']],
                                                "displayLength": 25,
                                                "drawCallback": function (settings) {
                                                    var api = this.api();
                                                    var rows = api.rows({page: 'current'}).nodes();
                                                    var last = null;

                                                    api.column(2, {page: 'current'}).data().each(function (group, i) {
                                                        if (last !== group) {
                                                            $(rows).eq(i).before(
                                                                    '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                                                                    );

                                                            last = group;
                                                        }
                                                    });
                                                }
                                            });

                                            // Order by the grouping
                                            $('#example tbody').on('click', 'tr.group', function () {
                                                var currentOrder = table.order()[0];
                                                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                                                    table.order([2, 'desc']).draw();
                                                } else {
                                                    table.order([2, 'asc']).draw();
                                                }
                                            });
                                        });
                                    });
            </script>

            <script type = 'text/javascript' >

                function getTableContent() {
                    var table = document.getElementById('myTable').outerHTML;
                    console.log(table);

                    var form = document.createElement("form");
                    form.setAttribute("method", "post");
                    form.setAttribute("action", "export_clients_redirect.php");

                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", "table");
                    hiddenField.setAttribute("value", table);
                    form.appendChild(hiddenField);
                    document.body.appendChild(form);

                    form.submit();


                }

            </script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

        </div>
    </body>
</html>
