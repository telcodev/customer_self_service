<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {

        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}

// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];


// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}

//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);

//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);


//get user info from our db

$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];

if (isset($_GET['replyto'])) {

    $repliedMsgPortalInfo = $operator->getMsgInfoByMsgID($_GET['replyto']);
    $repliedtoSenderInfo = $operator->getUserInfoByUserID($repliedMsgPortalInfo[0]['sender_user_id']);
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- wysihtml5 CSS -->
        <link rel="stylesheet" href="../../plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
        <!-- Dropzone css -->
        <link href="../../plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fix-sidebar fix-header">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <?php
            require './_nav.php';
            ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Compose Mail</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="home.php">Home</a></li>
                                <li class="active">Compose Mail</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- row -->
                    <div class="row">
                        <!-- Left sidebar -->
                        <div class="col-md-12">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3  col-sm-4 col-xs-12 inbox-panel">
                                        <div>
                                            <a href="compose.php" class="btn btn-custom btn-block waves-effect waves-light">Compose</a>
                                            <div class="list-group mail-list m-t-20">
                                                <a href="inbox.php" class="list-group-item ">Inbox <span class="label label-rouded label-success pull-right"><?php echo count($operator->getMessagesByUserID($_SESSION['user_id'])); ?></span></a>
                                                <a href="sent.php" class="list-group-item">Sent <span class="label label-rouded label-primary pull-right"><?php echo count($operator->getSentMessagesByUserID($_SESSION['user_id'])); ?></span></a>
                                                <a href="trash.php" class="list-group-item">Trash <span class="label label-rouded label-default pull-right"><?php echo count($operator->getTrashMessagesByUserID($_SESSION['user_id'])); ?></span></a>
                                            </div>
                                            <h3 class="panel-title m-t-40 m-b-0">Labels</h3>
                                            <hr class="m-t-5">
                                            <div class="list-group b-0 mail-list">
                                                <a onclick="setMessageTo('sales')" class="list-group-item"><span class="fa fa-circle text-info m-r-10"></span>Sales</a>
                                                <a onclick="setMessageTo('billing')" class="list-group-item"><span class="fa fa-circle text-warning m-r-10"></span>Billing</a>
                                                <a onclick="setMessageTo('support')" class="list-group-item"><span class="fa fa-circle text-purple m-r-10"></span>Support</a>
                                                <a onclick="setMessageTo('fault')" class="list-group-item"><span class="fa fa-circle text-danger m-r-10"></span>Faults</a>
                                                <a onclick="setMessageTo('general')" class="list-group-item"><span class="fa fa-circle text-success m-r-10"></span>General</a>
                                            </div>
                                        </div>
                                    </div>
                                    <form action = "sendmessageredirect.php" method = "post">
                                        <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                                            <h3 class="box-title">Compose New Message</h3>
                                            <div class="form-group">
                                                <div class = "col-lg-10">
                                                    <input  class="form-control" required = "required" name  = "to" placeholder="To: (enter the username or phone number)"   <?php
                                                    if (isset($_GET['replyto'])) {
                                                        echo 'value = "' . $repliedtoSenderInfo[0]['username'] . '" readonly = "readonly"';
                                                    }
                                                    ?>  ></div>
                                                <div class = "col-lg-2">
                                                    <select name ="msg_category" id="msg_category" class="form-control" >
                                                        <option value = "general" >General</option>
                                                        <option value = "sales" >Sales</option>
                                                        <option value = "billing" >Billing</option>
                                                        <option value = "support" >Support</option>
                                                        <option value = "fault" >Fault</option>
                                                    </select></div>
                                                <br>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" required = "required" name  = "subject" placeholder="Subject:"
                                                <?php
                                                if (isset($_GET['replyto'])) {
                                                    echo 'value = "RE: ' . $repliedMsgPortalInfo[0]['msg_subject'] . '" readonly = "readonly"';
                                                }
                                                ?> 
                                                       >
                                            </div>
                                            <div class="form-group">
                                                <textarea class="textarea_editor form-control" required = "required" rows="15" name = "message" placeholder="Enter text ..."></textarea>
                                            </div>

                                            <hr>
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                                            <a href = "inbox.php"><button class="btn btn-default"><i class="fa fa-times"></i> Discard</button></a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <!-- .right-sidebar -->

                    <!-- /.right-sidebar -->
                </div>
                <!-- /.container-fluid -->
                 
                <?php
                    require './_notifyier.php'; require './_footer.php';
                    ?>
            </div>
            <!-- /#page-wrapper -->
            <!-- /#wrapper -->

            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <script src="../../plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
            <script src="../../plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
            <script src="../../plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
            <script>
                                                    $(document).ready(function () {

                                                        $('.textarea_editor').wysihtml5();

                                                    });
            </script>
            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
            <script type="text/javascript">
                                                    function setMessageTo(type) {
                                                        document.getElementById('msg_category').value = type;
                                                    }
            </script>
        </div>
    </body>
</html>
