<?php

include '../dbFunctions.php';
$operator = new DatabaseFunctionsClass();

$token = 't3lc0zss';
$ibs_id = $_POST['ibsId'];
$su_user_id = $_POST['userId'];
$topup_type = $_POST['radio'];
$comment = $_POST['comment'];
$amount = round($_POST['amount'], 2);



switch ($topup_type) {

    case 'ADD':

        $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';

        $data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => $amount, 'topup_type' => $topup_type, 'comment' => $comment);

        $json = $operator->CallAPI('POST', $service_address, $data);

        $result = json_decode($json, true);



        if ($result[0] == 'true') {

            header('location: client_info.php?user_id=' . $su_user_id . '&notify=62');
        } else {

            header('location: client_info.php?user_id=' . $su_user_id . '&notify=63');
        }


        break;




    case 'SET':

        $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_set_user_deposit.php';

        $data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => $amount, 'topup_type' => $topup_type, 'comment' => $comment);

        $json = $operator->CallAPI('POST', $service_address, $data);

        $result = json_decode($json, true);



        if ($result[0] == 'true') {

            header('location: client_info.php?user_id=' . $su_user_id . '&notify=64');
        } else {

            header('location: client_info.php?user_id=' . $su_user_id . '&notify=65');
        }



        break;

    default:

        break;
}