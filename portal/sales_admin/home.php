<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}






// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];



// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}



//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);







//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);







//get the further's group info from ibs in order to get the data left

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsDataInfo = json_decode($json, true);







//get user info from our db

$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];
?>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>  

    <body class="fix-sidebar">
        <!-- Preloader            -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <?php
            require './_nav.php';
            ?>
            <!-- Page Content -->
            <div id="page-wrapper">

                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Home</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="logoutredirect.php">Log Out</a></li>
                                <li class="active">Home Page</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- page content begins here -->



                    <!--                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12"> 
                                            <div class="white-box">
                                                <div class="user-bg"> <img width="100%"  src="<?php echo $profileimageurl; ?>" alt="user" >
                                                    <div class="overlay-box">
                                                        <div class="user-content"> <a href="profile.php"><img id = "profileImageDashboard" alt="img" class="thumb-lg img-circle" src="<?php echo $profileimageurl; ?>"></a>
                                                            <h4 class="text-white" id = "nameDashboard"><?php echo $name . " " . $surname ?></h4>
                    
                                                            <h5 class="text-white"  id = "usernameDashboard"><?php echo $username; ?></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="user-btm-box">
                                                    <div class="stats-row col-md-12 m-t-20 m-b-0 text-center">
                                                        <div class="stat-item">
                                                            <h6>Contact info</h6>
                                                            <hr/>
                                                            <b id = "phoneDashboard"><i class="ti-mobile"></i><?php echo $phone; ?></b></div>
                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                    
                                         <div class="col-md-6 col-lg-8  col-xs-12 col-sm-12">
                                            <div class="white-box text-center bg-purple">
                                                <h1 class="text-white counter">VIEW FAULT REPORTS</h1>
                                                <p class="text-white">counters</p>
                                            </div>
                                             
                    
                                            <div class="white-box text-center bg-purple">
                                                <h1 class="text-white counter">VIEW ALL CLIENT TRANSACTIONS</h1>
                                                <p class="text-white">counters</p>
                                            </div>     
                                             
                                        </div>-->



                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="row">
                            <div class="col-md-12">                                
                                <h3>Client Management Functions</h3>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <a href = 'add_client.php' ><div class="white-box">
                                    <h3 class="box-title">ADD NEW CLIENT</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-people text-info"></i></li>
                                        <li class="text-right"><span class="counter">+1</span></li>
                                    </ul>
                                </div></a>
                        </div>        


                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <a href = 'manage_clients.php'> <div class="white-box">
                                    <h3 class="box-title">MANAGE CLIENTS</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-people text-info"></i></li>
                                        <li class="text-right"><span class="counter"><?php echo $operator->getNumberOfUsers(); ?></span></li>
                                    </ul>
                                </div></a>
                        </div>

                    </div>




                    <div class="col-md-6 col-lg-4  col-xs-12 col-sm-12">

                        <div class="row">
                            <div class="col-md-12">

                                <h3>Documents</h3>
                            </div>
                        </div>

                        <div class="white-box text-center bg-purple">
                            <a href = 'upload_doc.php' ><h1 class="text-white counter">UPLOAD DOCUMENTS</h1>
                                <p class="text-white">counters</p></a>
                        </div>

                        <div class="white-box text-center bg-purple">
                            <a href = 'manage_docs.php'><h1 class="text-white counter">MANAGE DOCUMENTS</h1>
                                <p class="text-white">counters</p></a>
                        </div>

                    </div>


                    <div class="col-md-6 col-lg-4  col-xs-12 col-sm-12">

                        <div class="row">
                            <div class="col-md-12">

                                <h3>Advertising</h3>
                            </div>
                        </div>

                        <div class="white-box text-center bg-purple">
                            <a href = 'upload_ad.php'><h1 class="text-white counter">UPLOAD ADVERTS</h1>
                                <p class="text-white">counters</p></a>
                        </div>

                        <div class="white-box text-center bg-purple">
                            <a href = 'manage_ads.php'><h1 class="text-white counter">MANAGE ADVERTS</h1>
                                <p class="text-white">counters</p></a>
                        </div>

                    </div>




                    <div class="col-md-6 col-lg-4  col-xs-12 col-sm-12">

                        <div class="row">
                            <div class="col-md-12">

                                <h3>Frequently Asked Questions</h3>
                            </div>
                        </div>

                        <div class="white-box text-center bg-purple">
                            <a href = 'upload_faq.php'><h1 class="text-white counter">UPLOAD NEW FAQ</h1>
                                <p class="text-white">counters</p></a>
                        </div>

                        <div class="white-box text-center bg-purple">
                            <a href = 'manage_faqs.php'><h1 class="text-white counter">MANAGE ALL FAQ's</h1>
                                <p class="text-white">counters</p></a>
                        </div>

                    </div>






                    <!-- page content ends here -->
                    <!-- .right-side bar -->

                    <!-- /.right-side bar -->
                </div>
                <!-- /.container-fluid -->


                 
                <?php
                    require './_notifyier.php'; require './_footer.php';
                    ?>

            </div>



            <!-- /#wrapper -->

            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <script src="../../plugins/bower_components/toast-master/js/jquery.toast.js"></script>
            <script src="../js/toastr.js"></script>

            <!-- Sweet-Alert  -->
            <script src="../../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
            <script src="../../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>


            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

        </div>
    </body>
</html>
