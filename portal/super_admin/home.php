<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}






// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];







//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);







//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);







//get the further's group info from ibs in order to get the data left

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsDataInfo = json_decode($json, true);







//get user info from our db

$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];
?>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!--Owl carousel CSS -->
        <link href="../../plugins/bower_components/owl.carousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
        <link href="../../plugins/bower_components/owl.carousel/owl.theme.default.css" rel="stylesheet" type="text/css" />
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fix-sidebar">
        <!-- Preloader            -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <!-- Top Navigation -->
            <nav class="navbar navbar-default navbar-static-top m-b-0">
                <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                    <div class="top-left-part"><a class="logo" href="home.php"><b><img src="../../plugins/images/eliteadmin-logo.png" alt="home" /></b><span class="hidden-xs"><img src="../../plugins/images/eliteadmin-text.png" alt="home" /></span></a></div>
                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                        <li>
                            <form role="search" class="app-search hidden-xs">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>  

                            </form>
                        </li>
                    </ul>
                    <ul class="nav navbar-top-links navbar-right pull-right">
                        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>

                                <?php
                                $countMessages = $operator->phpGetNumberOfUnreadMessages($user_id);

                                if ($countMessages > 0) {

                                    echo '<div class="notify"><span class="heartbit"></span><span class="point"></span></div>';
                                }
                                ?>

                            </a>
                            <ul class="dropdown-menu mailbox animated bounceInDown">
                                <li>
                                    <div class="drop-title">You have <?php echo $operator->phpGetNumberOfUnreadMessages($user_id); ?> new messages</div>
                                </li>
                                <li>
                                    <div class="message-center">

                                        <!--add message rows here-->

                                        <?php
                                        $operator->phpPopulateMessagesOnHomePage($user_id);
                                        ?>

                                        <!--stop adding message row here-->



                                    </div>
                                </li>
                                <li> <a class="text-center" href="inbox.php"> <strong>See all messages</strong> <i class="fa fa-angle-right"></i> </a></li>
                            </ul>



                            <!-- /.dropdown-messages ends here -->
                        </li>
                        <!-- /.dropdown documents starts here-->



                        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-doc"></i>
                                <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                            </a>

                            <ul class="dropdown-menu dropdown-tasks animated bounceInDown">


                                <!--   start adding documents rows here -->

                                <?php
                                $operator->populateDocumentsOnHomePage();
                                ?>

                                <!--   stop adding documents rows here -->


                                <li> <a class="text-center" href="#"> <strong>See All Documents</strong> <i class="fa fa-angle-right"></i> </a> </li>
                            </ul>
                            <!-- /.dropdown-tasks -->
                        </li>
                        <!-- /.dropdown documents ends here -->


                        <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img id = "profileImageTopBar" src="../../plugins/images/users/default_profile_image.png" alt="user-img" width="36" class="img-circle"><b class="hidden-xs" id = "nameTopBar"><?php echo $name; ?></b> </a>
                            <ul class="dropdown-menu dropdown-user animated flipInY">
                                <li><a href="profile.php"><i class="ti-user"></i> My Profile</a></li>
                                <li><a href="purchase-history.php"><i class="ti-wallet"></i> Purchase History </a></li>
                                <li><a href="inbox.php"><i class="ti-email"></i> Inbox</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="profile.php"><i class="ti-settings"></i> Account Setting</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="../logoutredirect.php"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>
                        <li class="right-side-toggle"> <a class="waves-effect waves-light" href="profile.php"><i class="ti-settings"></i></a></li>
                        <!-- /.dropdown -->
                    </ul>
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>
            <!-- End Top Navigation -->
            <!-- Left navbar-header -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                            <!-- input-group -->
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                                </span> </div>
                            <!-- /input-group -->
                        </li>
                        <li class="user-pro"> <a href="#" class="waves-effect"><img id = "profileImageSideBar" src="../../plugins/images/users/default_profile_image.png" alt="user-img"  class="img-circle"> <span class="hide-menu" id = "nameSideBar"><?php echo $name . " " . $surname; ?><span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="profile.php"><i class="ti-user"></i> My Profile</a></li>
                                <li><a href="purchase-history.php"><i class="ti-wallet"></i> Purchase History</a></li>
                                <li><a href="inbox.php"><i class="ti-email"></i> Inbox</a></li>
                                <li><a href="profile.php"><i class="ti-settings"></i> Account Setting</a></li>
                                <li><a href="../logoutredirect.php"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </li>
                        <li class="nav-small-cap m-t-10">--- Main Menu</li>
                        <li> <a href="index.html" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right">5</span></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="home.php">Home</a> </li>
                                <!-- add option in the side bar menu to activate account if it is blocked for some reason -->              
                                <?php
                                if (strcmp($ibsUserInfo['attrs']['lock'], 'Blocked') == 0) {
                                    echo '
  <li> <a href="index2.html">Activate Account</a> </li>
  ';
                                }

//add option in side bar menu if the credit is 0 to allow them to reactive their aacount


                                if ($ibsUserInfo['basic_info']['credit'] <= 0 || strcmp($ibsUserInfo['basic_info']['status'], 'Recharged') == 0) {
                                    echo '
  <li> <a href="#" data-toggle="modal" data-target=".bs-example-modal-sm"  >Renew Package</a> </li>
  ';
                                }
                                ?>



                                <li> <a href = "#" data-toggle="modal" data-target=".bs-example-modal-lg">Credit Purchase</a> </li>
                                <li> <a href="purchase-history.php">Purchase History</a> </li>
                                <li> <a href="compose.php?replyto=100">Report a Fault</a> </li>
                            </ul>
                        </li>
                        <li><a href="inbox.html" class="waves-effect"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Messaging <span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="compose.php">Compose</a></li>
                                <li> <a href="inbox.php">Inbox</a></li>
                                <li> <a href="compose.php?replyto=100">Contact Support</a></li>
                            </ul>
                        </li>


                        <li><a href="../logoutredirect.php" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
                        <li class="nav-small-cap">--- Support</li>
                        <li><a href="documentation.php" class="waves-effect"><i class="fa fa-circle-o text-danger"></i> <span class="hide-menu">Documentation</span></a></li>
                        <li><a href="compose.php?replyto=100" class="waves-effect"><i class="fa fa-circle-o text-info"></i> <span class="hide-menu">Contact Support</span></a></li>
                        <li><a href="faq.php" class="waves-effect"><i class="fa fa-circle-o text-success"></i> <span class="hide-menu">Faqs</span></a></li>
                    </ul>
                </div>
            </div>
            <!-- Left navbar-header end -->
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Home</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="../logoutredirect.php">Log Out</a></li>
                                <li class="active">Home Page</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- page content begins here -->



                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">



                        <div class="white-box">
                            <div class="user-bg"> <img width="100%"  src="<?php echo $profileimageurl; ?>" alt="user" >
                                <div class="overlay-box">
                                    <div class="user-content"> <a href="profile.php"><img id = "profileImageDashboard" alt="img" class="thumb-lg img-circle" src="<?php echo $profileimageurl; ?>"></a>
                                        <h4 class="text-white" id = "nameDashboard"><?php echo $name . " " . $surname ?></h4>

                                        <h5 class="text-white"  id = "usernameDashboard"><?php echo $username; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="stats-row col-md-12 m-t-20 m-b-0 text-center">
                                    <div class="stat-item">
                                        <h6>Contact info</h6>
                                        <hr/>
                                        <b id = "phoneDashboard"><i class="ti-mobile"></i><?php echo $phone; ?></b></div>

                                </div>
                            </div>
                        </div>
                    </div>





                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Package you're on</h3>
                            <div class="text-right"> <span class="text-muted"><?php
                                    if (strcmp($ibsUserInfo['basic_info']['status'], 'Recharged') == 0)
                                        $additionalText = "(Out-of-Bundle)";
                                    if (strcmp($ibsUserInfo['basic_info']['status'], 'Package') == 0)
                                        $additionalText = "(In-Bundle)";

                                    echo "<b>" . $ibsUserInfo['basic_info']['group_name'] . "</b> " . $additionalText;
                                    ?></span>
                                <h1><sup><i class="fa fa-money text-success"> </i></sup>$<?php echo round($ibsUserInfo['basic_info']['credit'], 2); ?></h1>
                            </div>
                            <span class="text-success"><?php
                                //  echo $info['basic_info']['credit']/$groupinfo['group_info'][1]['raw_attrs']['group_credit']*100; 
                                echo round($ibsDataInfo[1][$ibsGroupInfo['group_info'][1]['attrs']['charge']][1] / 1024 / 1024, 2); // to convert to megabytes; 
                                ?>MB remaining</span> 
                            <div class="progress m-b-0">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ibsUserInfo['basic_info']['credit'] / $ibsGroupInfo['group_info'][1]['raw_attrs']['group_credit'] * 100; ?>%;"> <span class="sr-only">20% Complete</span> </div>
                            </div> 

                            <hr/>          

                            <h3 class="box-title">Wallet Balance</h3>
                           <div class="text-right"> <!--  <span class="text-muted"><?php echo $ibsUserInfo['basic_info']['group_name']; ?></span> -->
                                <h1><sup><i class="fa fa-credit-card text-success"> </i></sup>$<?php echo round($ibsUserInfo['basic_info']['deposit'], 2); ?></h1>
                            </div>
                           <!-- <span class="text-success">20%</span> -->
                            <div class="progress m-b-0">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">20% Complete</span> </div>
                            </div>            
                            <hr>
                            <div class = "text-right">
                                <?php
                                if ($ibsUserInfo['basic_info']['credit'] <= 0 || strcmp($ibsUserInfo['basic_info']['status'], 'Recharged') == 0)
                                    echo '<span><a href = "" data-toggle="modal" data-target=".bs-example-modal-sm" ><b>Renew Package</b></a></span> |';
                                ?>

                                <span><a href = "packages.html">Upgrade Package</a></span>
                            </div>
                        </div>

                    </div>




                    <!-- the modal to choose if you are using wallet or paynow to renew package sits here -->





                    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                    <h4 class="modal-title" id="mySmallModalLabel">Which Payment Method would you like to use?</h4>
                                </div>
                                <div class="modal-body"> 




                                    <form action = "renewpackage_using_wallet.php" method = "post">
                                        <input type = "hidden" name = "amount" value = "<?php echo $ibsGroupInfo['group_info'][1]['raw_attrs']['group_credit']; ?>">
                                        <button class="btn btn-block btn-info" type = "submit">Pay using wallet</button>
                                    </form>  

                                    <hr/>

                                    <form action = "paynowredirect_renewpackage.php" method = "post">
                                        <input type = "hidden" name = "amount" value = "<?php echo $ibsGroupInfo['group_info'][1]['raw_attrs']['group_credit']; ?>">
                                        <button class="btn btn-block btn-info" type = "submit">Other Payment Methods</button>
                                    </form>  



                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>



                    <!-- the modal to choose if you are using wallet or paynow to renew package ends here -->




                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">

                        <a href = "#" data-toggle="modal" data-target=".bs-example-modal-lg-2">
                            <div class="white-box text-center bg-purple">
                                <b><h2 class="text-white counter"><i class="icon-wallet"></i>Wallet Recharge</h2></b>
                                <p class="text-white">Need more credit? You can use your wallet balance to top uo your credit in the future, should you run out. Top up your wallet here.</p>        
                            </div>
                        </a>




                        <!-- wallet top up modal starts here-->
                        <!-- this is the header of the modal-->

                        <div class="modal fade bs-example-modal-lg-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                        <h4 class="modal-title" id="myLargeModalLabel">How much would you like to add to your wallet?</h4>
                                    </div>
                                    <div class="modal-body">

                                        <!-- The content within the modal comes here -->



                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="white-box">
                                                    <div class="row pricing-plan">


                                                        <!-- row 1 starts here--> 

                                                        <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                                            <div class="pricing-box">
                                                                <div class="pricing-body b-l">
                                                                    <div class="pricing-header">
                                                                        <h4 class="text-center">...</h4>
                                                                        <h2 class="text-center"><span class="price-sign">$</span>5.00</h2>
                                                                        <p class="uppercase">...</p>
                                                                    </div>
                                                                    <div class="price-table-content">   
                                                                        <div class="price-row">               
                                                                            <form action = "paynowredirect_add_to_wallet.php" method = "post">
                                                                                <input type = "hidden" name = "amount" value = "5.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Buy Now </button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- row 1 ends here-->  
                                                        <!-- row 2 starts here--> 

                                                        <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                                            <div class="pricing-box">
                                                                <div class="pricing-body b-l">
                                                                    <div class="pricing-header">
                                                                        <h4 class="text-center">...</h4>
                                                                        <h2 class="text-center"><span class="price-sign">$</span>10.00</h2>
                                                                        <p class="uppercase">...</p>
                                                                    </div>
                                                                    <div class="price-table-content">   
                                                                        <div class="price-row">               
                                                                            <form action = "paynowredirect_add_to_wallet.php" method = "post">
                                                                                <input type = "hidden" name = "amount" value = "10.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Buy Now </button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- row 2 ends here-->  
                                                        <!-- row 3 starts here--> 

                                                        <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                                            <div class="pricing-box">
                                                                <div class="pricing-body b-l">
                                                                    <div class="pricing-header">
                                                                        <h4 class="text-center">...</h4>
                                                                        <h2 class="text-center"><span class="price-sign">$</span>20.00</h2>
                                                                        <p class="uppercase">...</p>
                                                                    </div>
                                                                    <div class="price-table-content">   
                                                                        <div class="price-row">               
                                                                            <form action = "paynowredirect_add_to_wallet.php" method = "post">
                                                                                <input type = "hidden" name = "amount" value = "20.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Buy Now </button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- row 3 ends here-->  
                                                        <!-- row 4 starts here-->  

                                                        <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                                            <div class="pricing-box">
                                                                <div class="pricing-body b-l">
                                                                    <div class="pricing-header">
                                                                        <h4 class="text-center">...</h4>
                                                                        <h2 class="text-center"><span class="price-sign">$</span>50.00</h2>
                                                                        <p class="uppercase">...</p>
                                                                    </div>
                                                                    <div class="price-table-content">   
                                                                        <div class="price-row">               
                                                                            <form action = "paynowredirect_add_to_wallet.php" method = "post">
                                                                                <input type = "hidden" name = "amount" value = "50.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Buy Now </button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- row 4 ends here-->  


                                                    </div>
                                                </div>
                                            </div> 


                                            <!-- The content within the modal ends here -->

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>


                            <!-- wallet top up modal ends here-->

                        </div>



                        <a href = "#" data-toggle="modal" data-target=".bs-example-modal-lg">
                            <div class="white-box text-center bg-info">
                                <h2 class="text-white counter"><i class="icon-refresh"></i>Credit Purchase</h2>
                                <p class="text-white">Get credit using your wallet balance in order to continue browsing.</p>
                            </div>
                        </a>


                        <!-- wallet push modal starts here-->
                        <!-- this is the header of the modal-->

                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                        <h4 class="modal-title" id="myLargeModalLabel">How much would you like to add to your credit?</h4>
                                    </div>
                                    <div class="modal-body">

                                        <!-- The content within the modal comes here -->



                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="white-box">
                                                    <div class="row pricing-plan">


                                                        <!-- row 1 starts here--> 

                                                        <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                                            <div class="pricing-box">
                                                                <div class="pricing-body b-l">
                                                                    <div class="pricing-header">
                                                                        <h4 class="text-center">250MB</h4>
                                                                        <h2 class="text-center"><span class="price-sign">$</span>1.00</h2>
                                                                        <p class="uppercase">250 MB</p>
                                                                    </div>
                                                                    <div class="price-table-content">
                                                                        <div class="price-row"><i class="icon-user"></i> up to 2Mbps</div>
                                                                        <div class="price-row"><i class="icon-screen-smartphone"></i> Valid for 30 days</div>
                                                                        <div class="price-row"><i class="icon-drawar"></i> Fast Broadband Speed</div>
                                                                        <div class="price-row"><i class="icon-refresh"></i> Pay-As-You-Go!</div>
                                                                        <div class="price-row">
                                                                            <form action = "walletpay.php" method = "post">
                                                                                <input type="hidden" name = "amount" value = "1.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                                            </form>
                                                                            <form action = "paynowredirect_add_to_credit.php" method = "post">
                                                                                <input type = "hidden" name = "amount" value = "1.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- row 1 ends here-->  
                                                        <!-- row 2 starts here--> 

                                                        <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                                            <div class="pricing-box b-l">
                                                                <div class="pricing-body">
                                                                    <div class="pricing-header">
                                                                        <h4 class="text-center">1GB</h4>
                                                                        <h2 class="text-center"><span class="price-sign">$</span>3.00</h2>
                                                                        <p class="uppercase">1 GB</p>
                                                                    </div>
                                                                    <div class="price-table-content">
                                                                        <div class="price-row"><i class="icon-user"></i> up to 2Mbps</div>
                                                                        <div class="price-row"><i class="icon-screen-smartphone"></i> Valid for 60 days</div>
                                                                        <div class="price-row"><i class="icon-drawar"></i> Fast Broadband Speed</div>
                                                                        <div class="price-row"><i class="icon-refresh"></i> Pay-As-You-Go!</div>
                                                                        <div class="price-row">
                                                                            <form action = "walletpay.php" method = "post">
                                                                                <input type="hidden" name = "amount" value = "3.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                                            </form>
                                                                            <form action = "paynowredirect_add_to_credit.php" method = "post">
                                                                                <input type = "hidden" name = "amount" value = "3.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- row 2 ends here-->  
                                                        <!-- row 3 starts here--> 

                                                        <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                                            <div class="pricing-box featured-plan">
                                                                <div class="pricing-body">
                                                                    <div class="pricing-header">
                                                                        <h4 class="price-lable text-white bg-warning"> Popular</h4>
                                                                        <h4 class="text-center">3GB</h4>
                                                                        <h2 class="text-center"><span class="price-sign">$</span>7.00</h2>
                                                                        <p class="uppercase">3 GB</p>
                                                                    </div>
                                                                    <div class="price-table-content">
                                                                        <div class="price-row"><i class="icon-user"></i> up to 5Mbps</div>
                                                                        <div class="price-row"><i class="icon-screen-smartphone"></i> Valid for 75 days</div>
                                                                        <div class="price-row"><i class="icon-drawar"></i> Fast Broadband Speed</div>
                                                                        <div class="price-row"><i class="icon-refresh"></i> Pay-As-You-Go!</div>
                                                                        <div class="price-row">
                                                                            <form action = "walletpay.php" method = "post">
                                                                                <input type="hidden" name = "amount" value = "7.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                                            </form>
                                                                            <form action = "paynowredirect_add_to_credit.php" method = "post">
                                                                                <input type = "hidden" name = "amount" value = "7.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- row 3 ends here-->  
                                                        <!-- row 4 starts here-->  

                                                        <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                                            <div class="pricing-box">
                                                                <div class="pricing-body b-r">
                                                                    <div class="pricing-header">
                                                                        <h4 class="text-center">5GB</h4>
                                                                        <h2 class="text-center"><span class="price-sign">$</span>13.00</h2>
                                                                        <p class="uppercase">5 GB</p>
                                                                    </div>
                                                                    <div class="price-table-content">
                                                                        <div class="price-row"><i class="icon-user"></i> up to 5Mbps</div>
                                                                        <div class="price-row"><i class="icon-screen-smartphone"></i> Valid for 90 days</div>
                                                                        <div class="price-row"><i class="icon-drawar"></i> Fast Broadband Speed</div>
                                                                        <div class="price-row"><i class="icon-refresh"></i> Pay-As-You-Go!</div>
                                                                        <div class="price-row">
                                                                            <form action = "walletpay.php" method = "post">
                                                                                <input type="hidden" name = "amount" value = "13.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                                            </form>
                                                                            <form action = "paynowredirect_add_to_credit.php" method = "post">
                                                                                <input type = "hidden" name = "amount" value = "13.00"/>
                                                                                <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- row 4 ends here-->  


                                                </div>
                                            </div>
                                        </div> 


                                        <!-- The content within the modal ends here -->

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>


                        <!-- wallet push modal ends here-->

                    </div>


                    <!-- top up history begins here-->

                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Top Up History
                                <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
                                    <select class="form-control pull-right row b-none">
                                        <option><?php echo date('M Y'); ?></option>
                                        <option><?php echo date('M Y', strtotime("-1 month")); ?></option>
                                        <option><?php echo date('M Y', strtotime("-2 month")); ?></option>
                                        <option><?php echo date('M Y', strtotime("-3 month")); ?></option>
                                        <option><?php echo date('M Y', strtotime("-4 month")); ?></option>
                                    </select>
                                </div>
                            </h3>
                            <div class="row sales-report">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h2><?php echo date('M Y'); ?></h2>
                                    <p>PURCHASE HISTORY</p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 ">
                                    <h1 class="text-right text-success m-t-20">$<?php echo round($ibsUserInfo['basic_info']['credit'], 2); ?></h1>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>NAME</th>
                                            <th>STATUS</th>
                                            <th>DATE</th>
                                            <th>PRICE</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                                        <!--Start adding purchase history rows here-->



                                        <?php $operator->phpPopulatePurchaseHistory($user_id); ?>



                                        <!--Stop adding purchase history rows here-->


                                    </tbody>
                                </table>
                                <a href="#" class = "showbottom">View all purchase history</a> </div>
                        </div>
                    </div>



                    <!-- page content ends here -->
                    <!-- .right-sidebar -->

                    <!-- /.right-sidebar -->
                </div>
                <!-- /.container-fluid -->


                <footer class="footer text-center"> &copy Copyright TeleContract 2016 </footer>
            </div>

            <?php
// show the red banner if the account is locked


            if (strcmp($ibsUserInfo['attrs']['lock'], 'Blocked') == 0) {


                echo '


  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop" style="display: block;"> <i class="ti-user"></i> Your account has been suspened. For more information and to reactivate it, <a href = "#" class = "text-info">Contact support.</a><a href="#" class="closed">x</a> </div>

  ';
            }

// show message is the switch fails because user still has credit


            if (isset($_GET['error']) && strcmp($_GET['error'], 'credit_present') == 0 && $ibsUserInfo['basic_info']['credit'] > 0 && strcmp($ibsUserInfo['basic_info']['status'], 'Package') == 0) {


                echo '


  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop" style="display: block;"> <i class="ti-user"></i> Your are still within your bundle limits, you need to have run out in order to get more credit.<a href="#" class="closed">x</a> </div>

  ';
            }

            if (isset($_GET['error']) && isset($_GET['amount']) && strcmp($_GET['error'], 'deposit_insufficient') == 0 && $ibsUserInfo['basic_info']['deposit'] < $_GET['amount']) {


                echo '


  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop" style="display: block;"> <i class="ti-user"></i> You wallet balance is insufficient for you to purchase $' . $_GET['amount'] . ' worth of credit.  If you would like to top your wallet <a href = "package-purchase.html">Click here.</a><a href="#" class="closed">x</a> </div>

  ';
            }


// show the green banner if switch is successful


            if (isset($_GET['message']) && strcmp($_GET['message'], 'switch_success') == 0) {


                echo '


  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop" style="display: block;"> <i class="ti-user"></i> You have successfully pushed credit to your account<a href="#" class="closed">x</a> </div>

  ';
            }


            if (isset($_GET['message']) && strcmp($_GET['message'], 'renew_success') == 0) {


                echo '


  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop" style="display: block;"> <i class="ti-user"></i> You have successfully re activated your subscription <a href="#" class="closed">x</a> </div>

  ';
            }


//give user the option to add credit if they have no credit but do have some balance in their wallet 

            if ($ibsUserInfo['basic_info']['credit'] == 0 && $ibsUserInfo['basic_info']['deposit'] > 0) {


                echo '


  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-warning myadmin-alert-top alerttop" style="display: block;"> <i class="ti-user"></i> Your main credit has run out. Would you like to top up your credit? <a href = "" data-toggle="modal" data-target=".bs-example-modal-sm" >Top up now.</a><a href="#" class="closed">x</a> </div>

  ';
            }


//show the green banner if the payment was a success

            $txn_status;
            $txn_amount;

            include ("../dbconnect.php");
            $db = mysql_select_db($dbname, $conn);
// SQL query to fetch information of registerd users and finds user match.
            $query = mysql_query("select * from transactions where txn_id ='" . $_GET['order_id'] . "' AND user_id = '" . $user_id . "'", $conn);

            while ($row = mysql_fetch_assoc($query)) {
// OR just echo the data: // store the userid in a session variable
                $txn_status = $row['txn_status'];
                $txn_amount = $row['txn_amount'];
            }


            if (strcmp($txn_status, 'complete') == 0) {

                echo '


  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop" style="display: block;"> <i class="ti-user"></i> Congratulations! You have successfully topped up your balance with $' . $txn_amount . '<a href="#" class="closed">x</a> </div>

  ';
            }
            ?>


            <!-- /#wrapper -->


            <!-- jQuery -->
            <script src="../../plugins/bower_components/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <script src="../../plugins/bower_components/toast-master/js/jquery.toast.js"></script>
            <script src="../js/toastr.js"></script>

            <script type="text/javascript">
                //Alerts

                $(".myadmin-alert .closed").click(function (event) {
                    $(this).parents(".myadmin-alert").fadeToggle(350);

                    return false;
                });

                /* Click to close */

                $(".myadmin-alert-click").click(function (event) {
                    $(this).fadeToggle(350);

                    return false;
                });


            </script>

            <!-- Sweet-Alert  -->
            <script src="../../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
            <script src="../../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>


            <!--Morris JavaScript -->
            <script src="../../plugins/bower_components/raphael/raphael-min.js"></script>
            <script src="../../plugins/bower_components/morrisjs/morris.js"></script>

            <!-- jQuery for carousel -->
            <script src="../../plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
            <script src="../../plugins/bower_components/owl.carousel/owl.custom.js"></script>

            <script src="../../plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
            <script src="../../plugins/bower_components/counterup/jquery.counterup.min.js"></script>

            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <script src="../js/widget.js"></script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

        </div>
    </body>
</html>
