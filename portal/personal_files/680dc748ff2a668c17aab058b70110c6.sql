-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 05, 2017 at 10:56 PM
-- Server version: 5.6.37
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ppavppbq_txnsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_information`
--

CREATE TABLE `account_information` (
  `account_id` int(11) NOT NULL DEFAULT '0',
  `ibs_id` varchar(255) NOT NULL,
  `account_type` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `ad_id` int(11) NOT NULL,
  `ad_title` varchar(255) NOT NULL,
  `ad_article` varchar(255) NOT NULL,
  `ad_category` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `media0` varchar(255) NOT NULL,
  `media1` varchar(255) NOT NULL,
  `media2` varchar(255) NOT NULL,
  `media3` varchar(255) NOT NULL,
  `uploader` varchar(255) NOT NULL,
  `date_uploaded` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`ad_id`, `ad_title`, `ad_article`, `ad_category`, `status`, `media0`, `media1`, `media2`, `media3`, `uploader`, `date_uploaded`) VALUES
(9, 'Get Instant Credit ', 'Did you know you can instant credit just by topping up your account on time? Try it, you will be amazed', '', 'new', 'http://www.geeksquad.co.zw/portal/ad_media/e06e9d625ed268b23d3feae6085b6045.png', 'http://www.geeksquad.co.zw/portal/ad_media/b8e133ea51f6e88245501c574958774b.png', '', '', '200', '2016-11-15');

-- --------------------------------------------------------

--
-- Table structure for table `audit_trail`
--

CREATE TABLE `audit_trail` (
  `action_id` int(11) NOT NULL,
  `action_group_id` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `old_value` varchar(255) NOT NULL,
  `new_value` varchar(255) NOT NULL,
  `performed_by` varchar(255) NOT NULL,
  `performed_on` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_performed` date NOT NULL,
  `time_performed` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_trail`
--

INSERT INTO `audit_trail` (`action_id`, `action_group_id`, `action`, `old_value`, `new_value`, `performed_by`, `performed_on`, `description`, `date_performed`, `time_performed`) VALUES
(1, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', '0', '2016-10-20', '00:00:00'),
(2, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: 455', '2016-10-20', '04:36:36'),
(3, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: 456', '2016-10-20', '04:36:48'),
(4, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: 457', '2016-10-20', '04:40:09'),
(5, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-10-20', '05:10:51'),
(6, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-10-20', '05:10:52'),
(7, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-10-20', '05:54:14'),
(8, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:27:45'),
(9, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:29:21'),
(10, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: 470', '2016-11-12', '11:29:54'),
(11, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:31:23'),
(12, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:31:47'),
(13, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:34:52'),
(14, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:36:21'),
(15, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:36:26'),
(16, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:36:48'),
(17, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:37:39'),
(18, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:38:33'),
(19, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:38:48'),
(20, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:42:08'),
(21, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:43:03'),
(22, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:46:13'),
(23, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:46:30'),
(24, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: 471', '2016-11-12', '11:49:02'),
(25, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:50:01'),
(26, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: 472', '2016-11-12', '11:50:19'),
(27, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:51:19'),
(28, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:52:49'),
(29, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: 473', '2016-11-12', '11:53:13'),
(30, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:53:44'),
(31, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: 474', '2016-11-12', '11:55:17'),
(32, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-12', '11:56:26'),
(33, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: 475', '2016-11-12', '11:57:06'),
(34, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2016-11-14', '09:43:48'),
(35, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '4925', 'rev_id: FAILED', '2017-01-03', '06:05:38'),
(36, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '4926', 'rev_id: FAILED', '2017-01-20', '11:29:23'),
(37, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2017-04-11', '01:09:56'),
(38, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '92', 'rev_id: FAILED', '2017-04-11', '01:11:12'),
(39, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '1', 'rev_id: 1060', '2017-04-11', '01:23:01'),
(40, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '1', 'rev_id: FAILED', '2017-04-11', '01:29:19'),
(41, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '1', 'rev_id: FAILED', '2017-04-11', '01:36:52'),
(42, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '5', 'rev_id: 1119', '2017-09-27', '01:27:16'),
(43, '', 'reversal', '', '', 'ZSS token: t3lc0zss', '5', 'rev_id: 1120', '2017-09-27', '01:29:11');

-- --------------------------------------------------------

--
-- Table structure for table `auto_renewals`
--

CREATE TABLE `auto_renewals` (
  `entry_id` int(11) NOT NULL,
  `user_id` varchar(45) NOT NULL,
  `ibs_id` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auto_renewals`
--

INSERT INTO `auto_renewals` (`entry_id`, `user_id`, `ibs_id`) VALUES
(53, '26', '5096'),
(71, '42', '4860');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `doc_id` int(11) NOT NULL,
  `doc_name` varchar(255) NOT NULL,
  `doc_description` varchar(255) NOT NULL,
  `doc_path` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_uploaded` date NOT NULL,
  `uploaded_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`doc_id`, `doc_name`, `doc_description`, `doc_path`, `user_id`, `status`, `date_uploaded`, `uploaded_by`) VALUES
(1, 'EULA.pdf', 'test doc descrpition', 'documents/doc.pdf', '', 'deleted', '2016-09-06', 1),
(3, 'The_Last_Stand', 'This is an xmen comic reavafvearercawceqwecWECWDWECwec', 'http://www.geeksquad.co.zw/portal/documents/The_Last_Stand.pdf', '', 'deleted', '2016-11-02', 5);

-- --------------------------------------------------------

--
-- Table structure for table `dummy table`
--

CREATE TABLE `dummy table` (
  `_id` int(11) NOT NULL,
  `col1` varchar(45) DEFAULT NULL,
  `col2` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `faq_id` int(11) NOT NULL,
  `faq_question` varchar(255) NOT NULL,
  `faq_answer` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `uploader` varchar(255) NOT NULL,
  `date_uploaded` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`faq_id`, `faq_question`, `faq_answer`, `status`, `uploader`, `date_uploaded`) VALUES
(1, 'How do I activate my account?', 'See more info in the link below', 'new', '', '2016-11-12'),
(2, 'How do I activate my account?', 'See more info in the link below', 'new', '', '2016-11-12'),
(3, 'How do I activate my account??', 'See more info in the link below http://www.google.com', 'new', '', '2017-01-03'),
(4, 'How do I activate my account??', 'See more info in the link below http://www.google.com/doodles', 'new', '', '2017-01-03');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `file_name` varchar(245) DEFAULT NULL,
  `file_description` varchar(245) DEFAULT NULL,
  `file_path` varchar(245) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `upload_date` date DEFAULT NULL,
  `uploader` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`file_id`, `user_id`, `file_name`, `file_description`, `file_path`, `status`, `upload_date`, `uploader`) VALUES
(17, '344', 'Extension_list', NULL, 'http://www.geeksquad.co.zw/portal/personal_files/31735c35025d345773a16c1e8256bd91.', 'new', '2017-04-14', '');

-- --------------------------------------------------------

--
-- Table structure for table `ibs_groups`
--

CREATE TABLE `ibs_groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `ibs_group_name` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `quota` int(11) NOT NULL,
  `burst` int(11) NOT NULL,
  `validity` int(11) NOT NULL,
  `desc1` varchar(450) DEFAULT NULL,
  `desc2` varchar(450) DEFAULT NULL,
  `popular` int(11) DEFAULT NULL,
  `group_status` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ibs_groups`
--

INSERT INTO `ibs_groups` (`group_id`, `group_name`, `ibs_group_name`, `price`, `quota`, `burst`, `validity`, `desc1`, `desc2`, `popular`, `group_status`) VALUES
(1, 'Velocity 11G', 'Velocity_11G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(2, 'Velocity 12G', 'Velocity_12G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(3, 'Velocity 150G', 'Velocity_150G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(4, 'Velocity 18G', 'Velocity_18G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(5, 'Velocity 25G', 'Velocity_25G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(6, 'Velocity 38G', 'Velocity_38G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(7, 'Velocity 50G', 'Velocity_50G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(8, 'Velocity 65G', 'Velocity_65G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(9, 'Velocity 90G', 'Velocity_90G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(10, 'Velocity Fire', 'Fibre-Velocity-Fire', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(11, 'Velocity Ultra', 'Fibre-Velocity-Ultra', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(12, 'Velocity Xtreme', 'Fibre-Velocity-Xtreme', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(13, '24 HR', '24_HR', 1, 250, 1, 30, 'Fast Broadband Speed', 'Pay-As-You-Go', 1, 'active'),
(15, '48 HR', '48_HR', 2, 500, 2, 30, 'Fast Broadband Speed', 'Pay-As-You-Go', 0, 'active'),
(16, '72 HR', '72_HR', 3, 1000, 3, 30, 'Fast Broadband Speed', 'Pay-As-You-Go', 1, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `msg_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `msg_category` varchar(255) NOT NULL,
  `sender_user_id` varchar(255) NOT NULL,
  `receiver_user_id` varchar(255) NOT NULL,
  `msg_subject` varchar(255) NOT NULL,
  `msg_content` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `time_created` time NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`msg_id`, `thread_id`, `msg_category`, `sender_user_id`, `receiver_user_id`, `msg_subject`, `msg_content`, `date_created`, `time_created`, `status`) VALUES
(100, 0, 'sales', '100', '', 'Support Inquiry', '', '0000-00-00', '00:00:00', ''),
(200, 0, 'billing', '200', '', 'Billing Inquiry', '', '0000-00-00', '00:00:00', 'read'),
(203, 0, 'general', '344', '200', 'RE: Account Activation', 'imi', '2017-04-14', '07:57:25', 'read'),
(204, 0, 'general', '344', '344', 'RE: RE: Account Activation', 'lol', '2017-04-14', '08:24:09', 'unread'),
(205, 0, 'general', '5', '100', 'RE: Support Inquiry', '<img alt=\"\" src=\"http://\"><img alt=\"\" src=\"https://www.colourbox.com/preview/1706086-beautiful-brightly-red-flower-on-a-white-background.jpg\">', '2017-04-15', '10:15:03', 'read'),
(206, 0, 'general', '347', '100', 'RE: Support Inquiry', 'hello.', '2017-04-24', '10:28:07', 'read'),
(207, 0, 'billing', '347', '', 'account credit top up ', 'my credit is not showing if it has topped up.&nbsp;', '2017-05-05', '09:52:20', 'read'),
(208, 0, 'general', '347', '347', 'RE: account credit top up ', 'are you able to browse<br><br>', '2017-05-05', '10:00:05', 'read'),
(209, 0, 'general', '200', '344', 'RE: RE: Account Activation', 'what are you talking about?', '2017-06-03', '08:37:55', 'read'),
(210, 0, 'general', '200', '', 'Testing fake username', 'Will this message actually go through or not?', '2017-06-03', '08:41:03', 'unread');

-- --------------------------------------------------------

--
-- Table structure for table `messagethreads`
--

CREATE TABLE `messagethreads` (
  `thread_id` int(11) NOT NULL,
  `owner_id` varchar(255) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `date_created` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messagethreads`
--

INSERT INTO `messagethreads` (`thread_id`, `owner_id`, `receiver_id`, `date_created`) VALUES
(1, '5', 0, ''),
(3, '5', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `message_codes`
--

CREATE TABLE `message_codes` (
  `_id` int(11) NOT NULL,
  `code` varchar(3) DEFAULT NULL,
  `message` varchar(450) DEFAULT NULL,
  `description` varchar(450) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message_codes`
--

INSERT INTO `message_codes` (`_id`, `code`, `message`, `description`, `status`) VALUES
(1, '01', 'Renew Failed', '', 0),
(2, '02', 'Your account is not yet verifed. Please open your email address to verify your account.', NULL, 0),
(3, '03', 'Your account still needs to be verified by billing', NULL, 0),
(4, '04', 'Your account still needs to be verified by support', NULL, 0),
(5, '05', 'Your was deleted, contact support', NULL, 0),
(6, '06', 'Your account was disabled', NULL, 0),
(7, '07', 'Your account shows an unknown error, but it is not active', NULL, 0),
(8, '08', '\'Your account is not currently link to a billing server ID\'', NULL, 0),
(9, '09', 'Your account still has credit', NULL, 0),
(10, '10', 'Your package has expired, you need to reactivate your package first', NULL, 0),
(11, '11', 'Your deposit balance is insufficient to perform this action', NULL, 0),
(12, '12', 'Renew Successful', NULL, 1),
(13, '13', 'Message successfully sent', NULL, 1),
(14, '14', 'Profile updated successfully', NULL, 1),
(15, '15', 'Profile update failed', NULL, 0),
(16, '16', 'Failed to change groups', NULL, 0),
(17, '17', 'Failed to renew package', NULL, 0),
(18, '18', 'Failed to set edposit', NULL, 0),
(19, '19', 'Package upgrade was successful', NULL, 1),
(20, '20', 'Error preserving the credit after changing your package', NULL, 0),
(21, '21', 'Successfully bought credit using your wallet balnce', NULL, 1),
(22, '22', 'Error deducting the wallet balance after buying credit using wallet', NULL, 0),
(23, '23', 'Your account has been blocked in the billing server. For more information and to reactivate it', NULL, 0),
(24, '24', 'Your main credit has run out. Would you like to top up your credit?', NULL, 2),
(25, '25', 'Congratulations! You have successfully topped up your balance', NULL, 1),
(26, '26', 'Your transaction was not completed. ', NULL, 0),
(27, '27', 'Account has been activated by billing and is now pending support activation', NULL, 2),
(28, '28', 'Failed to activate the account', NULL, 0),
(29, '29', 'Link to IBS entry was successful', NULL, 1),
(30, '30', 'New user save was successful', NULL, 1),
(31, '31', 'Linking with IBS customer was not successful', NULL, 0),
(32, '32', 'New user could not be saved', NULL, 0),
(33, '33', 'Account(s) deletion was successful', NULL, 1),
(34, '34', 'Could not mark account as deleted in portal database', NULL, 0),
(35, '35', 'Could not delete account(s)', NULL, 0),
(36, '36', 'Advert was successfully deleted', NULL, 1),
(37, '37', 'Could not mark advert as deleted in our database', NULL, 0),
(38, '38', 'Could not delete advert', NULL, 0),
(39, '39', 'Document was deleted successfully', NULL, 1),
(40, '40', 'Could not mark document as deleted in portal database', NULL, 0),
(41, '41', 'Document could not be deleted', NULL, 0),
(42, '42', 'FAQ was successfully deleted', NULL, 1),
(43, '43', 'Could not mark FAQ as deleted in our DB', NULL, 0),
(44, '44', 'Could not delete FAQ', NULL, 0),
(45, '45', 'Account has been successfully disabled', NULL, 1),
(46, '46', 'Could not mark account as disabled in portal DB', NULL, 0),
(47, '47', 'Could not disable account', NULL, 0),
(48, '48', 'Transaction successful added', NULL, 1),
(49, '49', 'Failed to update transaction details', NULL, 0),
(50, '50', 'Account was successfully enabled', NULL, 1),
(51, '51', 'Could not mark account as enabled in portal databse', NULL, 0),
(52, '52', 'Could not enable account', NULL, 0),
(53, '53', 'User file was successfully removed', NULL, 1),
(54, '54', 'Could not remove user file', NULL, 0),
(55, '55', 'User package was successfully renewed', NULL, 1),
(56, '56', 'Failed to preserve the users credit when package was renewed', NULL, 0),
(57, '57', 'Failed to renew this users package', NULL, 0),
(58, '58', 'Credit successfully added', NULL, 1),
(59, '59', 'Credit could not be added', NULL, 0),
(60, '60', 'Credit successfully set', NULL, 1),
(61, '61', 'Credit could not be set', NULL, 0),
(62, '62', 'Wallet balance added to successfully', NULL, 1),
(63, '63', 'Could not add to wallet balance', NULL, 0),
(64, '64', 'Wallet balance succesfully set', NULL, 1),
(65, '65', 'Could not set wallet balance', NULL, 0),
(66, '66', 'Group was changed successfully', NULL, 1),
(67, '67', 'Could not change group', NULL, 0),
(68, '68', 'Credit change failed after adding credit. CRITICAL ERROR', NULL, 0),
(69, '69', 'Failed to delete file', NULL, 0),
(70, '70', 'Failed to upload file, check its size, check if it does not already exist', NULL, 0),
(71, '71', 'File upload failed', NULL, 0),
(72, '72', 'File metadata saved successfully', NULL, 1),
(73, '73', 'File metadata could not be saved to the database', NULL, 0),
(74, '74', 'Transaction reversed', NULL, 1),
(75, '75', 'Failed to reverse transaction', NULL, 0),
(76, '76', 'User does not exist', NULL, 0),
(77, '77', 'Could not add transaction into database', NULL, 0),
(78, '78', 'Successfully added new transaction', NULL, 1),
(79, '79', 'Could not register the user with the details supplied', NULL, 0),
(80, '80', 'Upload Failed', NULL, 0),
(81, '81', 'Credit could not be removed. CRITICAL ERROR', NULL, 0),
(82, '82', 'Ad uploaded successfully', NULL, 1),
(83, '83', 'Upload successful', NULL, 1),
(84, '84', 'Operation successful', NULL, 1),
(85, '85', 'Opertaion Failed', NULL, 0),
(86, '86', 'Your Account was disabled, you will only have limited functionality', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `other_dummy_table`
--

CREATE TABLE `other_dummy_table` (
  `_id` int(11) NOT NULL,
  `col1` varchar(45) DEFAULT NULL,
  `col2` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `ibs_id` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_started` datetime NOT NULL,
  `date_ended` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `user_id`, `ibs_id`, `status`, `date_started`, `date_ended`) VALUES
(18, '1', '92', 'destroyed', '0000-00-00 00:00:00', '2016-09-08 10:49:05'),
(17, '1', '92', 'destroyed', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, '1', '92', 'destroyed', '2016-09-08 10:57:13', '2016-09-08 10:57:25'),
(20, '1', '92', 'active', '2016-09-08 12:06:03', '0000-00-00 00:00:00'),
(21, '5', '95', 'destroyed', '2016-09-08 12:29:27', '2016-09-08 12:32:47'),
(22, '1', '92', 'destroyed', '2016-09-08 12:32:54', '2016-09-08 12:44:59'),
(23, '5', '95', 'active', '2016-09-08 12:45:07', '0000-00-00 00:00:00'),
(24, '5', '95', 'active', '2016-09-08 12:58:56', '0000-00-00 00:00:00'),
(25, '1', '92', 'active', '2016-09-08 01:47:14', '0000-00-00 00:00:00'),
(26, '5', '95', 'active', '2016-09-08 02:01:16', '0000-00-00 00:00:00'),
(27, '25', '', 'destroyed', '2016-09-09 06:26:10', '2016-09-09 06:27:34'),
(28, '25', '', 'destroyed', '2016-09-09 06:44:37', '2016-09-09 06:46:17'),
(29, '1', '92', 'active', '2016-09-09 06:46:25', '0000-00-00 00:00:00'),
(30, '1', '92', 'active', '2016-09-10 06:16:04', '0000-00-00 00:00:00'),
(31, '1', '92', 'active', '2016-09-12 06:57:40', '0000-00-00 00:00:00'),
(32, '1', '92', 'active', '2016-09-13 06:00:03', '0000-00-00 00:00:00'),
(33, '1', '92', 'active', '2016-09-13 06:16:35', '0000-00-00 00:00:00'),
(34, '5', '95', 'destroyed', '2016-09-14 06:18:19', '2016-09-14 06:30:06'),
(35, '5', '95', 'destroyed', '2016-09-14 06:30:18', '2016-09-14 07:00:10'),
(36, '1', '92', 'active', '2016-09-14 07:00:19', '0000-00-00 00:00:00'),
(37, '1', '92', 'active', '2016-09-14 02:03:11', '0000-00-00 00:00:00'),
(38, '1', '92', 'active', '2016-09-15 06:29:22', '0000-00-00 00:00:00'),
(39, '1', '92', 'active', '2016-09-15 07:19:16', '0000-00-00 00:00:00'),
(40, '1', '92', 'active', '2016-09-15 07:20:02', '0000-00-00 00:00:00'),
(41, '1', '92', 'active', '2016-09-15 10:14:13', '0000-00-00 00:00:00'),
(42, '1', '92', 'active', '2016-09-16 10:01:12', '0000-00-00 00:00:00'),
(43, '1', '92', 'active', '2016-09-16 10:01:13', '0000-00-00 00:00:00'),
(44, '1', '92', 'active', '2016-09-16 10:49:27', '0000-00-00 00:00:00'),
(45, '1', '92', 'active', '2016-09-16 11:58:37', '0000-00-00 00:00:00'),
(46, '1', '92', 'active', '2016-09-16 11:59:26', '0000-00-00 00:00:00'),
(47, '1', '92', 'active', '2016-09-16 12:00:27', '0000-00-00 00:00:00'),
(48, '1', '92', 'destroyed', '2016-09-17 07:30:18', '2016-09-17 09:27:00'),
(49, '', '', 'destroyed', '2016-09-17 09:39:01', '2016-09-17 09:41:12'),
(500, '27', '5', 'active', '2016-09-17 09:41:26', '0000-00-00 00:00:00'),
(51, '1', '92', 'destroyed', '2016-09-19 06:15:56', '2016-09-19 08:55:09'),
(52, '1', '92', 'destroyed', '2016-09-19 08:55:31', '2016-09-19 12:56:09'),
(53, '1', '92', 'destroyed', '2016-09-19 12:56:11', '2016-09-19 01:12:58'),
(54, '1', '92', 'active', '2016-09-19 01:13:14', '0000-00-00 00:00:00'),
(55, '1', '92', 'active', '2016-09-20 06:20:47', '0000-00-00 00:00:00'),
(56, '1', '92', 'destroyed', '2016-09-20 07:16:37', '2016-09-20 12:20:16'),
(57, '5', '95', 'active', '2016-09-20 12:20:24', '0000-00-00 00:00:00'),
(58, '1', '92', 'active', '2016-09-21 07:26:49', '0000-00-00 00:00:00'),
(59, '1', '92', 'destroyed', '2016-09-21 08:17:44', '2016-09-21 08:21:05'),
(60, '1', '92', 'destroyed', '2016-09-21 09:28:28', '2016-09-21 09:40:20'),
(61, '5', '95', 'destroyed', '2016-09-21 09:40:32', '2016-09-21 09:59:38'),
(62, '5', '95', 'destroyed', '2016-09-21 09:59:49', '2016-09-21 09:59:55'),
(63, '1', '92', 'active', '2016-09-21 09:59:57', '0000-00-00 00:00:00'),
(64, '1', '92', 'destroyed', '2016-09-22 06:21:21', '2016-09-22 10:38:46'),
(65, '1', '92', 'active', '2016-09-22 09:43:59', '0000-00-00 00:00:00'),
(66, '1', '92', 'active', '2016-09-22 10:38:51', '0000-00-00 00:00:00'),
(67, '1', '92', 'destroyed', '2016-09-23 07:09:31', '2016-09-23 12:54:37'),
(68, '1', '25', 'active', '2016-09-23 12:54:39', '0000-00-00 00:00:00'),
(69, '1', '25', 'active', '2016-09-24 06:08:19', '0000-00-00 00:00:00'),
(70, '1', '25', 'destroyed', '2016-09-24 06:18:56', '2016-09-24 07:25:16'),
(71, '1', '25', 'destroyed', '2016-09-24 07:25:17', '2016-09-24 10:09:50'),
(72, '1', '25', 'active', '2016-09-24 07:26:21', '0000-00-00 00:00:00'),
(73, '1', '25', 'active', '2016-09-24 07:30:20', '0000-00-00 00:00:00'),
(74, '', '', 'active', '2016-09-24 10:10:55', '0000-00-00 00:00:00'),
(75, '', '', 'active', '2016-09-24 10:12:53', '0000-00-00 00:00:00'),
(76, '', '', 'active', '2016-09-24 10:14:09', '0000-00-00 00:00:00'),
(77, '1', '25', 'active', '2016-09-24 10:58:53', '0000-00-00 00:00:00'),
(78, '1', '25', 'destroyed', '2016-09-26 06:30:23', '2016-09-26 06:41:20'),
(79, '5', '95', 'destroyed', '2016-09-26 06:41:38', '2016-09-26 07:54:58'),
(80, '1', '25', 'destroyed', '2016-09-26 07:55:00', '2016-09-26 07:57:55'),
(81, '', '', 'destroyed', '2016-09-26 07:59:31', '2016-09-26 08:09:34'),
(82, '', '', 'destroyed', '2016-09-26 08:10:12', '2016-09-26 08:11:22'),
(83, '34', '', 'destroyed', '2016-09-26 08:11:53', '2016-09-26 08:12:59'),
(84, '35', '4853', 'destroyed', '2016-09-26 08:13:52', '2016-09-26 09:07:45'),
(85, '1', '25', 'destroyed', '2016-09-26 09:07:47', '2016-09-26 12:50:29'),
(86, '26', '', 'active', '2016-09-26 12:45:25', '0000-00-00 00:00:00'),
(87, '36', '4854', 'active', '2016-09-26 12:59:14', '0000-00-00 00:00:00'),
(88, '1', '25', 'active', '2016-09-26 01:01:51', '0000-00-00 00:00:00'),
(89, '37', '4855', 'active', '2016-09-26 02:05:40', '0000-00-00 00:00:00'),
(90, '38', '4856', 'active', '2016-09-26 02:07:33', '0000-00-00 00:00:00'),
(91, '39', '4857', 'active', '2016-09-26 02:11:31', '0000-00-00 00:00:00'),
(92, '1', '25', 'active', '2016-09-26 02:47:53', '0000-00-00 00:00:00'),
(93, '1', '25', 'destroyed', '2016-09-27 06:53:49', '2016-09-27 08:48:25'),
(94, '1', '25', 'destroyed', '2016-09-27 08:50:31', '2016-09-27 09:22:34'),
(95, '40', '4858', 'destroyed', '2016-09-27 09:39:00', '2016-09-27 10:14:24'),
(96, '41', '4859', 'destroyed', '2016-09-27 10:15:47', '2016-09-27 10:20:05'),
(97, '41', '4859', 'active', '2016-09-27 10:18:01', '0000-00-00 00:00:00'),
(98, '41', '4859', 'active', '2016-09-27 10:18:35', '0000-00-00 00:00:00'),
(99, '1', '25', 'destroyed', '2016-09-27 10:20:15', '2016-09-27 10:20:59'),
(100, '41', '4859', 'destroyed', '2016-09-27 10:21:15', '2016-09-27 10:21:20'),
(101, '1', '25', 'destroyed', '2016-09-27 10:21:30', '2016-09-27 10:24:40'),
(102, '41', '4859', 'destroyed', '2016-09-27 10:25:02', '2016-09-27 10:46:51'),
(103, '1', '25', 'active', '2016-09-27 10:47:28', '0000-00-00 00:00:00'),
(104, '1', '25', 'active', '2016-09-27 10:49:11', '0000-00-00 00:00:00'),
(105, '1', '25', 'destroyed', '2016-09-27 12:19:06', '2016-09-27 12:19:13'),
(106, '41', '4859', 'active', '2016-09-27 12:19:29', '0000-00-00 00:00:00'),
(107, '42', '4860', 'active', '2016-09-27 12:27:27', '0000-00-00 00:00:00'),
(108, '42', '4860', 'active', '2016-09-27 12:44:13', '0000-00-00 00:00:00'),
(109, '41', '4859', 'destroyed', '2016-09-28 06:23:55', '2016-09-28 06:24:27'),
(110, '1', '25', 'destroyed', '2016-09-28 06:24:33', '2016-09-28 06:27:18'),
(111, '43', '4861', 'destroyed', '2016-09-28 06:52:52', '2016-09-28 07:16:59'),
(112, '43', '4861', 'destroyed', '2016-09-28 07:17:34', '2016-09-28 07:20:54'),
(113, '41', '4859', 'destroyed', '2016-09-28 07:21:16', '2016-09-28 07:22:35'),
(114, '43', '4861', 'active', '2016-09-28 07:22:45', '0000-00-00 00:00:00'),
(115, '44', '4862', 'active', '2016-09-29 01:18:45', '0000-00-00 00:00:00'),
(116, '44', '4862', 'active', '2016-09-29 01:24:54', '0000-00-00 00:00:00'),
(117, '1', '25', 'active', '2016-10-01 06:37:07', '0000-00-00 00:00:00'),
(118, '26', '12', 'active', '2016-10-08 07:48:59', '0000-00-00 00:00:00'),
(119, '1', '25', 'active', '2016-10-10 08:39:07', '0000-00-00 00:00:00'),
(120, '26', '12', 'destroyed', '2016-10-13 06:39:48', '2016-10-13 06:40:18'),
(121, '1', '25', 'active', '2016-10-19 07:18:43', '0000-00-00 00:00:00'),
(122, '1', '25', 'active', '2016-10-22 06:39:53', '0000-00-00 00:00:00'),
(123, '1', '25', 'active', '2016-10-24 06:23:27', '0000-00-00 00:00:00'),
(124, '5', '95', 'destroyed', '2016-10-25 01:29:38', '2016-10-25 01:29:52'),
(125, '1', '25', 'active', '2016-10-25 01:30:02', '0000-00-00 00:00:00'),
(126, '1', '25', 'active', '2016-10-26 06:47:39', '0000-00-00 00:00:00'),
(127, '1', '25', 'destroyed', '2016-10-26 06:50:16', '2016-10-26 07:35:55'),
(128, '5', '95', 'destroyed', '2016-10-26 07:36:16', '2016-10-26 07:38:19'),
(129, '1', '25', 'destroyed', '2016-10-26 07:38:26', '2016-10-26 07:44:06'),
(130, '1', '25', 'active', '2016-10-26 07:44:20', '0000-00-00 00:00:00'),
(131, '1', '25', 'active', '2016-10-27 07:35:56', '0000-00-00 00:00:00'),
(132, '1', '92', 'active', '2016-10-28 06:31:26', '0000-00-00 00:00:00'),
(133, '1', '92', 'active', '2016-10-28 03:10:41', '0000-00-00 00:00:00'),
(134, '1', '92', 'active', '2016-10-28 04:43:37', '0000-00-00 00:00:00'),
(135, '1', '92', 'active', '2016-10-29 07:17:49', '0000-00-00 00:00:00'),
(136, '1', '92', 'active', '2016-10-31 06:26:32', '0000-00-00 00:00:00'),
(137, '45', '4863', 'active', '2016-10-31 07:06:38', '0000-00-00 00:00:00'),
(138, '46', '4864', 'active', '2016-10-31 07:12:43', '0000-00-00 00:00:00'),
(139, '47', '4865', 'active', '2016-10-31 07:13:17', '0000-00-00 00:00:00'),
(140, '48', '4866', 'active', '2016-10-31 07:14:06', '0000-00-00 00:00:00'),
(141, '49', '4867', 'destroyed', '2016-10-31 07:14:39', '2016-10-31 07:23:18'),
(142, '1', '92', 'active', '2016-10-31 07:23:25', '0000-00-00 00:00:00'),
(143, '1', '92', 'active', '2016-11-01 12:28:05', '0000-00-00 00:00:00'),
(144, '5', '95', 'destroyed', '2016-11-01 12:40:38', '2016-11-01 12:51:04'),
(145, '5', '95', 'active', '2016-11-01 12:50:02', '0000-00-00 00:00:00'),
(146, '1', '92', 'active', '2016-11-01 12:51:12', '0000-00-00 00:00:00'),
(147, '5', '95', 'active', '2016-11-02 08:40:47', '0000-00-00 00:00:00'),
(148, '1', '92', 'active', '2016-11-02 09:51:47', '0000-00-00 00:00:00'),
(149, '5', '95', 'destroyed', '2016-11-02 01:46:47', '2016-11-02 02:13:58'),
(150, '1', '92', 'active', '2016-11-02 02:14:08', '0000-00-00 00:00:00'),
(151, '1', '92', 'active', '2016-11-03 06:57:41', '0000-00-00 00:00:00'),
(152, '1', '92', 'active', '2016-11-03 08:56:33', '0000-00-00 00:00:00'),
(153, '1', '92', 'active', '2016-11-03 09:36:23', '0000-00-00 00:00:00'),
(154, '1', '92', 'active', '2016-11-05 06:30:26', '0000-00-00 00:00:00'),
(155, '1', '92', 'active', '2016-11-05 06:53:21', '0000-00-00 00:00:00'),
(156, '1', '92', 'active', '2016-11-06 09:00:36', '0000-00-00 00:00:00'),
(157, '1', '92', 'active', '2016-11-09 12:16:48', '0000-00-00 00:00:00'),
(158, '1', '92', 'active', '2016-11-09 01:51:47', '0000-00-00 00:00:00'),
(159, '1', '92', 'active', '2016-11-09 01:55:36', '0000-00-00 00:00:00'),
(160, '5', '95', 'active', '2016-11-10 06:43:59', '0000-00-00 00:00:00'),
(161, '5', '95', 'active', '2016-11-10 06:52:56', '0000-00-00 00:00:00'),
(162, '5', '95', 'destroyed', '2016-11-10 07:01:50', '2016-11-10 07:24:00'),
(163, '5', '95', 'destroyed', '2016-11-10 07:24:42', '2016-11-10 07:24:47'),
(164, '5', '95', 'active', '2016-11-10 07:24:55', '0000-00-00 00:00:00'),
(165, '5', '95', 'active', '2016-11-10 12:29:45', '0000-00-00 00:00:00'),
(166, '1', '92', 'destroyed', '2016-11-11 06:51:13', '2016-11-11 07:58:40'),
(167, '1', '92', 'active', '2016-11-11 07:59:15', '0000-00-00 00:00:00'),
(168, '1', '92', 'active', '2016-11-12 07:02:28', '0000-00-00 00:00:00'),
(169, '1', '92', 'active', '2016-11-12 08:26:19', '0000-00-00 00:00:00'),
(170, '1', '92', 'active', '2016-11-12 09:54:15', '0000-00-00 00:00:00'),
(171, '1', '92', 'active', '2016-11-12 09:58:20', '0000-00-00 00:00:00'),
(172, '1', '92', 'active', '2016-11-14 06:37:10', '0000-00-00 00:00:00'),
(173, '1', '92', 'active', '2016-11-14 09:39:59', '0000-00-00 00:00:00'),
(174, '5', '95', 'destroyed', '2016-11-15 12:54:16', '2016-11-15 02:03:42'),
(175, '1', '92', 'active', '2016-11-15 02:03:50', '0000-00-00 00:00:00'),
(176, '5', '95', 'destroyed', '2016-11-15 02:05:55', '2016-11-15 02:50:40'),
(177, '1', '92', 'active', '2016-11-15 02:50:49', '0000-00-00 00:00:00'),
(178, '1', '92', 'active', '2016-11-16 06:27:26', '0000-00-00 00:00:00'),
(179, '1', '92', 'active', '2016-11-17 06:19:07', '0000-00-00 00:00:00'),
(180, '1', '92', 'active', '2016-11-17 09:28:11', '0000-00-00 00:00:00'),
(181, '1', '92', 'destroyed', '2016-11-18 06:43:28', '2016-11-18 06:52:50'),
(182, '1', '92', 'destroyed', '2016-11-18 06:52:57', '2016-11-18 07:16:03'),
(183, '1', '92', 'destroyed', '2016-11-18 07:16:10', '2016-11-18 08:01:58'),
(184, '1', '92', 'destroyed', '2016-11-18 08:02:03', '2016-11-18 08:53:02'),
(185, '1', '92', 'destroyed', '2016-11-18 08:53:08', '2016-11-18 09:48:03'),
(186, '1', '92', 'destroyed', '2016-11-18 09:48:09', '2016-11-18 09:58:03'),
(187, '1', '92', 'destroyed', '2016-11-18 09:58:12', '2016-11-18 12:22:12'),
(188, '1', '92', 'destroyed', '2016-11-18 12:22:25', '2016-11-18 12:22:42'),
(189, '46', '0', 'active', '2016-11-18 12:24:08', '0000-00-00 00:00:00'),
(190, '47', '0', 'destroyed', '2016-11-18 12:39:12', '2016-11-18 01:19:07'),
(191, '48', '0', 'destroyed', '2016-11-18 01:20:26', '2016-11-18 02:25:33'),
(192, '48', '0', 'active', '2016-11-18 02:26:33', '0000-00-00 00:00:00'),
(193, '48', '0', 'active', '2016-11-19 06:25:58', '0000-00-00 00:00:00'),
(194, '48', '0', 'destroyed', '2016-11-19 06:47:45', '2016-11-19 06:50:40'),
(195, '48', '0', 'active', '2016-11-19 06:50:57', '0000-00-00 00:00:00'),
(196, '48', '0', 'active', '2016-11-20 03:22:37', '0000-00-00 00:00:00'),
(197, '48', '0', 'destroyed', '2016-11-21 06:34:30', '2016-11-21 06:56:26'),
(198, '49', '', 'active', '2016-11-21 06:58:06', '0000-00-00 00:00:00'),
(199, '49', '', 'destroyed', '2016-11-21 07:21:05', '2016-11-21 07:29:47'),
(200, '5', '4923', 'destroyed', '2016-11-21 07:29:55', '2016-11-21 07:33:22'),
(201, '49', '4926', 'active', '2016-11-21 07:33:33', '0000-00-00 00:00:00'),
(202, '49', '4926', 'active', '2016-11-22 06:44:15', '0000-00-00 00:00:00'),
(203, '49', '4926', 'active', '2016-11-22 09:47:36', '0000-00-00 00:00:00'),
(204, '49', '4926', 'destroyed', '2016-11-23 07:00:52', '2016-11-23 07:42:46'),
(205, '5', '4923', 'destroyed', '2016-11-23 07:42:54', '2016-11-23 07:51:48'),
(206, '49', '4926', 'active', '2016-11-23 10:14:37', '0000-00-00 00:00:00'),
(207, '5', '4923', 'destroyed', '2016-11-23 11:12:53', '2016-11-23 11:13:57'),
(208, '5', '4923', 'destroyed', '2016-11-23 11:16:21', '2016-11-23 02:34:34'),
(209, '5', '4923', 'active', '2016-11-23 02:34:42', '0000-00-00 00:00:00'),
(210, '49', '4926', 'destroyed', '2016-11-24 06:27:55', '2016-11-24 06:30:03'),
(211, '50', '', 'destroyed', '2016-11-24 06:30:11', '2016-11-24 06:31:51'),
(212, '5', '4923', 'destroyed', '2016-11-24 06:32:00', '2016-11-24 06:33:14'),
(213, '26', '12', 'destroyed', '2016-11-24 06:43:37', '2016-11-24 08:01:39'),
(214, '52', '', 'active', '2016-11-24 06:53:06', '0000-00-00 00:00:00'),
(215, '52', '', 'destroyed', '2016-11-24 06:55:53', '2016-11-24 07:01:29'),
(216, '1', '4926', 'destroyed', '2016-11-24 07:01:42', '2016-11-24 07:11:04'),
(217, '52', '4927', 'destroyed', '2016-11-24 07:11:33', '2016-11-24 07:20:25'),
(218, '1', '4926', 'destroyed', '2016-11-24 07:20:39', '2016-11-24 07:21:51'),
(219, '50', '', 'destroyed', '2016-11-24 07:22:06', '2016-11-24 07:24:26'),
(220, '52', '4927', 'destroyed', '2016-11-24 07:24:33', '2016-11-24 07:36:16'),
(221, '50', '', 'destroyed', '2016-11-24 07:36:24', '2016-11-24 08:34:59'),
(222, '41', '4859', 'destroyed', '2016-11-24 08:28:56', '2016-11-24 08:31:33'),
(223, '41', '4859', 'active', '2016-11-24 08:30:44', '0000-00-00 00:00:00'),
(224, '41', '4859', 'active', '2016-11-24 08:31:46', '0000-00-00 00:00:00'),
(225, '41', '4859', 'active', '2016-11-24 08:34:57', '0000-00-00 00:00:00'),
(226, '5', '4923', 'active', '2016-11-30 11:26:51', '0000-00-00 00:00:00'),
(227, '5', '4923', 'destroyed', '2016-12-01 07:13:30', '2016-12-01 07:24:53'),
(228, '5', '4923', 'active', '2016-12-01 08:52:27', '0000-00-00 00:00:00'),
(229, '5', '4923', 'active', '2016-12-01 09:54:25', '0000-00-00 00:00:00'),
(230, '5', '4923', 'destroyed', '2016-12-01 12:06:17', '2016-12-01 12:28:03'),
(231, '50', '', 'destroyed', '2016-12-01 12:31:58', '2016-12-01 01:01:04'),
(232, '1', '4926', 'active', '2016-12-03 07:40:29', '0000-00-00 00:00:00'),
(233, '5', '4923', 'destroyed', '2016-12-05 07:04:01', '2016-12-05 07:48:05'),
(234, '5', '4923', 'active', '2016-12-05 09:42:01', '0000-00-00 00:00:00'),
(235, '5', '4923', 'active', '2016-12-05 01:24:57', '0000-00-00 00:00:00'),
(236, '101', '', 'destroyed', '2016-12-06 08:34:28', '2016-12-06 12:21:08'),
(237, '5', '4923', 'active', '2016-12-06 12:21:31', '0000-00-00 00:00:00'),
(238, '103', '', 'active', '2016-12-06 12:23:59', '0000-00-00 00:00:00'),
(239, '105', '', 'active', '2016-12-06 12:33:49', '0000-00-00 00:00:00'),
(240, '106', '', 'active', '2016-12-06 01:22:56', '0000-00-00 00:00:00'),
(241, '106', '4932', 'active', '2016-12-06 01:56:41', '0000-00-00 00:00:00'),
(242, '1', '4926', 'active', '2016-12-07 06:24:13', '0000-00-00 00:00:00'),
(243, '107', '', 'active', '2016-12-07 07:01:22', '0000-00-00 00:00:00'),
(244, '5', '4923', 'active', '2016-12-07 07:30:39', '0000-00-00 00:00:00'),
(245, '5', '4923', 'active', '2016-12-07 07:42:46', '0000-00-00 00:00:00'),
(246, '200', '', 'active', '2016-12-08 07:48:24', '0000-00-00 00:00:00'),
(247, '5', '4923', 'active', '2016-12-08 08:04:36', '0000-00-00 00:00:00'),
(248, '300', '', 'destroyed', '2016-12-08 08:17:54', '2016-12-08 08:31:28'),
(249, '100', '', 'active', '2016-12-08 09:24:17', '0000-00-00 00:00:00'),
(250, '100', '', 'active', '2016-12-08 10:33:19', '0000-00-00 00:00:00'),
(251, '100', '', 'active', '2016-12-08 10:46:53', '0000-00-00 00:00:00'),
(252, '100', '', 'active', '2016-12-08 10:55:02', '0000-00-00 00:00:00'),
(253, '100', '', 'active', '2016-12-09 08:13:30', '0000-00-00 00:00:00'),
(254, '100', '', 'active', '2016-12-09 08:29:52', '0000-00-00 00:00:00'),
(255, '100', '', 'active', '2016-12-10 08:03:41', '0000-00-00 00:00:00'),
(256, '200', '', 'destroyed', '2016-12-10 08:24:27', '2016-12-10 08:24:33'),
(257, '100', '', 'active', '2016-12-10 08:24:47', '0000-00-00 00:00:00'),
(258, '302', '', 'active', '2016-12-10 08:44:40', '0000-00-00 00:00:00'),
(259, '5', '4923', 'destroyed', '2016-12-13 06:16:29', '2016-12-13 07:23:58'),
(260, '50', '', 'active', '2016-12-13 07:24:07', '0000-00-00 00:00:00'),
(261, '200', '', 'active', '2016-12-13 08:34:56', '0000-00-00 00:00:00'),
(262, '200', '', 'active', '2016-12-13 08:59:59', '0000-00-00 00:00:00'),
(263, '100', '', 'active', '2016-12-13 12:31:04', '0000-00-00 00:00:00'),
(264, '5', '4923', 'active', '2016-12-13 02:18:17', '0000-00-00 00:00:00'),
(265, '100', '', 'active', '2016-12-13 03:00:52', '0000-00-00 00:00:00'),
(266, '5', '4923', 'active', '2016-12-14 06:17:59', '0000-00-00 00:00:00'),
(267, '200', '', 'active', '2016-12-15 06:48:20', '0000-00-00 00:00:00'),
(268, '5', '4923', 'destroyed', '2016-12-20 06:46:03', '2016-12-20 08:47:36'),
(269, '100', '', 'destroyed', '2016-12-20 08:48:19', '2016-12-20 10:36:05'),
(270, '100', '', 'destroyed', '2016-12-28 05:49:11', '2016-12-28 07:18:44'),
(271, '5', '4923', 'destroyed', '2016-12-28 07:18:54', '2016-12-28 08:17:15'),
(272, '100', '', 'active', '2016-12-28 08:17:36', '0000-00-00 00:00:00'),
(273, '100', '', 'active', '2016-12-29 06:22:14', '0000-00-00 00:00:00'),
(274, '5', '4923', 'active', '2016-12-29 02:30:38', '0000-00-00 00:00:00'),
(275, '5', '4923', 'active', '2016-12-31 06:34:33', '0000-00-00 00:00:00'),
(276, '200', '', 'destroyed', '2017-01-03 06:21:51', '2017-01-03 06:23:56'),
(277, '305', '', 'active', '2017-01-03 06:33:47', '0000-00-00 00:00:00'),
(278, '305', '', 'destroyed', '2017-01-03 06:37:08', '2017-01-03 06:39:04'),
(279, '200', '', 'destroyed', '2017-01-03 06:39:29', '2017-01-03 06:40:46'),
(280, '100', '', 'active', '2017-01-03 06:41:01', '0000-00-00 00:00:00'),
(281, '200', '', 'destroyed', '2017-01-03 06:41:24', '2017-01-03 06:49:51'),
(282, '100', '', 'destroyed', '2017-01-03 06:50:05', '2017-01-03 06:50:28'),
(283, '305', '4950', 'destroyed', '2017-01-03 06:50:30', '2017-01-03 06:55:25'),
(284, '300', '', 'active', '2017-01-03 06:55:39', '0000-00-00 00:00:00'),
(285, '306', '', 'destroyed', '2017-01-03 07:01:55', '2017-01-03 07:03:38'),
(286, '100', '', 'destroyed', '2017-01-03 07:03:49', '2017-01-03 07:17:19'),
(287, '100', '', 'destroyed', '2017-01-03 07:17:36', '2017-01-03 07:23:39'),
(288, '200', '', 'destroyed', '2017-01-03 07:23:52', '2017-01-03 08:10:28'),
(289, '307', '', 'destroyed', '2017-01-03 08:06:07', '2017-01-03 08:16:04'),
(290, '5', '4923', 'destroyed', '2017-01-03 08:11:38', '2017-01-03 09:32:51'),
(291, '307', '', 'destroyed', '2017-01-03 08:16:17', '2017-01-03 08:16:31'),
(292, '307', '', 'destroyed', '2017-01-03 08:17:14', '2017-01-03 08:21:19'),
(293, '200', '', 'destroyed', '2017-01-03 09:33:20', '2017-01-03 09:37:02'),
(294, '5', '4923', 'destroyed', '2017-01-03 09:37:11', '2017-01-03 12:42:22'),
(295, '5', '4923', 'destroyed', '2017-01-03 12:43:43', '2017-01-03 01:25:16'),
(296, '5', '4923', 'destroyed', '2017-01-03 01:30:59', '2017-01-03 02:15:48'),
(297, '200', '', 'destroyed', '2017-01-03 02:16:24', '2017-01-03 02:39:24'),
(298, '5', '4923', 'destroyed', '2017-01-03 02:39:32', '2017-01-03 02:47:16'),
(299, '200', '', 'active', '2017-01-03 02:47:26', '0000-00-00 00:00:00'),
(300, '200', '', 'destroyed', '2017-01-04 06:12:13', '2017-01-04 06:38:17'),
(301, '100', '', 'destroyed', '2017-01-04 06:38:26', '2017-01-04 06:46:00'),
(302, '5', '4923', 'destroyed', '2017-01-04 06:46:08', '2017-01-04 07:18:27'),
(303, '100', '', 'destroyed', '2017-01-04 07:18:52', '2017-01-04 07:34:19'),
(304, '5', '4923', 'active', '2017-01-04 07:34:37', '0000-00-00 00:00:00'),
(305, '200', '', 'active', '2017-01-05 10:11:55', '0000-00-00 00:00:00'),
(306, '200', '', 'active', '2017-01-06 06:36:36', '0000-00-00 00:00:00'),
(307, '100', '', 'destroyed', '2017-01-09 06:27:18', '2017-01-09 06:30:23'),
(308, '200', '', 'active', '2017-01-09 06:30:31', '0000-00-00 00:00:00'),
(309, '200', '', 'active', '2017-01-09 10:40:38', '0000-00-00 00:00:00'),
(310, '200', '', 'active', '2017-01-10 07:14:00', '0000-00-00 00:00:00'),
(311, '200', '', 'destroyed', '2017-01-11 10:55:03', '2017-01-11 10:55:26'),
(312, '100', '', 'active', '2017-01-11 10:55:40', '0000-00-00 00:00:00'),
(313, '200', '', 'active', '2017-01-13 06:43:05', '0000-00-00 00:00:00'),
(314, '5', '4923', 'active', '2017-01-13 09:03:29', '0000-00-00 00:00:00'),
(501, '200', '', 'active', '2017-01-18 06:30:11', '0000-00-00 00:00:00'),
(502, '200', '', 'destroyed', '2017-01-19 01:49:27', '2017-01-19 01:51:53'),
(503, '5', '4923', 'active', '2017-01-19 01:52:02', '0000-00-00 00:00:00'),
(504, '200', '', 'active', '2017-01-20 08:24:53', '0000-00-00 00:00:00'),
(505, '200', '', 'active', '2017-01-20 08:29:05', '0000-00-00 00:00:00'),
(506, '200', '', 'destroyed', '2017-01-22 03:58:15', '2017-01-22 03:59:41'),
(507, '5', '4923', 'destroyed', '2017-01-22 03:59:53', '2017-01-22 04:00:38'),
(508, '200', '', 'active', '2017-01-24 08:44:54', '0000-00-00 00:00:00'),
(509, '5', '4923', 'active', '2017-03-07 01:03:51', '0000-00-00 00:00:00'),
(510, '5', '4923', 'active', '2017-03-13 07:11:50', '0000-00-00 00:00:00'),
(511, '26', '12', 'active', '2017-03-29 10:24:12', '0000-00-00 00:00:00'),
(512, '26', '12', 'destroyed', '2017-03-29 01:26:02', '2017-03-29 01:29:36'),
(513, '200', '', 'destroyed', '2017-03-29 01:29:53', '2017-03-29 01:32:42'),
(514, '100', '', 'destroyed', '2017-03-29 01:32:56', '2017-03-29 01:49:21'),
(515, '26', '5096', 'active', '2017-03-29 01:49:35', '0000-00-00 00:00:00'),
(516, '26', '5096', 'destroyed', '2017-03-29 02:02:45', '2017-03-29 02:03:22'),
(517, '5', '4923', 'destroyed', '2017-03-29 02:03:37', '2017-03-29 02:08:23'),
(518, '200', '', 'active', '2017-03-29 02:08:30', '0000-00-00 00:00:00'),
(519, '5', '4923', 'active', '2017-03-29 02:18:21', '0000-00-00 00:00:00'),
(520, '5', '4923', 'destroyed', '2017-04-03 06:25:03', '2017-04-03 06:26:15'),
(521, '200', '', 'active', '2017-04-03 06:26:31', '0000-00-00 00:00:00'),
(522, '200', '', 'active', '2017-04-03 06:59:46', '0000-00-00 00:00:00'),
(523, '200', '', 'active', '2017-04-05 11:39:17', '0000-00-00 00:00:00'),
(524, '316', '', 'active', '2017-04-05 01:25:32', '0000-00-00 00:00:00'),
(525, '317', '', 'active', '2017-04-05 01:27:23', '0000-00-00 00:00:00'),
(526, '200', '', 'active', '2017-04-05 02:21:23', '0000-00-00 00:00:00'),
(527, '320', '', 'active', '2017-04-05 03:01:21', '0000-00-00 00:00:00'),
(528, '200', '', 'active', '2017-04-07 12:06:42', '0000-00-00 00:00:00'),
(529, '200', '', 'active', '2017-04-07 02:00:40', '0000-00-00 00:00:00'),
(530, '321', '', 'active', '2017-04-07 02:16:50', '0000-00-00 00:00:00'),
(531, '200', '', 'active', '2017-04-08 06:35:15', '0000-00-00 00:00:00'),
(532, '200', '', 'active', '2017-04-08 08:13:26', '0000-00-00 00:00:00'),
(533, '200', '', 'destroyed', '2017-04-08 02:50:11', '2017-04-08 02:54:59'),
(534, '334', '45', 'destroyed', '2017-04-08 02:55:09', '2017-04-08 02:56:13'),
(535, '5', '4923', 'destroyed', '2017-04-08 02:56:35', '2017-04-08 02:56:40'),
(536, '5', '4923', 'destroyed', '2017-04-08 02:58:10', '2017-04-08 02:59:44'),
(537, '100', '', 'active', '2017-04-08 03:00:00', '0000-00-00 00:00:00'),
(538, '100', '', 'destroyed', '2017-04-08 03:56:05', '2017-04-08 04:31:45'),
(539, '200', '', 'destroyed', '2017-04-08 04:35:46', '2017-04-08 05:46:25'),
(540, '5', '4923', 'active', '2017-04-08 05:46:34', '0000-00-00 00:00:00'),
(541, '336', '', 'active', '2017-04-09 09:00:53', '0000-00-00 00:00:00'),
(542, '336', '', 'destroyed', '2017-04-09 09:03:06', '2017-04-09 09:08:28'),
(543, '337', '', 'active', '2017-04-09 09:42:23', '0000-00-00 00:00:00'),
(544, '339', '', 'destroyed', '2017-04-09 09:54:04', '2017-04-09 11:32:48'),
(545, '200', '', 'active', '2017-04-09 11:33:14', '0000-00-00 00:00:00'),
(546, '26', '5096', 'destroyed', '2017-04-13 08:39:55', '2017-04-13 08:52:40'),
(547, '26', '5096', 'destroyed', '2017-04-13 08:52:55', '2017-04-13 01:19:46'),
(548, '336', '5149', 'destroyed', '2017-04-13 01:35:16', '2017-04-13 01:36:18'),
(549, '100', '', 'destroyed', '2017-04-13 01:36:28', '2017-04-13 01:42:18'),
(550, '336', '5149', 'destroyed', '2017-04-13 01:42:21', '2017-04-13 01:46:31'),
(551, '200', '', 'active', '2017-04-13 01:48:55', '0000-00-00 00:00:00'),
(552, '100', '', 'active', '2017-04-13 01:49:13', '0000-00-00 00:00:00'),
(553, '340', '', 'active', '2017-04-13 01:56:44', '0000-00-00 00:00:00'),
(554, '340', '', 'destroyed', '2017-04-13 02:04:56', '2017-04-13 02:20:25'),
(555, '340', '5150', 'destroyed', '2017-04-13 02:20:54', '2017-04-13 02:21:00'),
(556, '340', '5150', 'destroyed', '2017-04-13 02:21:17', '2017-04-13 02:21:24'),
(557, '340', '5150', 'destroyed', '2017-04-13 02:22:52', '2017-04-13 02:25:48'),
(558, '340', '5150', 'active', '2017-04-13 02:25:51', '0000-00-00 00:00:00'),
(559, '26', '5096', 'destroyed', '2017-04-13 02:36:57', '2017-04-13 02:37:03'),
(560, '26', '5096', 'active', '2017-04-13 02:37:11', '0000-00-00 00:00:00'),
(561, '26', '5096', 'destroyed', '2017-04-13 02:42:54', '2017-04-13 02:51:17'),
(562, '26', '5096', 'destroyed', '2017-04-13 02:46:54', '2017-04-13 02:47:50'),
(563, '200', '', 'destroyed', '2017-04-13 02:48:07', '2017-04-13 02:48:43'),
(564, '26', '5096', 'active', '2017-04-13 02:49:09', '0000-00-00 00:00:00'),
(565, '200', '', 'active', '2017-04-13 02:51:27', '0000-00-00 00:00:00'),
(566, '340', '5150', 'destroyed', '2017-04-14 05:48:18', '2017-04-14 05:48:23'),
(567, '342', '', 'active', '2017-04-14 06:13:45', '0000-00-00 00:00:00'),
(568, '200', '', 'active', '2017-04-14 06:15:26', '0000-00-00 00:00:00'),
(569, '343', '', 'destroyed', '2017-04-14 07:00:25', '2017-04-14 07:01:09'),
(570, '344', '', 'active', '2017-04-14 07:17:55', '0000-00-00 00:00:00'),
(571, '200', '', 'active', '2017-04-14 07:24:17', '0000-00-00 00:00:00'),
(572, '344', '5151', 'active', '2017-04-14 11:06:54', '0000-00-00 00:00:00'),
(573, '200', '', 'destroyed', '2017-04-14 08:07:30', '2017-04-15 06:28:44'),
(574, '5', '4923', 'active', '2017-04-15 07:21:48', '0000-00-00 00:00:00'),
(575, '5', '4923', 'active', '2017-04-15 12:09:08', '0000-00-00 00:00:00'),
(576, '5', '4923', 'active', '2017-04-15 01:38:26', '0000-00-00 00:00:00'),
(577, '5', '4923', 'active', '2017-04-15 05:27:08', '0000-00-00 00:00:00'),
(578, '5', '4923', 'active', '2017-04-15 08:53:11', '0000-00-00 00:00:00'),
(579, '5', '4923', 'active', '2017-04-17 07:59:13', '0000-00-00 00:00:00'),
(580, '200', '', 'active', '2017-04-19 08:34:02', '0000-00-00 00:00:00'),
(581, '5', '4923', 'active', '2017-04-19 09:09:45', '0000-00-00 00:00:00'),
(582, '5', '4923', 'active', '2017-04-19 10:23:57', '0000-00-00 00:00:00'),
(583, '200', '', 'destroyed', '2017-04-20 09:34:12', '2017-04-20 09:34:27'),
(584, '5', '4923', 'active', '2017-04-20 09:35:22', '0000-00-00 00:00:00'),
(585, '5', '4923', 'active', '2017-04-20 12:53:06', '0000-00-00 00:00:00'),
(586, '5', '4923', 'active', '2017-04-20 02:57:45', '0000-00-00 00:00:00'),
(587, '5', '4923', 'destroyed', '2017-04-24 08:29:46', '2017-04-24 08:56:19'),
(588, '5', '5', 'active', '2017-04-24 08:56:30', '0000-00-00 00:00:00'),
(589, '347', '', 'active', '2017-04-24 09:37:56', '0000-00-00 00:00:00'),
(590, '347', '12', 'active', '2017-04-24 10:14:42', '0000-00-00 00:00:00'),
(591, '347', '12', 'destroyed', '2017-04-24 10:15:22', '2017-04-24 11:08:14'),
(592, '5', '5', 'destroyed', '2017-04-24 10:20:09', '2017-04-24 11:39:23'),
(593, '347', '12', 'destroyed', '2017-04-24 11:08:21', '2017-04-24 01:06:48'),
(594, '200', '', 'active', '2017-04-24 11:39:54', '0000-00-00 00:00:00'),
(595, '347', '12', 'active', '2017-04-24 01:07:17', '0000-00-00 00:00:00'),
(596, '347', '12', 'active', '2017-04-24 01:19:42', '0000-00-00 00:00:00'),
(597, '347', '12', 'destroyed', '2017-05-03 07:10:18', '2017-05-03 07:11:07'),
(598, '347', '12', 'active', '2017-05-03 07:11:20', '0000-00-00 00:00:00'),
(599, '347', '12', 'active', '2017-05-03 07:51:38', '0000-00-00 00:00:00'),
(600, '5', '5', 'active', '2017-05-03 12:20:37', '0000-00-00 00:00:00'),
(601, '5', '5', 'active', '2017-05-03 12:20:41', '0000-00-00 00:00:00'),
(602, '100', '', 'active', '2017-05-03 12:24:41', '0000-00-00 00:00:00'),
(603, '100', '', 'destroyed', '2017-05-03 12:36:49', '2017-05-03 12:42:23'),
(604, '5', '5', 'active', '2017-05-03 12:42:36', '0000-00-00 00:00:00'),
(605, '347', '12', 'destroyed', '2017-05-05 06:53:33', '2017-05-05 06:56:36'),
(606, '347', '12', 'destroyed', '2017-05-05 07:04:32', '2017-05-05 07:05:05'),
(607, '347', '12', 'active', '2017-05-05 07:05:13', '0000-00-00 00:00:00'),
(608, '347', '12', 'active', '2017-05-05 08:17:45', '0000-00-00 00:00:00'),
(609, '347', '12', 'active', '2017-05-05 09:48:19', '0000-00-00 00:00:00'),
(610, '347', '12', 'destroyed', '2017-05-05 10:29:18', '2017-05-05 10:47:57'),
(611, '347', '12', 'destroyed', '2017-05-05 10:49:39', '2017-05-05 10:53:08'),
(612, '347', '12', 'active', '2017-05-05 10:52:20', '0000-00-00 00:00:00'),
(613, '347', '12', 'active', '2017-05-05 10:53:12', '0000-00-00 00:00:00'),
(614, '347', '12', 'active', '2017-05-08 09:06:04', '0000-00-00 00:00:00'),
(615, '347', '12', 'active', '2017-05-17 12:55:14', '0000-00-00 00:00:00'),
(616, '347', '12', 'active', '2017-05-17 02:08:34', '0000-00-00 00:00:00'),
(617, '100', '', 'destroyed', '2017-05-21 11:38:31', '2017-05-21 11:40:12'),
(618, '347', '12', 'active', '2017-05-22 07:14:40', '0000-00-00 00:00:00'),
(619, '348', '', 'destroyed', '2017-05-31 06:38:03', '2017-05-31 06:42:51'),
(620, '348', '', 'active', '2017-05-31 07:23:43', '0000-00-00 00:00:00'),
(621, '348', '', 'active', '2017-05-31 01:13:17', '0000-00-00 00:00:00'),
(622, '348', '', 'destroyed', '2017-06-03 06:39:32', '2017-06-03 07:03:49'),
(623, '200', '', 'destroyed', '2017-06-03 07:04:07', '2017-06-03 07:26:57'),
(624, '100', '', 'destroyed', '2017-06-03 07:27:19', '2017-06-03 07:32:33'),
(625, '348', '', 'active', '2017-06-03 07:32:36', '0000-00-00 00:00:00'),
(626, '348', '', 'destroyed', '2017-06-03 08:33:32', '2017-06-03 08:36:04'),
(627, '200', '', 'destroyed', '2017-06-03 08:36:40', '2017-06-03 08:43:28'),
(628, '348', '', 'destroyed', '2017-06-03 08:43:33', '2017-06-03 08:43:51'),
(629, '5', '5', 'destroyed', '2017-06-03 08:43:59', '2017-06-03 08:44:46'),
(630, '348', '', 'destroyed', '2017-06-03 08:53:19', '2017-06-03 09:52:10'),
(631, '200', '', 'active', '2017-06-03 09:54:35', '0000-00-00 00:00:00'),
(632, '349', '', 'active', '2017-06-16 10:07:41', '0000-00-00 00:00:00'),
(633, '1', '4926', 'destroyed', '2017-08-03 06:15:29', '2017-08-03 06:20:22'),
(634, '1', '4926', 'active', '2017-08-03 06:20:31', '0000-00-00 00:00:00'),
(635, '353', '', 'destroyed', '2017-08-03 08:41:35', '2017-08-03 08:47:31'),
(636, '1', '4926', 'destroyed', '2017-08-03 08:47:44', '2017-08-03 08:49:29'),
(637, '5', '5', 'destroyed', '2017-08-03 08:49:40', '2017-08-03 09:12:52'),
(638, '1', '4926', 'destroyed', '2017-08-03 09:13:01', '2017-08-03 09:13:32'),
(639, '5', '5', 'destroyed', '2017-08-03 09:13:43', '2017-08-03 09:42:22'),
(640, '354', '', 'destroyed', '2017-08-03 09:46:34', '2017-08-03 09:52:30'),
(641, '355', '', 'destroyed', '2017-08-03 09:53:30', '2017-08-03 09:55:11'),
(642, '356', '', 'destroyed', '2017-08-03 09:56:07', '2017-08-03 10:01:07'),
(643, '100', '', 'destroyed', '2017-08-03 10:01:24', '2017-08-03 10:14:50'),
(644, '357', '', 'destroyed', '2017-08-03 10:15:42', '2017-08-03 10:15:58'),
(645, '100', '', 'destroyed', '2017-08-03 10:16:10', '2017-08-03 10:18:03'),
(646, '358', '', 'destroyed', '2017-08-03 10:19:01', '2017-08-03 10:19:20'),
(647, '100', '', 'destroyed', '2017-08-03 10:19:36', '2017-08-03 10:26:01'),
(648, '200', '', 'destroyed', '2017-08-03 10:26:13', '2017-08-03 10:43:38'),
(649, '100', '', 'destroyed', '2017-08-03 10:43:51', '2017-08-03 10:46:14'),
(650, '200', '', 'destroyed', '2017-08-03 10:46:31', '2017-08-03 11:37:02'),
(651, '359', '', 'destroyed', '2017-08-03 11:40:44', '2017-08-03 11:41:04'),
(652, '200', '', 'destroyed', '2017-08-03 11:42:38', '2017-08-03 11:45:05'),
(653, '360', '', 'active', '2017-08-03 11:46:10', '0000-00-00 00:00:00'),
(654, '200', '', 'destroyed', '2017-08-03 12:00:00', '2017-08-03 12:01:19'),
(655, '360', '5', 'destroyed', '2017-08-03 12:01:50', '2017-08-03 12:02:41'),
(656, '361', '', 'active', '2017-08-03 12:27:55', '0000-00-00 00:00:00'),
(657, '362', '', 'active', '2017-08-03 01:23:56', '0000-00-00 00:00:00'),
(658, '200', '', 'destroyed', '2017-08-03 01:25:41', '2017-08-03 01:27:05'),
(659, '362', '5', 'destroyed', '2017-08-03 01:27:17', '2017-08-03 01:44:24'),
(660, '200', '', 'destroyed', '2017-08-03 01:44:53', '2017-08-03 02:00:47'),
(661, '363', '', 'destroyed', '2017-08-03 02:02:54', '2017-08-03 02:03:52'),
(662, '200', '', 'destroyed', '2017-08-03 02:05:06', '2017-08-03 02:11:57'),
(663, '100', '', 'destroyed', '2017-08-03 02:12:17', '2017-08-03 02:24:43'),
(664, '200', '', 'active', '2017-08-03 02:35:36', '0000-00-00 00:00:00'),
(665, '347', '12', 'active', '2017-08-18 09:54:56', '0000-00-00 00:00:00'),
(666, '26', '5096', 'active', '2017-09-02 09:57:13', '0000-00-00 00:00:00'),
(667, '364', '', 'active', '2017-09-10 03:43:13', '0000-00-00 00:00:00'),
(668, '365', '', 'active', '2017-09-12 06:24:30', '0000-00-00 00:00:00'),
(669, '5', '5', 'destroyed', '2017-09-18 10:17:37', '2017-09-18 10:33:01'),
(670, '200', '', 'destroyed', '2017-09-18 10:33:16', '2017-09-18 10:34:06'),
(671, '5', '5', 'active', '2017-09-18 10:34:17', '0000-00-00 00:00:00'),
(672, '366', '', 'active', '2017-09-22 05:36:17', '0000-00-00 00:00:00'),
(673, '5', '5', 'destroyed', '2017-09-25 01:43:17', '2017-09-25 01:57:52'),
(674, '200', '', 'destroyed', '2017-09-27 10:12:13', '2017-09-27 10:54:11'),
(675, '200', '', 'active', '2017-09-27 10:59:32', '0000-00-00 00:00:00'),
(676, '347', '12', 'destroyed', '2017-09-28 12:56:36', '2017-09-28 01:20:34');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `txn_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `intergration_id` varchar(255) NOT NULL,
  `txn_reference_number` varchar(255) NOT NULL,
  `txn_amount` float NOT NULL,
  `additional_info` varchar(255) NOT NULL,
  `txn_status` varchar(255) NOT NULL,
  `txn_date` date NOT NULL,
  `txn_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`txn_id`, `user_id`, `intergration_id`, `txn_reference_number`, `txn_amount`, `additional_info`, `txn_status`, `txn_date`, `txn_time`) VALUES
(1072, '5', '', '', 3, 'Credit Auto Renewal', 'complete', '2017-04-15', '11:43:10'),
(1073, '', '2640', '1073', 1, 'Jones:jones@gmail.com:0771819478', 'complete', '2017-04-17', '09:38:25'),
(1074, '5', '2640', '1074', 5, '', 'complete', '2017-04-20', '09:40:06'),
(1075, '5', '2640', '1075', 5, '', 'complete', '2017-04-20', '09:40:34'),
(1076, '5', '2640', '1076', 10, '', 'complete', '2017-04-20', '01:05:45'),
(1077, '5', '2640', '1077', 5, 'Wallet Purchase >> Paynow', 'complete', '2017-04-20', '02:57:55'),
(1078, '5', '2640', '1078', 5, 'Wallet Purchase >> Paynow', 'complete', '2017-04-24', '08:45:39'),
(1079, '5', '2640', '1079', 5, 'Wallet Purchase >> Paynow', 'complete', '2017-04-24', '08:45:58'),
(1080, '5', '2640', '1080', 1, 'Package change >> Paynow', 'processing', '2017-04-24', '09:24:29'),
(1081, '5', '2640', '1081', 1, 'Package renewal >> Paynow', 'processing', '2017-04-24', '09:29:46'),
(1082, '347', '2640', '1082', 5, 'Credit Purchase >> Paynow', 'complete', '2017-04-24', '10:08:32'),
(1083, '347', '2640', '1083', 5, 'Wallet Purchase >> Paynow', 'complete', '2017-04-24', '10:10:58'),
(1084, '347', '2640', '1084', 5, 'Credit Purchase >> Paynow', 'complete', '2017-04-24', '10:19:46'),
(1085, '5', '2640', '1085', 5, 'Wallet Purchase >> Paynow', 'complete', '2017-04-24', '10:21:59'),
(1086, '347', '2640', '1086', 5, 'Wallet Purchase >> Paynow', 'processing', '2017-04-24', '10:22:23'),
(1087, '5', '2640', '1087', 5, 'Wallet Purchase >> Paynow', 'processing', '2017-04-24', '10:22:35'),
(1088, '5', '2640', '1088', 10, 'Wallet Purchase >> Paynow', 'complete', '2017-04-24', '10:23:40'),
(1089, '5', '2640', '1089', 5, 'Wallet Purchase >> Paynow', 'cancelled', '2017-04-24', '10:24:13'),
(1090, '5', '2640', '1090', 50, 'Wallet Purchase >> Paynow', 'cancelled', '2017-04-24', '10:26:21'),
(1091, '5', '2640', '1091', 20, 'Wallet Purchase >> Paynow', 'processing', '2017-04-24', '10:26:58'),
(1092, '5', '2640', '1092', 5, 'Wallet Purchase >> Paynow', 'complete', '2017-04-24', '10:31:04'),
(1093, '5', '2640', '1093', 5, 'Wallet Purchase >> Paynow', 'cancelled', '2017-04-24', '10:31:30'),
(1094, '5', '', '', 0, 'Wallet Purchase >> Paynow', 'initiate', '0000-00-00', '00:00:00'),
(1095, '5', '2640', '1095', 5, 'Wallet Purchase >> Paynow', 'processing', '2017-04-24', '10:32:18'),
(1096, '5', '2640', '1096', 5, 'Wallet Purchase >> Paynow', 'cancelled', '2017-04-24', '10:35:19'),
(1097, '5', '2640', '1097', 5, 'Wallet Purchase >> Paynow', 'cancelled', '2017-04-24', '10:38:42'),
(1098, '5', '2640', '1098', 1, 'Package renewal >> Paynow', 'cancelled', '2017-04-24', '10:45:51'),
(1099, '5', '2640', '1099', 1, 'Package renewal >> Paynow', 'complete', '2017-04-24', '10:46:44'),
(1100, '5', '', '', 1, 'Package renewal >> Wallet', 'complete', '2017-04-24', '11:01:31'),
(1101, '5', '', '', 1, 'Package renewal >> Wallet', 'complete', '2017-04-24', '11:01:53'),
(1102, '5', '', '', 1, 'Package renewal >> Wallet', 'complete', '2017-04-24', '11:02:16'),
(1103, '5', '', '', 1, 'Package renewal >> Wallet', 'complete', '2017-04-24', '11:02:23'),
(1104, '5', '', '', 1, 'Package renewal >> Wallet', 'complete', '2017-04-24', '11:02:32'),
(1105, '5', '2640', '1105', 20, 'Wallet Purchase >> Paynow', 'cancelled', '2017-04-24', '11:35:02'),
(1106, '5', '2640', '1106', 1, 'Package renewal >> Paynow', 'complete', '2017-05-03', '12:44:50'),
(1107, '347', '2640', '1107', 5, 'confidence gweera:confidencegweera@gmail.com:0775623785', 'complete', '2017-05-05', '07:15:41'),
(1108, '347', '2640', '1108', 13, 'Credit Purchase >> Paynow', 'complete', '2017-05-08', '09:32:59'),
(1109, '5', '2640', '1109', 5, 'Wallet Purchase >> Paynow', 'complete', '2017-08-03', '09:17:03'),
(1110, '5', '2640', '1110', 2, 'Package change >> Paynow', 'reversed: 1120', '2017-08-03', '09:20:03'),
(1111, '5', '', '', 2, 'Credit Auto Renewal', 'reversed: 1119', '2017-08-03', '09:24:02'),
(1112, '23', '2640', '1112', 1, 'Josh:Harold:0771819478', 'complete', '2017-08-03', '09:44:09'),
(1113, '23', '2640', '1113', 5, 'Josh :josh@gmail.com:0771819478', 'complete', '2017-08-03', '01:09:56'),
(1114, '362', '2640', '1114', 10, 'Wallet Purchase >> Paynow', 'complete', '2017-08-03', '01:31:11'),
(1115, '23', '2640', '1115', 1, 'Reed Richards:rr@gmail.com:0772819478', 'sent to paynow for processing', '2017-09-25', '11:08:54'),
(1116, '23', '2640', '1116', 1, 'vdvdfvdfv:dvdfvdfvdf:3234234234234', 'sent to paynow for processing', '2017-09-27', '09:28:43'),
(1117, '23', '2640', '1117', 1, 'gccnc: vb vcb c:4564656456', 'sent to paynow for processing', '2017-09-27', '10:02:21'),
(1118, '23', '2640', '1118', 1, 'hgnsfdsfv:dvsdfvbsdfbsfb:576857876978', 'complete', '2017-09-27', '10:09:05'),
(1119, '5', '', '', -2, 'ZSS based Reversal', 'reversal_complete', '2017-09-27', '01:27:15'),
(1120, '5', '', '', -2, 'ZSS based Reversal', 'reversal_complete', '2017-09-27', '01:29:11'),
(1121, '347', '2640', '1121', 50, 'Credit Purchase >> Paynow', 'processing', '2017-09-28', '12:58:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `privilege` varchar(255) NOT NULL,
  `last_login` date NOT NULL,
  `creation_date` date NOT NULL,
  `verified` varchar(255) NOT NULL,
  `account_status` varchar(255) NOT NULL,
  `profile_image_url` varchar(255) NOT NULL,
  `ibs_id` varchar(255) NOT NULL,
  `cst_code` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `surname`, `username`, `password`, `phone`, `email`, `address`, `city`, `country`, `privilege`, `last_login`, `creation_date`, `verified`, `account_status`, `profile_image_url`, `ibs_id`, `cst_code`, `hash`) VALUES
(1, 'Jayden', 'Shamhu', 'jayden', 'password', '263771819478', 'jayden@telco.co.zw', '4088 Nharira View, Chapungu Road', 'Norton', 'Zimbabwe', 'billing_admin', '2017-08-03', '2016-11-21', 'verified', 'disabled', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '4926', 'cst001', 'cb70ab375662576bd1ac5aaf16b3fca4'),
(5, 'Alfred', 'Johnson', 'aljay', '1a0974dbaad9b70bf37c43f4b417b648', '0771658499', 'alfred@hisemail.com', '32 Sentry Close, Borrowdale West', 'Harare', 'Zimbabwe', 'user', '2017-09-25', '0000-00-00', 'verified', 'disabled', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '5', 'cst445', ''),
(23, 'Yeukai', 'Guruva', 'yeuguru', 'd9dad169430127a273820eb75ce8d9ec', '0716183426', 'kgrv4093@gmail.com', '17155 Rhino Close, Vainona', 'Harare', '', 'user', '0000-00-00', '0000-00-00', '', 'disabled', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '4925', 'cst444', ''),
(24, 'Lee', 'Leonda', 'lee', 'leonda123', '08683000009', 'leonda@telco.co.zw', '', '', '', 'user', '0000-00-00', '0000-00-00', '', 'deleted', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '', '', ''),
(26, 'Dumisani', 'Nkala', 'duminkala', '36b951efe1d0fc96094b2c3a1164941f', '0782040796', 'duminkala@gmail.com', '131 6th Street, Parkmore', 'Parkmore', 'Zimbabwe', 'user', '2017-09-02', '0000-00-00', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '5096', 'STAFF101', ''),
(36, 'Beatrice', 'Madovi', 'beatrice.madovi', 'bestdad#1', '0773579970', 'beatricemad@gmail.com', '5 Jessop Road ', 'Harare', 'Zimbabwe', 'user', '0000-00-00', '2016-09-26', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '4873', '', ''),
(42, 'Ndabenhle', 'Nkala', 'nndaba', '25062009', '0772128272', 'nndaba@telco.co.zw', '21 Armadale Road, borrowdale', 'Harare', 'Zimbabwe', 'user', '2016-09-27', '2016-09-27', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '4860', '', '6f3ef77ac0e3619e98159e9b6febf557'),
(50, 'Trish', 'Jane', 'trish', 'password', '0771891478', 'trish@telco.co.zw', '21 Vainona', 'Gweru', 'Zimbabwe', 'billing_admin', '2016-12-13', '0000-00-00', 'verified', 'active', '', '', '', ''),
(100, 'Support', '@ Telco', 'support@telco.co.zw', '0b85eca6c78dcb5aaffe77e132f85e46', '0000000000', 'support@telco.co.zw', '21 Armadale Road, Borrowdale', 'Harare', 'Zimbabwe', 'support_admin', '2017-08-03', '2016-09-27', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '', '', ''),
(200, 'Billing', '@ Telco', 'billing@telco.co.zw', 'b8dafdefa1fe9575c33aba712f5fe730', '00000000', 'billing@telco.co.zw', '21 Armadale Road, Borrowdale', 'Harare', 'Zimbabwe', 'billing_admin', '2017-09-27', '0000-00-00', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '', '', ''),
(300, 'Sales', '@ Telco', 'sales@telco.co.zw', 'sales001', '000000', 'sales@telco.co.zw', '21 Armadale Road, Borrowdale', 'Harare', 'Zimbabwe', 'sales_admin', '2017-01-03', '0000-00-00', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '', '', ''),
(301, 'tam', 'wing', 'tam.wing', '12345', 'support@telco.co.zw', 'tam@gmail.com', '70 cardwell rd , matshleumhope, byo', 'Bulawayo', 'Zimbabwe', 'support_admin', '0000-00-00', '2016-12-08', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '4947', '5410', 'c0f168ce8900fa56e57789e2a2f2c9d0'),
(302, 'Alfred', 'White', 'alwhite', 'alwhite', '0771833674', 'alwhite@gmail.com', '4088 Nharira View', 'Norton', 'Zimbabwe', 'user', '0000-00-00', '2016-12-10', 'verified', 'deleted', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '4949', 'cst777', 'bc6dc48b743dc5d013b1abaebd2faed2'),
(307, 'Munyaradzi', 'Muchenje', 'munyac', 'cuthbertM1', '0772198816', 'muchenje.munyaradzi@gmail.com', '70 Hillside Road, Hillside', 'Harare', 'Zimbabwe', 'billing_admin', '2017-01-03', '2017-01-03', 'verified', 'inactive', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '', '', 'ea5d2f1c4608232e07d3aa3d998e5135'),
(344, 'Tom', 'Riddle', 'tom_riddle', 'jjjjjjjj', '0771819478', 'triddle@gmail.com', '4088 Nharira View', 'Norton', 'Zimbabwe', 'user', '2017-04-14', '2017-04-14', 'verified', 'pending_support', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '5151', 'cst555', '5a4b25aaed25c2ee1b74de72dc03c14e'),
(346, 'Brad', 'Pitt', 'brad_pitt', 'bbbbbbbb', '263782040797', 'duminkala@gmail.co', '4088 Nharira View', 'Norton', 'Zimbabwe', 'user', '0000-00-00', '2017-04-15', 'verified', 'deleted', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '92', 'cst000', 'f457c545a9ded88f18ecee47145a72c0'),
(347, 'confidence', 'gweera', 'confidednce_gweera', 'lovewhat', '0775623785', 'confidencegweera@gmail.com', '5491 white city ', 'chinhoyi', 'Zimbabwe', 'user', '2017-09-28', '2017-04-24', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '12', 'cst012', '9fd81843ad7f202f26c1a174c7357585'),
(362, 'Takudzwa', 'Shamhu', 'takudzwa_shamhu', 'qqqqqqqq', '0771819479', 'tjscorp@gmail.com', '4088', 'Harare', 'Zimbabwe', 'user', '2017-08-03', '2017-08-03', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '5', '', '63dc7ed1010d3c3b8269faf0ba7491d4'),
(363, 'Darth', 'Vadar', 'darth_vadar', 'qqqqqqqq', '0771819470', 'ndabenhle.nkala@gmail.com', 'redwood', 'Harare', 'Zimbabwe', 'user', '0000-00-00', '2017-08-03', 'verified', 'active', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '5163', 'cs27887686', '70efdf2ec9b086079795c442636b55fb'),
(364, 'Caseyhow', 'CaseyhowJB', 'Caseyhow', 'uc^55ltu3BI', '88797757998', 'faustinaprosserbcfp@yahoo.com', 'https://www.viagrapascherfr.com/', 'Lianyungang', '', 'user', '0000-00-00', '2017-09-10', 'unverified', 'inactive', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '', '', '9be40cee5b0eee1462c82c6964087ff9'),
(365, 'Lelandpivix', 'LelandpivixDA', 'Lelandpivix', '4#srP4xpp5T', '85421691454', 'laelebeidiorg@mail.ru', 'https://www.viagrapascherfr.com/', 'Kwajalein', '', 'user', '0000-00-00', '2017-09-12', 'unverified', 'inactive', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '', '', '6f3ef77ac0e3619e98159e9b6febf557'),
(366, 'RichardMUP', 'RichardMUPCA', 'RichardMUP', 'N%64bvpba3O', '86136869615', 'richardsem@mail.ru', 'https://www.viagrapascherfr.com/', 'Pirassununga', '', 'user', '0000-00-00', '2017-09-22', 'unverified', 'inactive', 'http://geeksquad.co.zw/plugins/images/users/default_profile_image.png', '', '', '084b6fbb10729ed4da8c3d3f5a3ae7c9');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_information`
--
ALTER TABLE `account_information`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `audit_trail`
--
ALTER TABLE `audit_trail`
  ADD PRIMARY KEY (`action_id`);

--
-- Indexes for table `auto_renewals`
--
ALTER TABLE `auto_renewals`
  ADD PRIMARY KEY (`entry_id`),
  ADD UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  ADD UNIQUE KEY `ibs_id_UNIQUE` (`ibs_id`),
  ADD UNIQUE KEY `entry_id_UNIQUE` (`entry_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`doc_id`);

--
-- Indexes for table `dummy table`
--
ALTER TABLE `dummy table`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `ibs_groups`
--
ALTER TABLE `ibs_groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `messagethreads`
--
ALTER TABLE `messagethreads`
  ADD PRIMARY KEY (`thread_id`),
  ADD UNIQUE KEY `_id` (`thread_id`);

--
-- Indexes for table `message_codes`
--
ALTER TABLE `message_codes`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `other_dummy_table`
--
ALTER TABLE `other_dummy_table`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`txn_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `phone_UNIQUE` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `audit_trail`
--
ALTER TABLE `audit_trail`
  MODIFY `action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `auto_renewals`
--
ALTER TABLE `auto_renewals`
  MODIFY `entry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dummy table`
--
ALTER TABLE `dummy table`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `ibs_groups`
--
ALTER TABLE `ibs_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;
--
-- AUTO_INCREMENT for table `messagethreads`
--
ALTER TABLE `messagethreads`
  MODIFY `thread_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `message_codes`
--
ALTER TABLE `message_codes`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=677;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `txn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1122;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=367;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
