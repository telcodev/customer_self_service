-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 05, 2017 at 10:57 PM
-- Server version: 5.6.37
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ppavppbq_txnsdb`
--

--
-- Dumping data for table `ibs_groups`
--

INSERT INTO `ibs_groups` (`group_id`, `group_name`, `ibs_group_name`, `price`, `quota`, `burst`, `validity`, `desc1`, `desc2`, `popular`, `group_status`) VALUES
(1, 'Velocity 11G', 'Velocity_11G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(2, 'Velocity 12G', 'Velocity_12G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(3, 'Velocity 150G', 'Velocity_150G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(4, 'Velocity 18G', 'Velocity_18G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(5, 'Velocity 25G', 'Velocity_25G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(6, 'Velocity 38G', 'Velocity_38G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(7, 'Velocity 50G', 'Velocity_50G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(8, 'Velocity 65G', 'Velocity_65G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(9, 'Velocity 90G', 'Velocity_90G_Branches', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(10, 'Velocity Fire', 'Fibre-Velocity-Fire', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(11, 'Velocity Ultra', 'Fibre-Velocity-Ultra', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(12, 'Velocity Xtreme', 'Fibre-Velocity-Xtreme', 0, 0, 0, 0, NULL, NULL, NULL, 'active'),
(13, '24 HR', '24_HR', 1, 250, 1, 30, 'Fast Broadband Speed', 'Pay-As-You-Go', 1, 'active'),
(15, '48 HR', '48_HR', 2, 500, 2, 30, 'Fast Broadband Speed', 'Pay-As-You-Go', 0, 'active'),
(16, '72 HR', '72_HR', 3, 1000, 3, 30, 'Fast Broadband Speed', 'Pay-As-You-Go', 1, 'active');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
