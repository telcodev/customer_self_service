<?php

if (!isset($_GET['h']) || empty($_GET['h']) || !isset($_GET['u']) || empty($_GET['u'])) { 
    header("location: login.html?invalid_pwd_reset_link"); // Redirecting To Other Page
}

 $username = mysqli_escape_string($conn, $_GET['u']); // Set email variable
    $hash = mysqli_escape_string($conn, $_GET['h']); // Set hash variable


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script><meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!--alerts CSS -->
        <link href="../plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <!-- Custom CSS -->
        <link href="css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="css/colors/blue.css" id="theme"  rel="stylesheet">
        <link href="../plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet"><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Preloader      -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <section id="wrapper" class="login-register">
            <div class="login-box">
                <div class="white-box">
                   

                    <form class="form-horizontal" method="post" action="set_pwd.php" onsubmit="return passwordValidator();">
                        <input type="hidden" value="<?php echo $username; ?>" name="u">
                        <input type="hidden" value="<?php echo $hash; ?>" name="h">
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <h3>Set new Password</h3>
                                <p class="text-muted">Enter in your new password for the account: <?php echo $username; ?> </p>
                            </div>
                        </div>
                        <div class="form-group ">
                            <h6>Password</h6>
                            <div class="col-xs-12">
                                <input class="form-control" type="password" name="password1" id="password1" required>
                            </div>
                        </div>
                        <div class="form-group ">
                            <h6>Repeat Password</h6>
                            <div class="col-xs-12">
                                <input class="form-control" type="password" name="password2" id="password2" required>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </section>
        
        <script ty="text/javascript">
        function passwordValidator(){
            
            if ($('#password1').val() !== $('#password2').val()) {
                alert('Your passwords do not match. Please verify that your passwords are matching');
                return false;
            }
            
            if ($('#password1').val().length < 8) {
                alert('Your passwords is too short. Make sure it is 8 characters or longer');
                return false;
            }
            return true;
            
        }
        </script>
        <!-- jQuery -->
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!-- Sweet-Alert  -->
        <script src="../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
        <script src="../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.js"></script>
        <!--Style Switcher -->
        <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script src="../plugins/bower_components/toast-master/js/jquery.toast.js">
        </script>
    </body>
</html>
