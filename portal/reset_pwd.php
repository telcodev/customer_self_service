<?php

require('./dbFunctions.php');
$dbFunctions = new DatabaseFunctionsClass();

$email = $_POST['email'];

$userInfo = $dbFunctions->getUserInfoByEmail($email);


if (count($userInfo) < 1) {
    header('location: login.html?pwd_reset_email_not_found');
    exit();
}

//send reset pwd email

$to = $email; // Send email to our user
$subject = 'Password Request | Telco Online TopUp Portal'; // Give the email a subject 
$message = '
                
Hello there 

Someone has requested for a password change on your Telco Online Personal TopUp Portal password!
    
Your account password can be be reset by visiting this link below.
    
If you did not request for a password change, you can ignore this email, your account is still secure.

Follow this URL to access Telco"s Online TopUp portal:

------------------------------------------------
https://selfservice.telco.co.zw/portal/set_password.php?h=' . $userInfo[0]["hash"] . '&u=' . $userInfo[0]["username"]. '
------------------------------------------------
    
'; // Our message above including the link

$headers = 'From:noreply@telco.co.zw' . "\r\n"; // Set from headers

$bool = mail($to, $subject, $message, $headers); // Send our email

$bool ? header('location: login.html?pwd_reset_email_sent') : header('location: login.html?pwd_reset_email_failed');
