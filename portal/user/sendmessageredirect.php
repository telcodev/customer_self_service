<?php

include ('../dbFunctions.php');
$operator = new DatabaseFunctionsClass();

$to = $_POST['to'];
$from = $_SESSION['user_id'];
//$subject = $_POST['subject'];
//$message = $_POST['message'];
$msg_category = $_POST['msg_category'];
//$receiver_user_id;



$portalUserInfo = $operator->getUserInfoByUserID($from);

if ($portalUserInfo[0]['verified'] != 'verified') {
    header('location: home.php?notify=02');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'inactive') {
    header('location: home.php?notify=03');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=86');
    exit();
}

if (empty($portalUserInfo[0]['cst_code'])) {
    header('location: home.php?notify=86');
    exit();
}

$article_id = $_POST['article_id'];
$customer_cst = $portalUserInfo[0]['cst_code'];
$from_email = $portalUserInfo[0]['email'];
$to_email = $to;
$subject = $_POST['subject'];
$article_body = $_POST['message'];


if (isset($article_id) && $article_id > 0) {
    //get tickets from otrs
    $service_address = 'https://hotspot.openaccess.co.zw/otrs_api/csp_portal/add_article_to_ticket_by_other_article_id.php';
    $data = array(
        'article_id' => $article_id,
        'customer_cst' => $customer_cst,
        'from_email' => $from_email,
        'to_email' => $to_email,
        'subject' => $subject,
        'article_body' => $article_body
    );
    $json = $operator->CallAPI('POST', $service_address, $data);
    $article_info = json_decode($json, true);
} else {
    
//    $operator->sendMessage($receiver_user_id, $from, $subject, $message, $msg_category);
    switch ($msg_category) {
        case 'billing':
            $queue_id = 5;
            break;
        case 'support':
            $queue_id = 16;
            break;
        case 'sales':
            $queue_id = 23;
            break;
        case 'fault':
            $queue_id = 6;
            break;
        default:
            $queue_id = 4;
            break;
    }

    //get tickets from otrs
    $service_address = 'https://hotspot.openaccess.co.zw/otrs_api/csp_portal/create_ticket_with_article.php';
    $data = array(
        'queue_id' => $queue_id,
        'customer_cst' => $customer_cst,
        'from_email' => $from_email,
        'to_email' => $to_email,
        'subject' => $subject,
        'article_body' => $article_body
    );
    $json = $operator->CallAPI('POST', $service_address, $data);
    $article_info = json_decode($json, true);
}


//$operator->sendMessage($receiver_user_id, $from, $subject, $message, $msg_category);
//switch ($msg_category) {
//    case 'billing':
//        mail('billing@telco.co.zw', 'Csutomer Service Portal Request - ' . $subject, $message);
//        break;
//    case 'support':
//        mail('noc@telco.co.zw', 'Csutomer Service Portal Request - ' . $subject, $message);
//        break;
//    case 'sales':
//        mail('sales@telco.co.zw', 'Csutomer Service Portal Request - ' . $subject, $message);
//        break;
//    case 'fault':
//        mail('sales@telco.co.zw', 'Csutomer Service Portal Fault Report Request - ' . $subject, $message);
//        break;
//}

header('location: inbox.php?notify=13');
