<?php

session_start();



include '../dbFunctions.php';
$operator = new DatabaseFunctionsClass();


//check that the account we are about to wallet recharge is a valid account
$portalUserInfo = $operator->getUserInfoByUserID($_SESSION['user_id']);

if ($portalUserInfo[0]['verified'] != 'verified') {
    header('location: home.php?notify=02');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'inactive') {
    header('location: home.php?notify=03');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=86');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'pending_support') {
    header('location: home.php?notify=04');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'deleted') {
    header('location: home.php?notify=05');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=06');
    exit();
}
if ($portalUserInfo[0]['account_status'] != 'active') {
    header('location: home.php?notify=07');
    exit();
}
if ($portalUserInfo[0]['ibs_id'] == '' || $portalUserInfo[0]['ibs_id'] == null) {
    header('location: home.php?notify=08');
    exit();
}


$token = 't3lc0zss';
$amount = $_POST['amount'];
$groupname = $_POST['group'];


$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';
$data = array('token' => $token, 'user_id' => $ibs_id);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsUserInfo = json_decode($json, true);

if ($ibsUserInfo['basic_info']['deposit'] < $amount) {
    header('location: home.php?notify=11');
    exit();
}


$credit = $ibsUserInfo['basic_info']['credit'];
$deposit = $ibsUserInfo['basic_info']['deposit'];


/////////////////////////////////////////////////////////////////////////////////

$existingCredit = $ibsUserInfo['basic_info']['credit'];
$deposit = $ibsUserInfo['basic_info']['deposit'];

//change the group under here
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_change_user_group.php';
$data = array('token' => $token, 'user_id' => $ibs_id, 'group_name' => $groupname);
$json = $operator->CallAPI('POST', $service_address, $data);
$info = json_decode($json, true);

if ($info[0] != true) {
    header('location: home.php?notify=16');
    exit();
}

// renew the group they have been put into below here, to simulate them getting fresh credit
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_renew_package.php';
$data = array('token' => $token, 'user_ids' => $ibs_id, 'comment' => 'changing user group, discarding all credit they used to have');
$json = $operator->CallAPI('POST', $service_address, $data);
$response = json_decode($json, true);

if ($response[0] != true) {
    header('location: home.php?notify=17');
    exit();
}

//deduct amount they paying
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';
$data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => -$amount, 'topup_type' => 'ADD', 'comment' => 'saving existing credit as user upgraded their package');
$json = $operator->CallAPI('POST', $service_address, $data);
$res = json_decode($json, true);

if ($res[0] != true) {
    header('location: home.php?notify=18');
    exit();
}

//restore the credit they already had
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';
$data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => $existingCredit, 'topup_type' => 'ADD', 'comment' => 'empty comment');
$json = $operator->CallAPI('POST', $service_address, $data);
$reply = json_decode($json, true);


if ($reply[0] == 'true') {

    include '../dbFunctions.php';
    $db = mysql_select_db($dbname, $conn);
    $insertSql = "INSERT INTO transactions (user_id, txn_status, additional_info) VALUES('" . $_SESSION['user_id'] . "','complete', 'Package Upgrade >> Wallet')";
    $query = mysql_query($insertSql, $conn);

    header('location: home.php?notify=19');
} else {
    header('location: home.php?notify=20');
}