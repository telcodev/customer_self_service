<?php
session_start();

/*
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
 */

include("../dbconnect.php");
include("../dbFunctions.php");

$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists
if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}


// assign the seesion variables to local variables
$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];


// we need to log the person out if we realise the session is not longer active
if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}


//get user information from ibs
$token = 't3lc0zss';
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';
$data = array('token' => $token, 'user_id' => $ibs_id);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsUserInfo = json_decode($json, true);


//get the user's group info from ibs
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';
$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsGroupInfo = json_decode($json, true);


//get the further's group info from ibs in order to get the data left
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';
$data = array('token' => $token, 'user_id' => $ibs_id);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsDataInfo = json_decode($json, true);


//get user info from our db
$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../../plugins/images/favicon.png">
    <title>Telco - Online Personal Portal</title>
    <!-- Bootstrap Core CSS -->
    <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="../css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../css/style.css" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <link href="../css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="fix-sidebar" onload="choosePurchasesToShow()">
<!-- Preloader            -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">

    <?php
    require './_nav.php';
    require './_modals.php';
    ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Home</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="logoutredirect.php">Log Out</a></li>
                        <li class="active">Home Page</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- page content begins here -->

            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Data Table</h3>
                    <p class="text-muted m-b-30">Contracts List</p>
                    <div class="table-responsive">
                        <table id="myTable" class="table table-striped">
                            <thead>
                            <tr>
                                <th style="display:none;">Invoice ID</th>
                                <th>Name</th>
                                <th>Balance</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Date of Next Invoice</th>
                                <th>Customer Code</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $operator->printContractTable($odooContractInfo['payload']);
                              // echo $odooContractInfo['payload'];
                               //echo '<pre>'; print_r($odooContractInfo['payload']); echo '</pre>';
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <!-- page content ends here -->
            <!-- .right-sidebar -->

            <!-- /.right-sidebar -->
        </div>
        <!-- /.container-fluid -->


        <?php
        require './_notifyier.php';
        require './_footer.php';
        ?>

        <!-- /#wrapper -->

        <!-- Bootstrap Core JavaScript -->
        <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="../js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="../js/waves.js"></script>

        <!--Counter js -->

        <script src="../../plugins/bower_components/toast-master/js/jquery.toast.js"></script>
        <script src="../js/toastr.js"></script>

        <!-- Sweet-Alert  -->
        <script src="../../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
        <script src="../../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>

        <!--Morris JavaScript -->
        <script src="../../plugins/bower_components/raphael/raphael-min.js"></script>
        <script src="../../plugins/bower_components/morrisjs/morris.js"></script>

        <!-- jQuery for carousel -->
        <script src="../../plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
        <script src="../../plugins/bower_components/owl.carousel/owl.custom.js"></script>

        <script src="../../plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
        <script src="../../plugins/bower_components/counterup/jquery.counterup.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/custom.js"></script>
        <script src="../js/widget.js"></script>
        <!--Style Switcher -->
        <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script type="text/javascript">

            function setAutoRenew(user_id, ibs_id) {
                var setorunset = document.getElementById('checkbox-autorenew').checked;
                var userid = user_id;
                var ibsid = ibs_id;
                var change = null;

                if (setorunset) {
                    change = 'ADD';
                } else {
                    change = 'REMOVE';
                }

                $.ajax({//create an ajax request to load_page.php
                    type: "GET",
                    url: "https://selfservice.telco.co.zw/portal/user/set_auto_renew.php",
                    data: {change: change, user_id: userid, ibs_id: ibsid},
                    dataType: "json", //expect html to be returned
                    success: function (response) {

                        ress = JSON.stringify(response);
                        res = JSON.parse(ress);
                        result = res['result'];

                        console.log(ress);

                    }

                });


            }

        </script>

        <script>

            function choosePurchasesToShow() {

                var dateSelected = document.getElementById('sDate').value;

                document.getElementById('purchaseHistoryHeader').innerHTML = dateSelected.toString().substring(0, 4) + " " + dateSelected.substring(7, 12);

                var table = document.getElementById("txn-table");


                for (var r = 1, n = table.rows.length; r < n; r++) {

                    var rowDate = table.rows[r].cells[4].innerHTML;

                    if (rowDate.toString().substring(5, 7) != dateSelected.toString().substring(4, 6)) {

                        table.rows[r].setAttribute("style", "display: none;");

                    } else {

                        table.rows[r].setAttribute("style", "display: in-line;");
                    }

                }
            }

        </script>

    </div>
</body>
</html>

