<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists
if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}


// assign the seesion variables to local variables
$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];


// we need to log the person out if we realise the session is not longer active
if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}



//get user information from ibs
$token = 't3lc0zss';
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';
$data = array('token' => $token, 'user_id' => $ibs_id);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsUserInfo = json_decode($json, true);


//get the user's group info from ibs
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';
$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsGroupInfo = json_decode($json, true);


//get the further's group info from ibs in order to get the data left
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';
$data = array('token' => $token, 'user_id' => $ibs_id);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsDataInfo = json_decode($json, true);


//get user info from our db
$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];


?>





<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fix-sidebar" onload="choosePurchasesToShow()">
        <!-- Preloader            -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php
            require './_nav.php';
            require './_modals.php';
            ?>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Home</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="logoutredirect.php">Log Out</a></li>
                                <li class="active">Home Page</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <!-- page content begins here -->


                    <div style="margin: auto; max-width: 728px;">
                        <div class="news-slide m-b-15">
                            <div class="vcarousel slide">
                                <!-- Carousel items -->
                                <div class="carousel-inner"> 
                                    <?php $operator->populateAdsOnAdsWidget(null); ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%"  src="<?php echo $profileimageurl; ?>" alt="user" >
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="my_profile.php">
                                            <img id = "profileImageDashboard" alt="Go to your profile page" class="thumb-lg img-circle" src="<?php echo $profileimageurl; ?>">
                                        </a>
                                        <h4 class="text-white" id = "nameDashboard"><?php echo $name . " " . $surname ?></h4>
                                        <h5 class="text-white"  id = "usernameDashboard">
                                            <?php
                                            echo $username;
                                            if ($portalUserInfo[0]["cst_code"] != '') {
                                                echo ' (' . $portalUserInfo[0]["cst_code"] . ')';
                                            }
                                            ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>                             
                            <div class="user-btm-box">
                                <div class="stats-row col-md-12 m-t-20 m-b-0 text-center">
                                    <div class="stat-item">
                                        <h6>Contact info</h6>
                                        <hr/>
                                        <b id = "phoneDashboard"><i class="ti-mobile"></i><?php echo $phone; ?></b></div>

                                </div>
                            </div>
                        </div>
                    </div>





                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Package you're on</h3>
                            <div class="text-right"> <span class="text-muted">
                                    <?php
                                    if (strcmp($ibsUserInfo['basic_info']['status'], 'Recharged') == 0)
                                        $additionalText = "(Out-of-Bundle)";
                                    if (strcmp($ibsUserInfo['basic_info']['status'], 'Package') == 0)
                                        $additionalText = "(In-Bundle)";

                                    echo "<b>" . $ibsUserInfo['basic_info']['group_name'] . "</b> " . $additionalText;
                                    ?></span>
                                <h1><sup><i class="fa fa-money text-success"> </i></sup>$<?php
                                    if ($portalUserInfo[0]['ibs_id'] != '') {
                                        echo round($ibsUserInfo['basic_info']['credit'], 2);
                                    } else {
                                        echo '--';
                                    }
                                    ?></h1>
                            </div>
                            <span class="text-success"><?php
                                if ($portalUserInfo[0]['ibs_id'] != '') {
                                    //  echo $info['basic_info']['credit']/$groupinfo['group_info'][1]['raw_attrs']['group_credit']*100; 
                                    echo round($ibsDataInfo[1][$ibsGroupInfo['group_info'][1]['attrs']['charge']][1] / 1024 / 1024, 2); // to convert to megabytes; 
                                } else {
                                    echo '--';
                                }
                                ?>MB remaining</span> 
                            <div class="progress m-b-0">
                                <div class="progress-bar <?php
                                $color = 'progress-bar-success';
                                $percentageLeft = $ibsUserInfo['basic_info']['credit'] / $ibsGroupInfo['group_info'][1]['raw_attrs']['group_credit'] * 100;

                                if ($percentageLeft < 50)
                                    $color = 'progress-bar-warning';

                                if ($percentageLeft < 15)
                                    $color = 'progress-bar-danger';

                                echo $color;
                                ?>" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ibsUserInfo['basic_info']['credit'] / $ibsGroupInfo['group_info'][1]['raw_attrs']['group_credit'] * 100; ?>%;"> <span class="sr-only">20% Complete</span> </div>
                            </div> 

                            <hr>

                            <div class="checkbox checkbox-primary p-t-0">
                                <input onclick="setAutoRenew(<?php echo "'" . $user_id . "'" . "," . "'" . $ibs_id . "'"; ?>);" id="checkbox-autorenew" type="checkbox" name = "autorenewcheckbox"
                                <?php
                                $arr = $operator->getAutoRenewUserByUserId($user_id);
                                if (sizeof($arr) == 1) {
                                    echo 'checked = "checked"';
                                }
                                ?> > <label for="checkbox-autorenew"><a href="#" data-toggle='modal' data-target='.autorenew-explained-modal'>Auto Renew</a> on credit finish</label>
                            </div> 

                            <!-- autorenew explained Modal -->
                            <div id="autorenew-explained-modal" class="modal fade autorenew-explained-modal" role="dialog">
                                <div class="modal-dialog modal-md">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                            <h4 class="modal-title" id="myLargeModalLabel">Auto Renew function explained.</h4>
                                        </div>
                                        <div class="modal-body">
                                            <!-- The content within the modal comes here -->
                                            <p>When you enable this option, the system will automatically renew your credit balance when it run outs. This is only possible if your credit balance remains more than or equal to the cost of your current package.</p>
                                            <!-- The content within the modal ends here -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr/>          

                            <h3 class="box-title">Wallet Balance</h3>
                           <div class="text-right"> <!--  <span class="text-muted"><?php echo $ibsUserInfo['basic_info']['group_name']; ?></span> -->
                                <h1><sup><i class="fa fa-credit-card text-success"> </i></sup>$<?php
                                    if ($portalUserInfo[0]['ibs_id'] != '') {
                                        echo round($ibsUserInfo['basic_info']['deposit'], 2);
                                    } else {
                                        echo '--';
                                    }
                                    ?></h1>
                            </div>
                           <!-- <span class="text-success">20%</span> -->
                            <div class="progress m-b-0">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">20% Complete</span> </div>
                            </div>            
                            <hr>
                            <div class = "text-right">
                                <?php
                                if ($ibsUserInfo['basic_info']['nearest_exp_date'] < date('Y-m-d h:i') || $ibsUserInfo['basic_info']['credit'] == 0 || $ibsUserInfo['basic_info']['status'] == 'Recharged')
                                    echo '<span><a href = "" data-toggle="modal" data-target=".renew-package-modal" ><b>Renew Package</b></a></span> |';
                                ?>

                                <span><a href = "#" data-toggle='modal' data-target='.upgrade-package-modal'>Upgrade Package</a></span>
                            </div>
                        </div>
                    </div>

                    <!-- upgrade package Modal -->
                    <div id="upgrade-package-modal" class="modal fade upgrade-package-modal" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                    <h4 class="modal-title" id="myLargeModalLabel">Your package is upgradeable to the following packages.</h4>
                                </div>
                                <div class="modal-body">

                                    <!-- The content within the modal comes here -->

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="white-box">
                                                <div class="row pricing-plan">
                                                    <div class="centraliser" style="text-align: center;">
                                                        <?php
                                                        $operator->populateUpgradebaleGroups($ibsGroupInfo['group_info'][1]['raw_attrs']['group_credit']);
                                                        ?>
                                                    </div >
                                                </div>
                                            </div>
                                        </div>
                                    </div> 


                                    <!-- The content within the modal ends here -->

                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
                        <a href = "#" data-toggle="modal" data-target=".wallet-top-up-modal">
                            <div class="white-box text-center bg-purple">
                                <b><h2 class="text-white counter"><i class="icon-wallet"></i>Wallet Recharge</h2></b>
                                <p class="text-white">Need more credit? You can use your wallet balance to top uo your credit in the future, should you run out. Top up your wallet here.</p>        
                            </div>
                        </a>

                        <a href = "#" data-toggle="modal" data-target=".credit-purchase-using-wallet-modal">
                            <div class="white-box text-center bg-info">
                                <h2 class="text-white counter"><i class="icon-refresh"></i>Credit Purchase</h2>
                                <p class="text-white">Get credit using your wallet balance in order to continue browsing.</p>
                            </div>
                        </a>      
                    </div>


                    <!-- top up history begins here-->

                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Top Up History
                                <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
                                    <select id = 'sDate' oninput="choosePurchasesToShow()" class="form-control pull-right row b-none">
                                        <option value = '<?php echo date('M m Y'); ?>'> <?php echo date('M Y'); ?></option>
                                        <option value = '<?php echo date('M m Y', strtotime("-1 month")); ?>'> <?php echo date('M Y', strtotime("-1 month")); ?></option>
                                        <option value = '<?php echo date('M m Y', strtotime("-2 month")); ?>'> <?php echo date('M Y', strtotime("-2 month")); ?></option>
                                        <option value = '<?php echo date('M m Y', strtotime("-3 month")); ?>'> <?php echo date('M Y', strtotime("-3 month")); ?></option>
                                        <option value = '<?php echo date('M m Y', strtotime("-4 month")); ?>'> <?php echo date('M Y', strtotime("-4 month")); ?></option>

                                        <?php $operator->logger(date('M m Y', strtotime("-3 month"))) ?>   

                                    </select>
                                </div>
                            </h3>
                            <div class="row sales-report">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h2 id = 'purchaseHistoryHeader'><?php echo date('M Y'); ?></h2>
                                    <p>PURCHASE HISTORY</p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 ">
                                    <h1 class="text-right text-success m-t-20">$<?php echo round($ibsUserInfo['basic_info']['credit'], 2); ?></h1>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id = "txn-table">
                                    <thead>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody id = 'phbody'>

                                        <!--Start adding purchase history rows here-->
                                        <?php $operator->phpPopulatePurchaseHistory($_SESSION['user_id']); ?>
                                        <!--Stop adding purchase history rows here-->


                                    </tbody>
                                </table>
                                <a href="full_purchase_history.php" class = "showbottom">View all purchase history</a> </div>
                        </div>
                    </div>




                    <!-- page content ends here -->
                    <!-- .right-sidebar -->

                    <!-- /.right-sidebar -->
                </div>
                <!-- /.container-fluid -->





                <?php
                require './_notifyier.php';
                require './_footer.php';
                ?>

                <!-- /#wrapper -->

                <!-- Bootstrap Core JavaScript -->
                <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
                <!-- Menu Plugin JavaScript -->
                <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
                <!--slimscroll JavaScript -->
                <script src="../js/jquery.slimscroll.js"></script>
                <!--Wave Effects -->
                <script src="../js/waves.js"></script>

                <!--Counter js -->

                <script src="../../plugins/bower_components/toast-master/js/jquery.toast.js"></script>
                <script src="../js/toastr.js"></script>

                <!-- Sweet-Alert  -->
                <script src="../../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
                <script src="../../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>

                <!--Morris JavaScript -->
                <script src="../../plugins/bower_components/raphael/raphael-min.js"></script>
                <script src="../../plugins/bower_components/morrisjs/morris.js"></script>

                <!-- jQuery for carousel -->
                <script src="../../plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
                <script src="../../plugins/bower_components/owl.carousel/owl.custom.js"></script>

                <script src="../../plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
                <script src="../../plugins/bower_components/counterup/jquery.counterup.min.js"></script>

                <!-- Custom Theme JavaScript -->
                <script src="../js/custom.js"></script>
                <script src="../js/widget.js"></script>
                <!--Style Switcher -->
                <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
                <script type = "text/javascript" >

                                    function setAutoRenew(user_id, ibs_id) {
                                        var setorunset = document.getElementById('checkbox-autorenew').checked;
                                        var userid = user_id;
                                        var ibsid = ibs_id;
                                        var change = null;

                                        if (setorunset) {
                                            change = 'ADD';
                                        } else {
                                            change = 'REMOVE';
                                        }

                                        $.ajax({//create an ajax request to load_page.php
                                            type: "GET",
                                            url: "https://selfservice.telco.co.zw/portal/user/set_auto_renew.php",
                                            data: {change: change, user_id: userid, ibs_id: ibsid},
                                            dataType: "json", //expect html to be returned                
                                            success: function (response) {

                                                ress = JSON.stringify(response);
                                                res = JSON.parse(ress);
                                                result = res['result'];

                                                console.log(ress);

                                            }

                                        });


                                    }

                </script>

                <script>

                    function choosePurchasesToShow() {

                        var dateSelected = document.getElementById('sDate').value;

                        document.getElementById('purchaseHistoryHeader').innerHTML = dateSelected.toString().substring(0, 4) + " " + dateSelected.substring(7, 12);

                        var table = document.getElementById("txn-table");


                        for (var r = 1, n = table.rows.length; r < n; r++) {

                            var rowDate = table.rows[r].cells[4].innerHTML;

                            if (rowDate.toString().substring(5, 7) != dateSelected.toString().substring(4, 6)) {

                                table.rows[r].setAttribute("style", "display: none;");

                            } else {

                                table.rows[r].setAttribute("style", "display: in-line;");
                            }

                        }
                    }

                </script>

            </div>
    </body>
</html>

