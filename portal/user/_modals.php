
<!-- wallet top up modal starts here-->
<!-- this is the header of the modal-->
<div class="modal fade wallet-top-up-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="myLargeModalLabel">How much would you like to add to your wallet?</h4>
            </div>
            <div class="modal-body">

                <!-- The content within the modal comes here -->



                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row pricing-plan">


                                <!-- row 1 starts here--> 

                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-l">
                                            <div class="pricing-header">
                                                <h4 class="text-center">...</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>5.00</h2>
                                                <p class="uppercase">...</p>
                                            </div>
                                            <div class="price-table-content">   
                                                <div class="price-row">               
                                                    <form action = "paynowredirect_add_to_wallet.php" method = "post">
                                                        <input type = "hidden" name = "amount" value = "5.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Buy Now </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 1 ends here-->  
                                <!-- row 2 starts here--> 

                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-l">
                                            <div class="pricing-header">
                                                <h4 class="text-center">...</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>10.00</h2>
                                                <p class="uppercase">...</p>
                                            </div>
                                            <div class="price-table-content">   
                                                <div class="price-row">               
                                                    <form action = "paynowredirect_add_to_wallet.php" method = "post">
                                                        <input type = "hidden" name = "amount" value = "10.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Buy Now </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 2 ends here-->  
                                <!-- row 3 starts here--> 

                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-l">
                                            <div class="pricing-header">
                                                <h4 class="text-center">...</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>20.00</h2>
                                                <p class="uppercase">...</p>
                                            </div>
                                            <div class="price-table-content">   
                                                <div class="price-row">               
                                                    <form action = "paynowredirect_add_to_wallet.php" method = "post">
                                                        <input type = "hidden" name = "amount" value = "20.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Buy Now </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 3 ends here-->  
                                <!-- row 4 starts here-->  

                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-l">
                                            <div class="pricing-header">
                                                <h4 class="text-center">...</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>50.00</h2>
                                                <p class="uppercase">...</p>
                                            </div>
                                            <div class="price-table-content">   
                                                <div class="price-row">               
                                                    <form action = "paynowredirect_add_to_wallet.php" method = "post">
                                                        <input type = "hidden" name = "amount" value = "50.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Buy Now </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 4 ends here-->  


                            </div>
                        </div>
                    </div> 


                    <!-- The content within the modal ends here -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <!-- wallet top up modal ends here-->

</div>

<!-- the modal to choose if you are using wallet or paynow to renew package sits here -->
<div class="modal fade renew-package-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="mySmallModalLabel">Would you like to renew this package?</h4>
            </div>
            <div class="modal-body" style="display: flex; justify-content: center;">
                <?php $operator->populateCurrentGroup($ibsGroupInfo['group_info'][1]['raw_attrs']['group_credit']); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- the modal to choose if you are using wallet or paynow to renew package ends here -->


<!-- wallet push modal starts here-->
<!-- this is the header of the modal-->
<div class="modal fade credit-purchase-using-wallet-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="myLargeModalLabel">How much would you like to add to your credit?</h4>
            </div>
            <div class="modal-body">

                <!-- The content within the modal comes here -->



                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row pricing-plan">


                                <!-- row 1 starts here--> 



                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-l">
                                            <div class="pricing-header">
                                                <h4 class="text-center">...</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>5.00</h2>
                                                <p class="uppercase">...</p>
                                            </div>
                                            <div class="price-table-content">   
                                                <div class="price-row">               
                                                    <form action = "walletpay.php" method = "post">
                                                        <input type="hidden" name = "amount" value = "5.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                    </form>
                                                    <form action = "paynowredirect_add_to_credit.php" method = "post">
                                                        <input type = "hidden" name = "amount" value = "5.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 1 ends here-->  
                                <!-- row 2 starts here--> 



                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-l">
                                            <div class="pricing-header">
                                                <h4 class="text-center">...</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>10.00</h2>
                                                <p class="uppercase">...</p>
                                            </div>
                                            <div class="price-table-content">   
                                                <div class="price-row">               
                                                    <form action = "walletpay.php" method = "post">
                                                        <input type="hidden" name = "amount" value = "10.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                    </form>
                                                    <form action = "paynowredirect_add_to_credit.php" method = "post">
                                                        <input type = "hidden" name = "amount" value = "10.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!-- row 2 ends here-->  
                                <!-- row 3 starts here--> 


                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-l">
                                            <div class="pricing-header">
                                                <h4 class="text-center">...</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>20.00</h2>
                                                <p class="uppercase">...</p>
                                            </div>
                                            <div class="price-table-content">   
                                                <div class="price-row">               
                                                    <form action = "walletpay.php" method = "post">
                                                        <input type="hidden" name = "amount" value = "20.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                    </form>
                                                    <form action = "paynowredirect_add_to_credit.php" method = "post">
                                                        <input type = "hidden" name = "amount" value = "20.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!-- row 3 ends here-->  
                                <!-- row 4 starts here-->  



                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-l">
                                            <div class="pricing-header">
                                                <h4 class="text-center">...</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>50.00</h2>
                                                <p class="uppercase">...</p>
                                            </div>
                                            <div class="price-table-content">   
                                                <div class="price-row">               
                                                    <form action = "walletpay.php" method = "post">
                                                        <input type="hidden" name = "amount" value = "50.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                    </form>
                                                    <form action = "paynowredirect_add_to_credit.php" method = "post">
                                                        <input type = "hidden" name = "amount" value = "50.00"/>
                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- row 4 ends here-->  


                            </div>
                        </div>
                    </div> 


                    <!-- The content within the modal ends here -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- wallet push modal ends here-->
</div>

