<?php

session_start();



include '../dbFunctions.php';
$operator = new DatabaseFunctionsClass();


//check that the account we are about to wallet recharge is a valid account
$portalUserInfo = $operator->getUserInfoByUserID($_SESSION['user_id']);

if ($portalUserInfo[0]['verified'] != 'verified') {
    header('location: home.php?notify=02');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'inactive') {
    header('location: home.php?notify=03');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=86');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'pending_support') {
    header('location: home.php?notify=04');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'deleted') {
    header('location: home.php?notify=05');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=06');
    exit();
}
if ($portalUserInfo[0]['account_status'] != 'active') {
    header('location: home.php?notify=07');
    exit();
}
if ($portalUserInfo[0]['ibs_id'] == '' || $portalUserInfo[0]['ibs_id'] == null) {
    header('location: home.php?notify=08');
    exit();
}


$token = 't3lc0zss';

$amount = $_POST['amount'];

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);

if ($ibsUserInfo['basic_info']['deposit'] < $amount) {

    header('location: home.php?notify=11');

    exit();
}


if ($ibsUserInfo['basic_info']['credit'] != 0 && strcmp($ibsUserInfo['basic_info']['status'], "Package") == 0) {

    header('location: home.php?notify=09');

    exit();
}

if ($ibsUserInfo['basic_info']['nearest_exp_date'] != '' && $ibsUserInfo['basic_info']['nearest_exp_date'] < date('Y-m-d')) {

    header('location: home.php?notify=10');

    exit();
}


$credit = $ibsUserInfo['basic_info']['credit'];
$deposit = $ibsUserInfo['basic_info']['deposit'];

$topup_type = 'ADD';
$comment = '';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_credit_to_user.php';

$data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => $amount, 'topup_type' => $topup_type, 'comment' => $comment);

$json = $operator->CallAPI('POST', $service_address, $data);

$ibsUserInfo = json_decode($json, true);


if ($ibsUserInfo[0] == 'true') {

    $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_set_user_deposit.php';

    $data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => -$amount, 'comment' => $comment);

    $json = $operator->CallAPI('POST', $service_address, $data);

    $ibsUserInfo = json_decode($json, true);

    if ($ibsUserInfo[0] == 'true') {

        include '../dbFunctions.php';
        $db = mysql_select_db($dbname, $conn);
        $insertSql = "INSERT INTO transactions (user_id, txn_status, additional_info) VALUES('" . $_SESSION['user_id'] . "','complete', 'Credit Purchase >> Wallet')";
        $query = mysql_query($insertSql, $conn);


        header('location: home.php?notify=21');
    } else {
        header('location: home.php?notify=22');
    }
}
