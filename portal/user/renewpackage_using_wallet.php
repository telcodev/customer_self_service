<?php

include ('../dbFunctions.php');
$operator = new DatabaseFunctionsClass();


//check that the account we are about to wallet recharge is a valid account
$portalUserInfo = $operator->getUserInfoByUserID($_SESSION['user_id']);

if ($portalUserInfo[0]['verified'] != 'verified') {
    header('location: home.php?notify=02');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=86');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'inactive') {
    header('location: home.php?notify=03');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'pending_support') {
    header('location: home.php?notify=04');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'deleted') {
    header('location: home.php?notify=05');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=06');
    exit();
}
if ($portalUserInfo[0]['account_status'] != 'active') {
    header('location: home.php?notify=07');
    exit();
}
if ($portalUserInfo[0]['ibs_id'] == '' || $portalUserInfo[0]['ibs_id'] == null) {
    header('location: home.php?notify=08');
    exit();
}



// assign the session variables to local variables
$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];




//get user information from ibs
$token = 't3lc0zss';
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';
$data = array('token' => $token, 'user_id' => $ibs_id);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsUserInfo = json_decode($json, true);


if ($ibsUserInfo['basic_info']['credit'] != 0 && strcmp($ibsUserInfo['basic_info']['status'], "Recharged") != 0) {
    header('location: home.php?notify=09');
    exit();
}


//get the user's group info from ibs
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';
$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsGroupInfo = json_decode($json, true);

$credit = $ibsUserInfo['basic_info']['credit'];
$deposit = $ibsUserInfo['basic_info']['deposit'];
$groupCredit = $ibsGroupInfo['group_info'][1]['raw_attrs']['group_credit'];


// check if their wallet balance is even enough
if ($deposit < $groupCredit) {
    header('location: home.php?notify=11');
    exit();
}



// renew the package below here
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_renew_package.php';
$data = array('token' => $token, 'user_ids' => $ibs_id, 'comment' => 'empty comment');
$json = $operator->CallAPI('POST', $service_address, $data);
$response = json_decode($json, true);

if ($response[0] != 'true') {
    $operator->logger('user id: ' . $user_id . ' - manual_renew_failed_to_renew_package');
    exit();
}

// deduct the amout used to renew the package
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';
$data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => -$groupCredit, 'topup_type' => $topup_type, 'comment' => 'empty comment');
$json = $operator->CallAPI('POST', $service_address, $data);
$reply = json_decode($json, true);

if ($reply[0] != 'true') {
    $operator->logger('user id: ' . $user_id . ' - manual_renew_failed_to_deduct_amount_used_to_renew');
    exit();
}


// restore the amout they had as their credit balance
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';
$data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => $credit, 'topup_type' => $topup_type, 'comment' => 'empty comment');
$json = $operator->CallAPI('POST', $service_address, $data);
$reply = json_decode($json, true);

if ($reply[0] != 'true') {
    $operator->logger('user id: ' . $user_id . ' - manual_renew_failed_to_deduct_amount_used_to_renew');
    exit();
}

$date = date('Y-m-d');
$time = date('h:i:sa');


include ("../dbconnect.php");
$db = mysql_select_db($dbname, $conn);

$sql = "INSERT INTO transactions (user_id, txn_reference_number, txn_amount, additional_info, txn_status, txn_date, txn_time)
VALUES ('" . $user_id . "','" . $receipt_no . "','" . $groupCredit . "', 'Package renewal >> Wallet', 'complete','" . $date . "', '" . $time . "')";

$bool = mysql_query($sql);

if ($bool != true) {
    $operator->logger('user id: ' . $user_id . ' - manual_renew_failed_to_save_renew_transaction_to_db');
    echo mysql_error();
    exit();
}

header('location: home.php?notify=12');
?>