<?php

session_start();

include '../dbFunctions.php';
$operator = new DatabaseFunctionsClass();

//check that the account we are about to wallet recharge is a valid account
$portalUserInfo = $operator->getUserInfoByUserID($_SESSION['user_id']);

if ($portalUserInfo[0]['verified'] != 'verified') {
    header('location: home.php?notify=02');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'inactive') {
    header('location: home.php?notify=03');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=86');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'pending_support') {
    header('location: home.php?notify=04');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'deleted') {
    header('location: home.php?notify=05');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=06');
    exit();
}
if ($portalUserInfo[0]['account_status'] != 'active') {
    header('location: home.php?notify=07');
    exit();
}
if ($portalUserInfo[0]['ibs_id'] == '' || $portalUserInfo[0]['ibs_id'] == null) {
    header('location: home.php?notify=08');
    exit();
}
if ($portalUserInfo[0]['cst_code'] == '' || $portalUserInfo[0]['cst_code'] == null) {
    header('location: home.php?notify=08');
    exit();
}

// try to get odoo invoice information
$service_address = 'https://hotspot.openaccess.co.zw/odoo_api/cs_portal/get_customer_invoices.php';
$data = array('cst_code' => $portalUserInfo[0]["cst_code"]);
$json = $operator->CallAPI('GET', $service_address, $data);
$odooInvoiceInfo = json_decode($json, true);

$invoice = null;
foreach ($odooInvoiceInfo['payload'] as $inv) {
    if ($inv['id'] == $_GET['inv_id']) {
        $invoice = $inv;
        break;
    }
}

global $order_id;
$token = 't3lc0zss';

$value = $invoice['amount_total_signed'];
$amount = $value;


$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];


// get ibs account information
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';
$data = array('token' => $token, 'user_id' => $ibs_id);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsUserInfo = json_decode($json, true);


if ($ibsUserInfo['basic_info']['credit'] != 0 && strcmp($ibsUserInfo['basic_info']['status'], "Recharged") != 0) {
    header('location: home.php?notify=09');
    exit();
}


if ($ibsUserInfo['basic_info']['nearest_exp_date'] != '' && $ibsUserInfo['basic_info']['nearest_exp_date'] < date('Y-m-d')) {
    header('location: home.php?notify=10');
    exit();
}


include ("../dbconnect.php");
// Selecting Database
$db = mysql_select_db($dbname, $conn);
$insertSql = "INSERT INTO transactions (user_id, txn_status, additional_info) VALUES('" . $_SESSION['user_id'] . "','initiate', 'Paynow >> Invoice: ".$invoice['display_name'].' '.$invoice['id']."')";
$query = mysql_query($insertSql, $conn);
$order_id = mysql_insert_id();

echo "


<form id='frm' action='paynowFunctions_pay_invoice.php?action=createtransaction' method='post'>

            <input type='hidden' name='resulturl' value='https://selfservice.telco.co.zw/portal/user/paynowFunctions_pay_invoice.php?action=notify&order_id=" . $order_id . "&session_id=" . $_SESSION['session_id'] . "' />
            <input type='hidden' name='returnurl' value='https://selfservice.telco.co.zw/portal/user/paynowFunctions_pay_invoice.php?action=return&order_id=" . $order_id . "&session_id=" . $_SESSION['session_id'] . "' />
            <input type='hidden' name='reference' value='Order Number: " . $order_id . "'/>
            <input type='hidden' name='amount' value='" . $value . "'/>
            <input type='hidden' name='additionalinfo' value='Some Info: lets start this action.'/>
            <input type='hidden' name='order_id' value='" . $order_id . "'/>
            If you are not automatically redirected
            <button type='submit'>Click Here</button>
</form>



 <script type='text/javascript'>document.getElementById('frm').submit(); // SUBMIT FORM </script>
";
