<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


$change = $_GET['change'];
$user_id = $_GET['user_id'];
$ibs_id = $_GET['ibs_id'];

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();


$reply = $operator->setAutoRenew($change, $user_id, $ibs_id);

echo json_encode(array('result' => $reply), JSON_UNESCAPED_SLASHES);
