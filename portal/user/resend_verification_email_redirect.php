<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include ("../dbFunctions.php");
$operator = new DatabaseFunctionsClass();


$user_id = $_GET['user_id'];
$portalUserInfo = $operator->getUserInfoByUserID($user_id);

//send activation email

$to = $portalUserInfo[0]['email']; // Send email to our user
$email = $portalUserInfo[0]['email'];
$hash = $portalUserInfo[0]['hash'];
$username = $portalUserInfo[0]['username'];
$password = $portalUserInfo[0]['password'];
$subject = 'Signup | Verification'; // Give the email a subject 
$message = '
 
Thanks for signing up with Telco"s Online Personal TopUp Portal!

Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.
 
------------------------
Username: ' . $username . '
Password: ' . $password . '
------------------------
 
Please click this link to activate your account:
https://selfservice.telco.co.zw/portal/verify.php?email=' . $email . '&hash=' . $hash . '
 
'; // Our message above including the link

$headers = 'From:noreply@telco.co.zw' . "\r\n"; // Set from headers
$bool = mail($to, $subject, $message, $headers); // Send our email

header('location: home.php');


