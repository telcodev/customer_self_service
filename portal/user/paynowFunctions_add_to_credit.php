<?php

session_start();

/* * *******************************************
  1. Define Constants
 * ******************************************* */
define('ps_error', 'Error');
define('ps_ok', 'Ok');
define('ps_created_but_not_paid', 'created but not paid');
define('ps_cancelled', 'cancelled');
define('ps_failed', 'failed');
define('ps_paid', 'paid');
define('ps_awaiting_delivery', 'awaiting delivery');
define('ps_delivered', 'delivered');
define('ps_awaiting_redirect', 'awaiting redirect');
define('site_url', 'https://selfservice.telco.co.zw');
/* * *******************************************
  2. sitewide variables, settings
 * ******************************************* */
$order_id = $_POST['order_id']; //current shopping session ID, we set it down there later (this is managed by your shopping cart)

$integration_id = '2640';
$integration_key = '3b1d258f-aba2-4c58-a85d-203697202063'; //oops this MUST BE SECRET, take it from a Database, encrypted or something
//$integration_id = '2607';
//$integration_key = '802996bd-710a-4e5a-9fdd-d69eeae3005f'; //oops this MUST BE SECRET, take it from a Database, encrypted or something

$authemail = 'jayden@telco.co.zw';
$initiate_transaction_url = 'https://www.paynow.co.zw/Interface/InitiateTransaction';
$orders_data_file = 'ordersdata.ini';
$checkout_url = site_url . '/product-purchase.html';
/* * *******************************************
  3. site routing
 * ******************************************* */
$action = isset($_GET['action']) ? $_GET['action'] : 'default';
switch ($action) {
    case 'createtransaction' : //create or initiate a transaction on paynow
        WereCreatingATransaction();
        break;
    case 'return' : //entry point when returning/redirecting from paynow
        WereBackFromPaynow();
        break;
    case 'notify' : //listen for transaction-status paynow
        PaynowJustUpdatingUs();
        break;
    default : //default
        JustTheDefault();
        break;
}

//route entry functions
function JustTheDefault() {
    global $order_id;
//Unique Order Id for current shopping session
    $order_id = rand(1, 60); //lets just use simple numbers here, 1-60, but yours should be the order number of the current shopping session, one you can use to identify this particular transaction with for eternity, in your shopping cart.
    getShoppingCartHTML();
}

function WereBackFromPaynow() {
    global $integration_id;
    global $integration_key;
    global $checkout_url;
    global $orders_data_file;
    $local_order_id = $_GET['order_id'];
//Lets get our locally saved settings for this order
    $orders_array = array();
    if (file_exists($orders_data_file)) {
        $orders_array = parse_ini_file($orders_data_file, true);
    }
    $order_data = $orders_array['OrderNo_' . $local_order_id];
    $ch = curl_init();
//set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $order_data['pollurl']);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, '');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//execute post
    $result = curl_exec($ch);
    if ($result) {
//close connection
        $msg = ParseMsg($result);
        $MerchantKey = $integration_key;
        $validateHash = CreateHash($msg, $MerchantKey);
        if ($validateHash != $msg["hash"]) {
            header("Location: $checkout_url");
        } else {
            /*             * *** IMPORTANT ****
              On Paynow, payment status has changed, say from Awaiting Delivery to Delivered

              Here is where you
              1. Update your local shopping cart of Payment Status etc and do appropriate actions here, Save data to DB
              2. Email, SMS Notifications to customer, merchant etc
              3. Any other thing

             * ** END OF IMPORTANT *** */


            include ("../dbconnect.php");
// Selecting Database
            $db = mysql_select_db($dbname, $conn);
            $sql = "UPDATE transactions SET txn_status = 'finalising' WHERE txn_id = '" . $local_order_id . "'";

//1. Lets write the updated settings
            $orders_array['OrderNo_' . $local_order_id] = $msg;
            $orders_array['OrderNo_' . $local_order_id]['returned_from_paynow'] = 'yes';
            write_ini_file($orders_array, $orders_data_file, true);
        }
    }
//Thank	your customer
    getBackFromPaynowHTML($_GET['session_id'], $local_order_id);
}

function PaynowJustUpdatingUs() {
    global $integration_id;
    global $integration_key;
    global $checkout_url;
    global $orders_data_file;
    $local_order_id = $_GET['order_id'];
//write a file to show that paynow silently visisted us sometime
    file_put_contents('sellingmilk_log.txt', date('d m y h:i:s') . '   Paynow visited us for order id ' . $local_order_id . '\n', FILE_APPEND | LOCK_EX);
//Lets get our locally saved settings for this order
    $orders_array = array();
    if (file_exists($orders_data_file)) {
        $orders_array = parse_ini_file($orders_data_file, true);
    }
    $order_data = $orders_array['OrderNo_' . $local_order_id];

    $ch = curl_init();
//set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $order_data['pollurl']);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, '');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//execute post
    $result = curl_exec($ch);
    if ($result) {
//close connection
        $msg = ParseMsg($result);
        $MerchantKey = $integration_key;
        $validateHash = CreateHash($msg, $MerchantKey);
        if ($validateHash != $msg["hash"]) {
            header("Location: $checkout_url");
        } else {
            /*             * *** IMPORTANT ****
              On Paynow, payment status has changed, say from Awaiting Delivery to Delivered

              Here is where you
              1. Update your local shopping cart of Payment Status etc and do appropriate actions here, Save data to DB
              2. Email, SMS Notifications to customer, merchant etc
              3. Any other thing

             * ** END OF IMPORTANT *** */


            $order_status = $msg["status"];

            include ("../dbconnect.php");


            $amountToCredit = 0;
            $user_id = 0;
            $ibs_id = 0;

            $db = mysql_select_db($dbname, $conn);
// SQL query to fetch information of registerd users and finds user match.
            $query = mysql_query("select * from transactions where txn_id ='" . $local_order_id . "'", $conn);

            while ($row = mysql_fetch_assoc($query)) {
// OR just echo the data: // store the userid in a session variable
                $amountToCredit = $row['txn_amount'];
                $user_id = $row['user_id'];
            }

            $db = mysql_select_db($dbname, $conn);
// SQL query to fetch information of registerd users and finds user match.
            $query = mysql_query("select * from users where user_id ='" . $user_id . "'", $conn);

            while ($row = mysql_fetch_assoc($query)) {
// OR just echo the data: // store the userid in a session variable

                $ibs_id = $row['ibs_id'];
            }


            if (strcmp($order_status, 'Paid') == 0) {
                creditUserAccount('t3lc0zss', $ibs_id, $amountToCredit, 'ADD', 'an empty comment', $local_order_id, 'complete');
            } else {

                include ("../dbconnect.php");
                $db = mysql_select_db($dbname, $conn);
                $sql = "UPDATE transactions SET txn_status = 'cancelled' WHERE txn_id = '" . $local_order_id . "'";
                $query = mysql_query($sql, $conn);

                //creditUserAccount('t3lc0zss', $ibs_id , 0, 'ADD', 'an empty comment', $local_order_id, 'failed');
            }




//1. Lets write the updated settings
            $orders_array['OrderNo_' . $local_order_id] = $msg;
            $orders_array['OrderNo_' . $local_order_id]['visited_by_paynow'] = 'yes';
            write_ini_file($orders_array, $orders_data_file, true);
        }
    }
    exit;
}

function WereCreatingATransaction() {
    global $_POST;
    global $integration_id;
    global $integration_key;
    global $authemail;
    global $initiate_transaction_url;
    global $orders_data_file;
    global $checkout_url;
    $local_order_id = $_POST['order_id'];
//set POST variables
    $values = array('resulturl' => $_POST['resulturl'], 'returnurl' => $_POST['returnurl'], 'reference' => $_POST['reference'], 'amount' => $_POST['amount'], 'id' => $integration_id, 'additionalinfo' => $_POST['additionalinfo'], 'authemail' => $authemail, 'status' => 'Message'); //just a simple message
    $fields_string = CreateMsg($values, $integration_key);
//open connection
    $ch = curl_init();
    $url = $initiate_transaction_url;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//execute post
    $result = curl_exec($ch);
    if ($result) {
        $msg = ParseMsg($result);
//first check status, take appropriate action
        if ($msg["status"] == ps_error) {
//var_dump($msg);exit;
//header("Location: $checkout_url");
//exit;
            $error = $msg['error'];
        } else
        if ($msg["status"] == ps_ok) {
//second, check hash
            $validateHash = CreateHash($msg, $integration_key);
            if ($validateHash != $msg["hash"]) {
                $error = "Paynow reply hashes do not match : " . $validateHash . " - " . $msg["hash"];
            } else {
                $theProcessUrl = $msg["browserurl"];
                /*                 * *** IMPORTANT ****
                  On User has approved paying you, maybe they are awaiting delivery etc

                  Here is where you
                  1. Save the PollURL that we will ALWAYS use to VERIFY any further incoming Paynow Notifications FOR THIS PARTICULAR ORDER
                  1. Update your local shopping cart of Payment Status etc and do appropriate actions here, Save any other relavant data to DB
                  2. Email, SMS Notifications to customer, merchant etc
                  3. Any other thing

                 * ** END OF IMPORTANT *** */
                include ("../dbconnect.php");
// Selecting Database
                $db = mysql_select_db($dbname, $conn);
                $sql = "UPDATE transactions SET intergration_id = '" . $integration_id . "', txn_reference_number = '" . $_POST['order_id'] . "', txn_amount = '" . $_POST['amount'] . "', txn_status = 'processing', txn_date = '" . date('Y/m/d') . "', txn_time = '" . date('h:i:sa') . "' WHERE txn_id = '" . $local_order_id . "'";
                $query = mysql_query($sql, $conn);
//1. Saving mine to a PHP.INI type of file, you should save it to a db etc
                $orders_array = array();
                if (file_exists($orders_data_file)) {
                    $orders_array = parse_ini_file($orders_data_file, true);
                }
                $orders_array['OrderNo_' . $local_order_id] = $msg;
                write_ini_file($orders_array, $orders_data_file, true);
            }
        } else {
//unknown status or one you dont want to handle locally
            $error = "Invalid status in from Paynow, cannot continue.";
        }
    } else {
        $error = curl_error($ch);
    }
//close connection
    curl_close($ch);
//Choose where to go
    if (isset($error)) {
//back to checkout, show the user what they need to do
//header("Location: $checkout_url");
        echo $error;
        exit;
    } else {
//redirect to paynow for user to complete payment
        header("Location: $theProcessUrl");
    }
    exit;
}
?>

<?php

/* * *******************************************
  Helper Functions
 * ******************************************* */

function ParseMsg($msg) {
    $parts = explode("&", $msg);
    $result = array();
    foreach ($parts as $i => $value) {
        $bits = explode("=", $value, 2);
        $result[$bits[0]] = urldecode($bits[1]);
    }
    return $result;
}

function UrlIfy($fields) {
    $delim = "";
    $fields_string = "";
    foreach ($fields as $key => $value) {
        $fields_string .= $delim . $key . '=' . $value;
        $delim = "&";
    }
    return $fields_string;
}

function CreateHash($values, $MerchantKey) {
    $string = "";
    foreach ($values as $key => $value) {
        if (strtoupper($key) != "HASH") {
            $string .= $value;
        }
    }
    $string .= $MerchantKey;
    $hash = hash("sha512", $string);
    return strtoupper($hash);
}

function CreateMsg($values, $MerchantKey) {
    $fields = array();
    foreach ($values as $key => $value) {
        $fields[$key] = urlencode($value);
    }
    $fields["hash"] = urlencode(CreateHash($values, $MerchantKey));
    $fields_string = UrlIfy($fields);
    return $fields_string;
}

//cutom function to write php config type of file from array
function write_ini_file($assoc_arr, $path, $has_sections = FALSE) {
    $content = "";
    if ($has_sections) {
        foreach ($assoc_arr as $key => $elem) {
            $content .= "[" . $key . "]\n";
            foreach ($elem as $key2 => $elem2) {
                if (is_array($elem2)) {
                    for ($i = 0; $i < count($elem2); $i++) {
                        $content .= $key2 . "[] = \"" . $elem2[$i] . "\"\n";
                    }
                } else
                if ($elem2 == "")
                    $content .= $key2 . " = \n";
                else
                    $content .= $key2 . " = \"" . $elem2 . "\"\n";
            }
        }
    }
    else {
        foreach ($assoc_arr as $key => $elem) {
            if (is_array($elem)) {
                for ($i = 0; $i < count($elem); $i++) {
                    $content .= $key2 . "[] = \"" . $elem[$i] . "\"\n";
                }
            } else
            if ($elem == "")
                $content .= $key2 . " = \n";
            else
                $content .= $key2 . " = \"" . $elem . "\"\n";
        }
    }
    if (!$handle = fopen($path, 'w')) {
        return false;
    }
    if (!fwrite($handle, $content)) {
        return false;
    }
    fclose($handle);
    return true;
}

function creditUserAccount($token, $user_id, $credit, $topup_type, $comment, $order_id, $status) {

    $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_credit_to_user.php';

    include ("../dbconnect.php");
    include ('../dbFunctions.php');
    $operator = new DatabaseFunctionsClass();

    $data = array('token' => $token, 'user_id' => $user_id, 'credit' => $credit, 'topup_type' => $topup_type, 'comment' => $comment);

    $json = $operator->CallAPI('POST', $service_address, $data);

    $info = json_decode($json, true);


    if ($info[0] == 'true') {
        $db = mysql_select_db($dbname, $conn);
        $sql = "UPDATE transactions SET txn_status = '" . $status . "' WHERE txn_id = '" . $order_id . "'";
        $query = mysql_query($sql, $conn);
    }
}
?>

<?php

/* * *******************************************
  HTML Functions
 * ******************************************* */

function getShoppingCartHTML() {
    header('location: https://selfservice.telco.co.zw/portal/package-purchase.html');
}

function getBackFromPaynowHTML($session_id, $order_id) {

    header('location: https://selfservice.telco.co.zw/portal/user/home.php?session_id=' . $session_id . '&order_id=' . $order_id);
}
?>