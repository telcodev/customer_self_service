<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}






// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];

// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}





//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);







//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);







//get the further's group info from ibs in order to get the data left

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsDataInfo = json_decode($json, true);






//get user info from our db

$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$password = $portalUserInfo[0]["password"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$city = $portalUserInfo[0]["city"];
$address = $portalUserInfo[0]["address"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];
?>




<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php
            require './_nav.php';
            require './_modals.php';
            ?>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">My Profile</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">

                                <li><a href="home.php">Home</a></li>
                                <li class="active">My Profile</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.row -->
                    <!-- .row -->
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="white-box">
                                <div class="user-bg">
                                    <img width="100%" alt="user" src="<?php echo $profileimageurl; ?>">
                                    <div class="overlay-box">
                                        <div class="user-content">
                                            <img id='profileImage' src="<?php echo $profileimageurl; ?>" class="thumb-lg img-circle" alt="img">
                                            <form style="display: none;" id='uploader-form'>
                                                <input type="hidden" name='user_id' value="<?php echo $_SESSION['user_id']; ?>"
                                                       <input type="file" name='new_image' id='uploader-input'>  
                                            </form>

                                            <h4 class="text-white">
                                                <?php
                                                echo $name . " " . $surname;
                                                ?>
                                            </h4>
                                            <h5 class="text-white">

                                                <?php
                                                echo $username;
                                                if ($portalUserInfo[0]["cst_code"] != '') {
                                                    echo ' (' . $portalUserInfo[0]["cst_code"] . ')';
                                                }
                                                ?>

                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="user-btm-box">
                                    <div class="stats-row col-md-12 m-t-20 m-b-0 text-center">
                                        <div class="stat-item">
                                            <h6>Contact info</h6>
                                            <hr/>
                                            <b id = "phoneDashboard"><i class="ti-mobile"></i><?php echo $phone; ?></b></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <div class="white-box">
                                <ul class="nav nav-tabs tabs customtab">
                                    <li class="active tab"><a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">News and Promotions</span> </a> </li>
                                    <li class="tab"><a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Profile</span> </a> </li>
                                    <li class="tab"><a href="#messages" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Messages</span> </a> </li>
                                    <li class="tab"><a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Settings</span> </a> </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="home">

                                        <?php $operator->populateNewsOnSettingsPage(); ?>


                                    </div>
                                    <div class="tab-pane" id="profile">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong> <br>
                                                <p class="text-muted"><?php echo $name . ' ' . $surname; ?></p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong> <br>
                                                <p class="text-muted"><?php echo $phone; ?></p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong> <br>
                                                <p class="text-muted"><?php echo $email; ?></p>
                                            </div>
                                            <div class="col-md-3 col-xs-6"> <strong>Location</strong> <br>
                                                <p class="text-muted"><?php echo $city; ?></p>
                                            </div>
                                        </div>
                                        <hr>

                                        <h4 class="font-bold m-t-30">Purchases</h4>





                                        <div class="white-box">

                                            <div class="row sales-report">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <h2><?php echo date('M Y'); ?></h2>
                                                    <p>PURCHASE HISTORY</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 ">
                                                    <h1 class="text-right text-success m-t-20">$<?php echo round($ibsUserInfo['basic_info']['credit'], 2); ?></h1>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>NAME</th>
                                                            <th>STATUS</th>
                                                            <th>DATE</th>
                                                            <th>PRICE</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>


                                                        <!--Start adding purchase history rows here-->



                                                        <?php $operator->phpPopulatePurchaseHistory($user_id); ?>



                                                        <!--Stop adding purchase history rows here-->


                                                    </tbody>
                                                </table>
                                                <a href="full_purchase_history.php" class = "showbottom">View all purchase history</a> </div>
                                        </div>





                                    </div>
                                    <div class="tab-pane" id="messages">
                                        <div class="steamline">


                                            <?php $operator->addMessagesOnSettingsPage($_SESSION['user_id']); ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="settings">

                                        <form id="loginform" action="updateprofileredirect.php" method="post" name="loginform">
                                            <input type="hidden" name ='user_id' value="<?php echo $_GET['user_id']; ?>">
                                            <div class="form-body">
                                                <h3 class="box-title">Client's Info</h3>
                                                <hr>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">First Name</label> <input readonly type="text" id="firstname" name = "firstname" value ='<?php echo $portalUserInfo[0]['firstname']; ?>' class="form-control" placeholder="Tom" required = "required">
                                                        </div><!--/span-->
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Last Name</label> <input readonly type="text" id="lastname" name="lastname" class="form-control" value ='<?php echo $portalUserInfo[0]['surname']; ?>' placeholder="Chibaya"  required = "required">
                                                        </div>
                                                    </div><!--/span-->
                                                </div><!--/row-->

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Username</label> <input readonly type="text" class="form-control" placeholder="tom.chibaya" id = "username" value ='<?php echo $portalUserInfo[0]['username']; ?>' name="username" required = "required">
                                                        </div>                                              
                                                    </div><!--/span-->


                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label>Role</label> <select disabled class="form-control" id='sRole' name = "privilege" required = "required">

                                                                <option>
                                                                    user
                                                                </option>
                                                            </select>
                                                        </div>

                                                    </div>


                                                </div><!--/row-->

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group" id = "email_form_group">
                                                            <label class="control-label">Email</label> <input readonly type="text" class="form-control" value ='<?php echo $portalUserInfo[0]['email']; ?>' placeholder="tomchibaya@telco.co.zw" id = "email" name="email" required = "required">
                                                        </div>
                                                    </div><!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone Number</label> <input readonly type="text" class="form-control" value ='<?php echo $portalUserInfo[0]['phone']; ?>' placeholder="077200123" id = "phone" name="phone"  required = "required">
                                                        </div>
                                                    </div><!--/span-->
                                                </div><!--/row-->

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group" id = "password_form_group">
                                                            <label class="control-label">Password</label> <input readonly type="password" id="password" value ='<?php echo $portalUserInfo[0]['password']; ?>' name = "password" class="form-control"  required = "required">
                                                        </div>
                                                    </div><!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group" id = "repeat_password_form_group">
                                                            <label class="control-label">Repeat Password</label> <input type="password" id="repeatpassword" value ='<?php echo $portalUserInfo[0]['password']; ?>' name = "repeatpassword" class="form-control" readonly>
                                                        </div>
                                                    </div><!--/span-->
                                                </div><!--/row-->

                                                <h3 class="box-title m-t-40">Address</h3>
                                                <hr>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Address Line</label> <input readonly type="text" name = "address" id = "address"  class="form-control" value ='<?php echo $portalUserInfo[0]['address']; ?>' required = "required">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>City</label> <input readonly type="text" name = "city" id = "city" class="form-control" required = "required" value ='<?php echo $portalUserInfo[0]['city']; ?>'>
                                                        </div>
                                                    </div><!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Country</label> <select disabled class="form-control" id='sCountry' name = "country" required = "required">

                                                                <option>
                                                                    Zimbabwe
                                                                </option>

                                                                <option>
                                                                    South Africa
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div><!--/span-->
                                                </div><!--/row-->





                                                <hr>

                                                <div class="form-actions">
                                                    <button type="button" onclick="openFields()"  class="btn btn-info" id = "editbtn"><i class="fa fa-pencil"></i> Edit</button> 
                                                    <button disabled type="submit"  class="btn btn-default" id = "submitbtn"><i class="fa fa-check"></i> Save</button> 
                                                </div>

                                                <hr/>


                                            </div>
                                        </form>


                                    </div>
                                </div>
                            </div>
                            <button type="submit"  class="btn btn-danger pull-right" id = "deletebtn"><i class="fa fa-times"></i> Delete my Account</button> 
                        </div>
                    </div>
                    <!-- /.row -->
                    <!-- .right-sidebar -->

                    <!-- /.right-sidebar -->
                </div>
                <!-- /.container-fluid -->
                <footer class="footer text-center"> 2016 &copy; Elite Admin brought to you by themedesigner.in </footer>

                <?php
                require './_notifyier.php';
                require './_footer.php';
                ?>

            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- Bootstrap Core JavaScript -->
        <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

        <!--slimscroll JavaScript -->
        <script src="../js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="../js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="../js/custom.js"></script>
        <!--Style Switcher -->
        <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script>

            $('#profileImage').click(function(){
    $('#uploader-input').removeAttr('style');
});
        </script>
        //script to open fields
        <script type="text/javascript">

            function openFields() {

                document.getElementById('firstname').removeAttribute("readonly");

                document.getElementById('lastname').removeAttribute("readonly");

                document.getElementById('sRole').removeAttribute("disabled");

                document.getElementById('username').removeAttribute("readonly");

                document.getElementById('password').removeAttribute("readonly");

                document.getElementById('email').removeAttribute("readonly");

                document.getElementById('phone').removeAttribute("readonly");

                document.getElementById('city').removeAttribute("readonly");

                document.getElementById('address').removeAttribute("readonly");

                document.getElementById('sCountry').removeAttribute("disabled");

                document.getElementById('submitbtn').removeAttribute('disabled');
            }

        </script>
    </body>
</html>
