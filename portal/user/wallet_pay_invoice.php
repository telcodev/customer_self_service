<?php

session_start();

include '../dbFunctions.php';
$operator = new DatabaseFunctionsClass();


//check that the account we are about to wallet recharge is a valid account
$portalUserInfo = $operator->getUserInfoByUserID($_SESSION['user_id']);

if ($portalUserInfo[0]['verified'] != 'verified') {
    header('location: home.php?notify=02');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'inactive') {
    header('location: home.php?notify=03');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=86');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'pending_support') {
    header('location: home.php?notify=04');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'deleted') {
    header('location: home.php?notify=05');
    exit();
}
if ($portalUserInfo[0]['account_status'] == 'disabled') {
    header('location: home.php?notify=06');
    exit();
}
if ($portalUserInfo[0]['account_status'] != 'active') {
    header('location: home.php?notify=07');
    exit();
}
if ($portalUserInfo[0]['ibs_id'] == '' || $portalUserInfo[0]['ibs_id'] == null) {
    header('location: home.php?notify=08');
    exit();
}


// try to get odoo invoice information
$service_address = 'https://hotspot.openaccess.co.zw/odoo_api/cs_portal/get_customer_invoices.php';
$data = array('cst_code' => $portalUserInfo[0]["cst_code"]);
$json = $operator->CallAPI('GET', $service_address, $data);
$odooInvoiceInfo = json_decode($json, true);

$invoice = null;
foreach ($odooInvoiceInfo['payload'] as $inv) {
    if ($inv['id'] == $_GET['inv_id']) {
        $invoice = $inv;
        break;
    }
}

// creating the initial transaction
$insertSql = "INSERT INTO transactions (user_id, txn_status, additional_info) VALUES('" . $_SESSION['user_id'] . "','initiate', 'Wallet >> Invoice: ".$invoice['display_name'].' '.$invoice['id']."')";
$query = mysqli_query($operator->link, $insertSql);
$order_id = mysqli_insert_id($operator->link);

$reply = processPaidInvoice($_SESSION['user_id'],  $invoice['amount_total_signed'], $order_id);

$reply ? header('location: home.php?notify=19') : header('location: home.php?notify=20');



function processPaidInvoice($user_id, $credit, $order_id)
{

    global $operator;
    global $invoice;
    $invoice_id = $invoice['id'];
    $success = true;

    foreach ($invoice['line_items'] as $item) {

        $product_id = $item['product_id'][0];

        // get the ibs group name that we are supposed to credit from predefined table values
        $ref = str_replace(']', '', str_replace('[', '', explode(" ", $item['product_id'][1])[0]));
        $sql = "select * from service_ibs_group_map where odoo_service_reference = '" . $ref . "' limit 1";
        $res = mysqli_query($operator->link, $sql);
        $correct_group = mysqli_fetch_assoc($res)['ibs_group_name'];

        // get an ibs account matching this correct_group from the customers own mappings
        $sql = "select * from customer_services where user_id = '" . $user_id . "' and odoo_reference = '" . $correct_group . "' and odoo_id = '" . $product_id . "' and resource_type = 'CONTRACT_LINE_ITEM' limit 1";
        $res = mysqli_query($operator->link, $sql);
        $mapping_info = mysqli_fetch_assoc($res);

        // get the ibs account
        $token = 't3lc0zss';
        $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';
        $data = array('token' => $token, 'user_id' => $mapping_info['ibs_id']);
        $json = $operator->CallAPI('GET', $service_address, $data);
        $info = json_decode($json, true);


        $goto_fail_safe = false;
        switch ($info['basic_info']['status']) {
            case 'Recharged':
                // the account is in out of bundle state so we need to preserve the credit and renew the package
                // save the existing credit to wallet
                $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';
                $data = array('token' => $token, 'user_id' => $mapping_info['ibs_id'], 'credit' => $info['basic_info']['credit'], 'topup_type' => 'ADD', 'comment' => 'Invoice ' . $invoice_id . ' paid by selfservice order id: ' . $order_id . '. Saving out-of-bundle credit.');
                $json = $operator->CallAPI('POST', $service_address, $data);
                $reply = json_decode($json, true);

                if (!$reply[0]) {
                    $goto_fail_safe = true;
                    break;
                }

                //change the group under here just in case of the payment corresponds to an upgrade or downgrade
                $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_change_user_group.php';
                $data = array('token' => $token, 'user_id' => $mapping_info['ibs_id'], 'group_name' => $correct_group);
                $json = $operator->CallAPI('POST', $service_address, $data);
                $reply = json_decode($json, true);

                if (!$reply[0]) {
                    $goto_fail_safe = true;
                    break;
                }

                // renew the package
                $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_renew_package.php';
                $data = array('token' => $token, 'user_ids' => $mapping_info['ibs_id'], 'comment' => 'Invoice '.$invoice_id.' paid by selfservice order id: '.$order_id.'. Renewing package.');
                $json = $operator->CallAPI('POST', $service_address, $data);
                $response = json_decode($json, true);


                if (!$response[0]) {
                    $goto_fail_safe = true;
                    break;
                }

                //deduct amount they paying
                $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';
                $data = array('token' => $token, 'user_id' => $mapping_info['ibs_id'], 'credit' => -$credit, 'topup_type' => 'ADD', 'comment' => 'deducting invoice line item amount from wallet');
                $json = $operator->CallAPI('POST', $service_address, $data);
                $res = json_decode($json, true);


                $sql = $res[0] ? "'INVOICE PAYMENT COMPLETED: $" . $credit . "' WHERE product_id = '" . $product_id . "'" : "'INVOICE PAYMENT, DEDUCTION FAILED LOST: $" . $credit . "' WHERE product_id = '" . $product_id . "'";
                $operator->logger('Order ' . $order_id . ' ' . $sql);

                break;

            case 'Package':
                // the account is still in bundle state  so we just renew the package
                //change the group under here just in case of the payment corresponds to an upgrade or downgrade
                $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_change_user_group.php';
                $data = array('token' => $token, 'user_id' => $mapping_info['ibs_id'], 'group_name' => $correct_group);
                $json = $operator->CallAPI('POST', $service_address, $data);
                $reply = json_decode($json, true);

                if (!$reply[0]) {
                    $goto_fail_safe = true;
                    break;
                }

                // renew the package
                $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_renew_package.php';
                $data = array('token' => $token, 'user_ids' => $mapping_info['ibs_id'], 'comment' => 'Invoice '.$invoice_id.' paid by selfservice order id: '.$order_id.'. Renewing package.');
                $json = $operator->CallAPI('POST', $service_address, $data);
                $reply = json_decode($json, true);
                
                if (!$reply[0]) {
                    $goto_fail_safe = true;
                }

                break;


            default:
                $goto_fail_safe = true;
                break;
        }

        if ($goto_fail_safe){
            // in this escape case we will just add the topped up amount to the wallet, this is preserving the amount so the can try and top it up later
            $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';
            $data = array('token' => $token, 'user_id' => $mapping_info['ibs_id'], 'credit' => $credit, 'topup_type' => 'ADD', 'comment' => 'Adding the invoice amount to the wallet since we reached an escape case');
            $json = $operator->CallAPI('POST', $service_address, $data);
            $reply = json_decode($json, true);

            $sql = $reply[0] ? "'INTERNAL FAILURE, CREDIT SAVED: $" . $credit . "' WHERE product_id = '" . $product_id . "'" : "'INTERNAL FAILURE, CREDIT LOST: $" . $credit . "' WHERE product_id = '" . $product_id . "'";
            $operator->logger('Order ' . $order_id . ' ' . $sql);
            $success = false;
        }

    }
    $sql = "UPDATE transactions SET txn_status = 'COMPLETE' WHERE txn_id = '" . $order_id . "'";
    mysqli_query($operator->link, $sql);
    return $success;

}
