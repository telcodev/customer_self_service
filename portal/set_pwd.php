<?php

include ('dbFunctions.php');
$operator = new DatabaseFunctionsClass();

if (isset($_POST['h']) && !empty($_POST['h']) AND isset($_POST['u']) && !empty($_POST['u']) AND isset($_POST['password1']) && !empty($_POST['password1']) AND isset($_POST['password2']) && !empty($_POST['password2']) ) {

    // test passwords
    $password1 = $_POST['password1'];
    $password2 = $_POST['password2'];
    
    if ($password1 !== $password2) {
        header("location: login.html?passwords_no_match"); // Redirecting To Other Page
        exit();
    }
    
    if(strlen($password1) < 8) {
        header("location: login.html?password_too_short"); // Redirecting To Other Page
        exit();
    }
    
    $res = $operator->resetPassword($_POST['u'], $password1, $_POST['h']);

    $res ?  header("location: login.html?password_reset_success") : header("location: login.html?password_reset_failed");

} else {
    // Invalid approach
    echo '<div class="statusmsg">Invalid approach, please use the link that has been send to your email.</div>';
}
