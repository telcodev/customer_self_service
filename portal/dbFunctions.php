<?php


class DatabaseFunctionsClass
{

    var $link;

    function __construct()
    {

        session_start(); // Starting Session

        $servername = "xenon.telco.co.zw";
        $dbname = "mwxwdasd_csp_db";
        $dbusername = "mwxwdasd_csp_usr";
        $dbpassword = "^=e[$,?KHknK";

        // Create connection
        $this->link = mysqli_connect($servername, $dbusername, $dbpassword, $dbname) or die("Failed to connect because: " . mysqli_connect_error());


    }

    function __destruct()
    {
        mysqli_close($this->link);
    }

    function logger($message)
    {

        $myfile = fopen("logs.txt", "a") or die("Unable to open file!");
        $txt = date('Y-m-d') . " " . date('h:i:s') . "    -  " . $message . "\n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }

    function debug_to_console($data)
    {

        if (is_array($data))
            $output = "<script>console.log( 'Debug Objects: " . implode(',', $data) . "' );</script>";
        else
            $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

        echo $output;
    }

    function CallAPI($method, $url, $data = false)
    {

// Method: POST, PUT, GET etc
// Data: array("param" => "value") ==> index.php?param=value


        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    function getMessagesByThreadByUserID($user_id)
    {
        $IDsArray = $this->getThreadIDsByUserID($user_id);
//	print_r($IDsArray);
// Selecting Database
        $messages = array();
        for ($i = 0; $i < count($IDsArray); $i++) {
            $message = array();
            $sql = "SELECT * FROM messages WHERE thread_id = '" . $IDsArray[$i]["thread_id"] . "' ORDER BY date_created DESC";
            $query = mysqli_query($this->link, $sql);
//    echo $IDsArray[$i]["thread_id"];
            if (!$query) {
                die('Could not get data: ' . mysqli_error($this->link));
            }
            $j = 0;
            while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
                $message[$j] = $row;
                $j++;
            }
            $messages[$i] = $message;
        }
        print_r($messages);
        return $messages;
    }

    function getThreadIDsByUserID($user_id)
    {
// Selecting Database
        $sql = "SELECT thread_id FROM messagethreads where owner_id = '" . $user_id . "' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
// look through query
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
//  var_dump($array);
        return $array;
    }

    function getUserInfoByUsername($username)
    {
// Selecting Database
        $sql = "SELECT * FROM users where username = '" . $username . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getUserInfoByPhone($username)
    {
// Selecting Database
        $sql = "SELECT * FROM users where phone = '" . $username . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getUserInfoByCSTCode($cst_code)
    {
// Selecting Database
        $sql = "SELECT * FROM users where cst_code = '" . $cst_code . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $row['anon_email'] = $this->return_anon_email($row['email']);
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function return_anon_email($email)
    {

        $username = explode('@', $email)[0];
        $domain = explode('@', $email)[1];
        $domain_name = explode('.', $domain)[0];

        $dm = explode('.', $domain);

        $rest_of_email = '';
        for ($i = 1; $i < count($dm); $i++) {
            $rest_of_email .= '.' . explode('.', $domain)[$i];
        }

        $username_length = count($username);
        $domain_name_length = count($domain_name);

        $username_sub = substr($username, 0, 3);
        $username_ending = substr($username, 4, 1000);

        $domain_name_sub = substr($domain_name, 0, 3);
        $domain_name_ending = substr($domain_name, 4, 1000);

        $username_placeholders = str_repeat('*', $username_length - 3);
        $domain_placeholders = str_repeat('*', $domain_name_length - 3);

        $hashed_username = preg_replace('/' . $username_ending . '$/', '__', $username);
        $hashed_domain_name = preg_replace('/' . $domain_name_ending . '$/', '__', $domain_name);

        $short_email = $hashed_username . '@' . $hashed_domain_name . $rest_of_email;

        return $short_email;
    }

    function getTxnInfoByTxnID($txn_id)
    {
// Selecting Database
        $sql = "SELECT * FROM transactions where txn_id = '" . $txn_id . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getAdInfoByAdID($ad_id)
    {
// Selecting Database
        $sql = "SELECT * FROM ads where ad_id = '" . $ad_id . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getFaqInfoByFaqID($faq_id)
    {
// Selecting Database
        $sql = "SELECT * FROM faqs where faq_id = '" . $faq_id . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function setAutoRenew($change, $user_id, $ibs_id)
    {


        if ($change == 'ADD') {


            $sql = "INSERT INTO auto_renewals (user_id, ibs_id) VALUES ('" . $user_id . "','" . $ibs_id . "')";
            $query = mysqli_query($this->link, $sql);
            return $query . mysqli_error($this->link);
        }
        if ($change == 'REMOVE') {


            $sql = "DELETE FROM auto_renewals WHERE user_id = '" . $user_id . "'";
            $query = mysqli_query($this->link, $sql);
            return $query . mysqli_error($this->link);
        }

        return false;
    }

    function markMessageAsResolved($change, $msg_id)
    {
        $val = $change == "MARK" ? "resolved" : $change == "UNMARK" ? "unresolved" : null;

        $sql = "UPDATE messages SET status = '$val' WHERE msg_id =" . $msg_id;
        $query = mysqli_query($this->link, $sql);
        return $query . mysqli_error($this->link);


        return false;
    }

    function getAutoRenewUserByUserId($user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM auto_renewals where user_id = '" . $user_id . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getUserInfoByUserID($user_id)
    {
        $sql = "SELECT * FROM users where user_id = '" . $user_id . "'";
        $query = mysqli_query($this->link, $sql);
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
            $array [$i] = $row;
            $i++;
        }

        $sql = "SELECT * FROM users where user_id = '" . $user_id . "'";
        $query = mysqli_query($this->link, $sql);
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
            $array [$i] = $row;
            $i++;
        }

        return $array;
    }

    function getFileByFileID($file_id)
    {
// Selecting Database
        $sql = "SELECT * FROM files where file_id = '" . $file_id . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getUserInfoByEmail($user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM users where email = '" . $user_id . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
            $array [$i] = $row;
            $i++;
        }
        return $array;
    }

    function getMsgInfoByMsgID($msg_id)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where msg_id = '" . $msg_id . "'";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function updateUser($firstname, $email, $password, $phone, $country)
    {


// Selecting Database
        $sql = "UPDATE users SET firstname = '" . $firstname . "', email = '" . $email . "', password = '" . $password . "', phone = '" . $phone . "', country = '" . $country . "' WHERE user_id = '" . $_SESSION['user_id'] . "'";
        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "New user created");


            header("location: profile.php?notify=14");
        } else {
            header("location: profile.php?notify=15");
            $messageArray = array("code" => 0, "message" => "Failed to add new user " . mysqli_error($this->link) . "");
        }
        return $messageArray;
    }

    function updateMyProfile($firstname, $lastname, $username, $email, $password, $phone, $address, $city, $country)
    {

// Selecting Database
        $sql = "UPDATE users SET firstname = '" . $firstname . "', surname = '" . $lastname . "', username = '" . $username . "', email = '" . $email . "', password = '" . $password . "', phone = '" . $phone . "', address = '" . $address . "', city = '" . $city . "', country = '" . $country . "' WHERE user_id = '" . $_SESSION['user_id'] . "'";
        if (mysqli_query($this->link, $sql) === TRUE) {

            $messageArray = array("code" => 1, "message" => "New user created");

            return true;
        } else {

            $messageArray = array("code" => 0, "message" => "Failed to add new user " . mysqli_error($this->link) . "");

            return false;
        }
        return $messageArray;
    }

    function updateSelectedUser($user_id, $firstname, $lastname, $username, $privilege, $email, $password, $phone, $address, $city, $country)
    {

// Selecting Database
        $sql = "UPDATE users SET firstname = '" . $firstname . "', surname = '" . $lastname . "', username = '" . $username . "', privilege = '" . $privilege . "',  email = '" . $email . "', phone = '" . $phone . "', address = '" . $address . "', city = '" . $city . "', country = '" . $country . "' WHERE user_id = '" . $user_id . "'";
        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "New user created");

            $sql = "SELECT password FROM users WHERE username = '" . $username . "'";
            $query = mysqli_query($this->link, $sql);
            $temp_user = mysqli_fetch_assoc($query);


            if ($temp_user['password'] !== md5($password)) {

                $sql = "UPDATE users set password = '" . md5($password) . "' WHERE username = '" . $username . "'";
                mysqli_query($this->link, $sql);
            }

            return true;
        } else {


            $messageArray = array("code" => 0, "message" => "Failed to add new user " . mysqli_error($this->link) . "");
            return false;
        }


        return $messageArray;
    }

    function editTxn($txnId, $firstname, $lastname, $cst_code, $branch, $amount, $receipt_no, $comment)
    {

        $date = date('Y-m-d');
        $time = time('h:i:sa');

        $userInfo = $this->getUserInfoByCSTCode($cst_code);
        $user_id = $userInfo[0]['user_id'];


        if ($userInfo[0]['firstname'] != null || $userInfo[0]['surname'] != null) {
            $firstname = $userInfo[0]['firstname'];
            $lastname = $userInfo[0]['surname'];
        } else {

            return false;
        }


// Selecting Database
        $sql = "UPDATE transactions SET user_id = '" . $user_id . "', txn_reference_number = '" . $receipt_no . "', txn_amount = '" . $amount . "', additional_info = '" . $comment . "', txn_date = '" . $date . "', txn_status = 'manually_completed', txn_time = '" . $time . "' WHERE txn_id = '" . $txnId . "'";
        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "New user created");

            return true;
        } else {

            return false;
        }

        return false;
    }

    function addNewTxn($firstname, $lastname, $cst_code, $branch, $amount, $receipt_no, $comment)
    {

        $date = date('Y-m-d');
        $time = time('h:i:sa');

        $userInfo = $this->getUserInfoByCSTCode($cst_code);
        $user_id = $userInfo[0]['user_id'];


        if ($userInfo[0]['firstname'] != null || $userInfo[0]['surname'] != null) {
            $firstname = $userInfo[0]['firstname'];
            $lastname = $userInfo[0]['surname'];
        } else {
            header('location: https://selfservice.telco.co.zw/portal/billing_admin/home.php?notify=76');
            exit();
        }


        // Selecting Database
        $sql = "INSERT INTO transactions (user_id, txn_reference_number, txn_amount, additional_info, txn_status, txn_date, txn_time)
VALUES ('" . $user_id . "','" . $receipt_no . "','" . $amount . "', '" . $comment . "', 'complete','" . $date . "', '" . $time . "')";
        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "New user created");
            header('location: https://selfservice.telco.co.zw/portal/billing_admin/home.php?notify=78');
        } else {
            header('location: https://selfservice.telco.co.zw/portal/billing_admin/home.php?notify=77');
        }
    }

    function linkClientToIBS($firstname, $lastname, $username, $privilege, $cst_code, $email, $phone, $password, $address, $city, $country, $ibs_id)
    {

        $hash = md5(rand(0, 1000));
        // Selecting Database

        $sql = "INSERT INTO users (firstname, surname, username, privilege, cst_code, email, phone, password, address, city, country, profile_image_url, ibs_id, creation_date, hash, verified, account_status)
VALUES ('" . $firstname . "','" . $lastname . "','" . $username . "', 'user', '" . $cst_code . "', '" . $email . "','" . $phone . "', '" . md5($password) . "','" . $address . "','" . $city . "','" . $country . "', 'https://selfservice.telco.co.zw/plugins/images/users/default_profile_image.png', '" . $ibs_id . "', '" . date("Y-m-d") . "', '" . $hash . "', 'verified', 'active')";


        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "New user created");
            return true;
        } else {

            $messageArray = array("code" => 0, "message" => "Failed to add new user " . mysqli_error($this->link));
            return false;
        }
        //  return $messageArray;
    }

    function addNewUser($role, $firstname, $lastname, $username, $privilege, $cst_code, $email, $phone, $password, $address, $city, $country, $returnPage)
    {


        $hash = md5(rand(0, 1000));
        // Selecting Database

        $acount_status = 'inactive';
        if (isset($cst_code) && $cst_code != "")
            $acount_status = 'active';


        if ($role == 'guest') {
            $sql = "INSERT INTO users (firstname, surname, username, privilege, email, phone, password, address, city, country, profile_image_url, creation_date, hash, verified, account_status)
VALUES ('" . $firstname . "','" . $lastname . "','" . $username . "', '" . $privilege . "', '" . $email . "','" . $phone . "', '" . md5($password) . "','" . $address . "','" . $city . "','" . $country . "', 'https://selfservice.telco.co.zw/plugins/images/users/default_profile_image.png', '" . date("Y-m-d") . "', '" . $hash . "', 'unverified', '" . $acount_status . "')";
        } else {

            $sql = "INSERT INTO users (firstname, surname, username, privilege, cst_code, email, phone, password, address, city, country, profile_image_url, creation_date, hash, verified, account_status)
VALUES ('" . $firstname . "','" . $lastname . "','" . $username . "', '" . $privilege . "', '" . $cst_code . "', '" . $email . "','" . $phone . "', '" . md5($password) . "','" . $address . "','" . $city . "','" . $country . "', 'https://selfservice.telco.co.zw/plugins/images/users/default_profile_image.png', '" . date("Y-m-d") . "', '" . $hash . "', 'verified', '" . $acount_status . "')";
        }


        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "New user created");


            //if there is a user who is registering set the session variable so that they can auto  log in
            if (strcmp($privilege, 'user') == 0) {

                $_SESSION['user_id'] = mysqli_insert_id($this->link);
                //$_SESSION['ibs_id'] = $ibs_id;
                //$this->addSession($_SESSION['user_id'], $_SESSION['ibs_id']);
            }

            //send activation email

            $to = $email; // Send email to our user
            $subject = 'Signup | Verification'; // Give the email a subject 
            $message = '
                
Thanks for signing up with Telco"s Online Personal TopUp Portal!
    
Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.
    
------------------------
Username: ' . $username . '
Password: ' . $password . '
------------------------
    
Please click this link to activate your account:
https://selfservice.telco.co.zw/portal/verify.php?email=' . $email . '&hash=' . $hash . '
    
'; // Our message above including the link

            $headers = 'From:noreply@telco.co.zw' . "\r\n"; // Set from headers


            if ($role == 'guest')
                mail($to, $subject, $message, $headers); // Send our email

            return true;
        } else {

            $messageArray = array("code" => 0, "message" => "Failed to add new user " . mysqli_error($this->link) . "");
            return false;
        }
        //  return $messageArray;
    }

    function attemptLogin($un, $pwd)
    {

        if (empty($un) || empty($pwd)) {
            $messageArray = array("code" => 0, "message" => "You need to supply a user name and a password");
            header("location: login.html");
            return $messageArray;
        }
        // Define $username and $password
        $username = $un;
        $password = $pwd;
        $privilege = null;

        // Establishing Connection with Server by passing server_name, user_id and password as a parameter
        // To protect MySQL injection for Security purpose
        $username = stripslashes($username);
        $password = stripslashes($password);
        $username = mysqli_real_escape_string($this->link, $username);
        $password = mysqli_real_escape_string($this->link, $password);

        $password = md5($password);

        // check if we have an email over here, then log in using email instead
        $sql = preg_match('/@/', $username) ?
            "select * from users where password ='" . $password . "' AND email ='" . $username . "'" :
            "select * from users where password ='" . $password . "' AND username ='" . $username . "'";

        // SQL query to fetch information of registerd users and finds user match.
        $query = mysqli_query($this->link, $sql);
        $rows = mysqli_num_rows($query);

        if ($rows != 1) {
            // check if this was username or username failed login attempt and increase the failed_logins value
            $sql = preg_match('/@/', $username) ?
                "UPDATE users SET login_attempts = login_attempts + 1 WHERE email = '" . $username . "'" :
                "UPDATE users SET login_attempts = login_attempts + 1 WHERE username = '" . $username . "'";

            mysqli_query($this->link, $sql);

            header("location: login.html?login_failed"); // Redirecting To Other Page
            $messageArray = array("code" => 0, "message" => "Username or Password is invalid");
            return $messageArray;
        }


        // This code runs when the user information is found and is correct.
        $row = mysqli_fetch_assoc($query);
        if (intval($row['login_attempts']) > 2) {
            $to = $row['email']; // Send email to our user
            $subject = 'Telco Customer Service Portal Account Maximum Failed Logins'; // Give the email a subject
            $message = '
                
                        Hello!
                        
                        Your account reached the maximum failed number of logins allowed. In order to avoid malicious attempts to penetrate your account, we have disabled your account from accepting any new log in attempts.

                        Your account password is still safe however, your account is still secure. If you would like to re-enable your account, Visit the link below.

                        --------------------------------------------------
                        --------------------------------------------------

                        Please click this link to activate your account:
                        https://selfservice.telco.co.zw/portal/re-enable-login.php?email=' . $to . '&hash=' . $row["hash"] . '

                        '; // Our message above including the link

            $headers = 'From:noreply@telco.co.zw' . "\r\n"; // Set from headers
            mail($to, $subject, $message, $headers); // Send our email
            header("location: login.html?max_failed_logins_reached"); // Redirecting To Other Page
            exit();
        }


        // OR just echo the data: // store the userid in a session variable
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['ibs_id'] = $row['ibs_id'];
        $privilege = $row['privilege'];
        $this->addSession($row['user_id'], $row['ibs_id']);


        $date = date("Y-m-d");
        $sql = preg_match('/@/', $username) ?
            "UPDATE users SET last_login = '" . $date . "', login_attempts = 0 WHERE password ='" . $password . "' AND email ='" . $username . "'" :
            "UPDATE users SET last_login = '" . $date . "', login_attempts = 0 WHERE password ='" . $password . "' AND username ='" . $username . "'";

        mysqli_query($this->link, $sql);

        switch ($privilege) {

            case 'user':
                header("location: user/home.php"); // Redirecting To Other Page
                $messageArray = array("code" => 1, "message" => "Log In Successful");
                break;

            case 'sales_admin':
                header("location: sales_admin/home.php"); // Redirecting To Other Page
                $messageArray = array("code" => 1, "message" => "Log In Successful");
                break;

            case 'support_admin':
                header("location: support_admin/home.php"); // Redirecting To Other Page
                $messageArray = array("code" => 1, "message" => "Log In Successful");
                break;

            case 'billing_admin':
                header("location: billing_admin/home.php"); // Redirecting To Other Page
                $messageArray = array("code" => 1, "message" => "Log In Successful");
                break;

            case 'super_admin':
                header("location: super_admin/home.php"); // Redirecting To Other Page
                $messageArray = array("code" => 1, "message" => "Log In Successful");
                break;

            default:
                header("location: login.html?login_failed"); // Redirecting To Other Page
                $messageArray = array("code" => 1, "message" => "Log In was Unsuccessful");
                break;
        }
        return $messageArray;
    }

    function addSession($user_id, $ibs_id)
    {

        $sql = "INSERT INTO sessions (user_id, ibs_id, status, date_started) VALUES ('" . $user_id . "','" . $ibs_id . "','active','" . date('Y-m-d h:i:sa') . "' )";
        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "New session created");
        } else {

            $messageArray = array("code" => 0, "message" => "Failed to add new session " . mysqli_error($this->link) . "");
        }
        $_SESSION['session_id'] = mysqli_insert_id($this->link);
    }

    function destroySession($session_id)
    {

        $sql = "UPDATE sessions SET status = 'destroyed', date_ended = '" . date('Y-m-d h:i:sa') . "' WHERE session_id = '" . $session_id . "'";

        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "Session deleted");
        } else {

            $messageArray = array("code" => 0, "message" => "Failed to destroy session " . mysqli_error($this->link) . "");
        }
    }

    function phpPopulateUserInfoOnHomePage($user_id)
    {

        $infoArray = $this->getUserInfoByUserID($user_id);
        $name = $infoArray[0]["firstname"];
        $surname = $infoArray[0]["surname"];
        $username = $infoArray[0]["username"];
        $email = $infoArray[0]["email"];
        $phone = $infoArray[0]["phone"];
        $profileimageurl = $infoArray[0]["profile_image_url"];
        echo '<script> jsPopulateUserInfoOnHomePage("' . $name . '","' . $surname . '","' . $username . '", "' . $email . '","' . $phone . '","' . $profileimageurl . '"); </script>';
    }

    function sendMessage($to, $from, $subject, $message, $msg_category)
    {


        $sql = "INSERT INTO messages (receiver_user_id, sender_user_id, msg_subject, msg_content, msg_category, status, date_created, time_created) VALUES ('" . $to . "','" . $from . "','" . $subject . "','" . $message . "', '" . $msg_category . "', 'unread','" . date('Y-m-d') . "','" . date('h:i:sa') . "' )";
        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "New session created");
        } else {

            $messageArray = array("code" => 0, "message" => "Failed to add new session " . mysqli_error($this->link) . "");
        }
    }

    function addFault($to, $from, $subject, $message, $msg_category)
    {


        $sql = "INSERT INTO messages (receiver_user_id, sender_user_id, msg_subject, msg_content, msg_category, status, date_created, time_created) VALUES ('" . $to . "','" . $from . "','" . $subject . "','" . $message . "', 'fault', 'unread','" . date('Y-m-d') . "','" . date('h:i:sa') . "' )";

        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "New fault created");
        } else {

            $messageArray = array("code" => 0, "message" => "Failed to add new fault " . mysqli_error($this->link) . "");
        }
    }

    function getFaults($user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where msg_category = 'fault' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getTxns()
    {
// Selecting Database
        $sql = "SELECT * FROM transactions ORDER BY txn_date DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getMessagesByUserID($user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where receiver_user_id = '" . $user_id . "' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getMessagesByTagAndUserID($tag, $user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where receiver_user_id = '" . $user_id . "' AND msg_category = '" . $tag . "' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getSentMessagesByUserID($user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where sender_user_id = '" . $user_id . "' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getTrashMessagesByUserID($user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where receiver_user_id = '" . $user_id . "' AND status = 'trashed' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getSentMessagesWithTagByUserID($tag, $user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where sender_user_id = '" . $user_id . "' AND msg_category = '" . $tag . "' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getTrashMessagesWithTagByUserID($tag, $user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where receiver_user_id = '" . $user_id . "' AND status = 'trashed' AND msg_category = '" . $tag . "' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getMessagesByUserIDByCriteria($searchBy, $value)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where " . $searchBy . " = '" . $value . "' AND msg_category != 'fault' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getFaultsByUserIDByCriteria($user_id, $searchBy, $value)
    {
// Selecting Database
        $sql = "SELECT * FROM messages where sender_user_id = '" . $user_id . "' AND " . $searchBy . " = '" . $value . "' ORDER BY date_created DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getDocuments()
    {
// Selecting Database
        $sql = "SELECT * FROM documents ORDER BY date_uploaded";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getFilesByUserId($user_id)
    {
// Selecting Database
        $sql = "SELECT * FROM files where user_id = '" . $user_id . "' ORDER BY upload_date";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getAds()
    {
// Selecting Database
        $sql = "SELECT * FROM ads ORDER BY date_uploaded";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getIbsGroups()
    {
// Selecting Database
        $sql = "SELECT * FROM ibs_groups";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function populateUpgradebaleGroups($price)
    {
        $groups = $this->getIbsGroups();

        for ($i = 0; $i < count($groups); $i++) {

            if ($groups[$i]['price'] > $price) {
                if ($groups[$i]['popular'] == 0) {


                    echo '
                

                        <div style="display: inline-block">
                                                    <div class="pricing-box">
                                                        <div class="pricing-body b-l" style="padding-right: 10px; padding-left: 10px;">
                                                            <div class="pricing-header">
                                                                <h4 class="text-center">' . $groups[$i]["quota"] . 'MB</h4>
                                                                <h2 class="text-center"><span class="price-sign">$</span>' . $groups[$i]["price"] . '</h2>
                                                                <p class="uppercase">' . $groups[$i]["quota"] . ' MB</p>
                                                            </div>
                                                            <div class="price-table-content">
                                                                <div class="price-row"><i class="icon-user"></i> up to ' . $groups[$i]["burst"] . 'Mbps</div>
                                                                <div class="price-row"><i class="icon-screen-smartphone"></i> Valid for ' . $groups[$i]["validity"] . ' days</div>
                                                                <div class="price-row"><i class="icon-drawar"></i> ' . $groups[$i]["desc1"] . '</div>
                                                                <div class="price-row"><i class="icon-refresh"></i> ' . $groups[$i]["desc2"] . '</div>
                                                                <div class="price-row">
                                                                    <form action = "wallet_upgrade.php" method = "post">
                                                                        <input type="hidden" name="group" value="' . $groups[$i]["ibs_group_name"] . '">
                                                                        <input type="hidden" name = "amount" value = "' . $groups[$i]["price"] . '"/>
                                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                                    </form>
                                                                    <form action = "paynowredirect_change_group.php" method = "post">
                                                                        <input type="hidden" name="group" value="' . $groups[$i]["ibs_group_name"] . '">
                                                                        <input type = "hidden" name = "amount" value = "' . $groups[$i]["price"] . '"/>
                                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                        
                        
                        
                        
                        
                        ';
                } else if ($groups[$i]['popular'] == 1) {


                    echo '
                

                        <div style=" display: inline-block">
                                                    <div class="pricing-box featured-plan">
                                                        <div class="pricing-body" style="padding-right: 10px; padding-left: 10px;">
                                                            <div class="pricing-header">
                                                                <h4 class="price-lable text-white bg-warning"> Popular</h4>
                                                                <h4 class="text-center">' . $groups[$i]["quota"] . 'GB</h4>
                                                                <h2 class="text-center"><span class="price-sign">$</span>' . $groups[$i]["price"] . '</h2>
                                                                <p class="uppercase">' . $groups[$i]["quota"] . ' GB</p>
                                                            </div>
                                                            <div class="price-table-content">
                                                                <div class="price-row"><i class="icon-user"></i> up to ' . $groups[$i]["burst"] . 'Mbps</div>
                                                                <div class="price-row"><i class="icon-screen-smartphone"></i> Valid for ' . $groups[$i]["validity"] . ' days</div>
                                                                <div class="price-row"><i class="icon-drawar"></i> ' . $groups[$i]["desc1"] . '</div>
                                                                <div class="price-row"><i class="icon-refresh"></i> ' . $groups[$i]["desc2"] . '</div>
                                                                <div class="price-row">
                                                                    <form action = "wallet_upgrade.php" method = "post">
                                                                        <input type="hidden" name="group" value="' . $groups[$i]["ibs_group_name"] . '">
                                                                        <input type="hidden" name = "amount" value = "' . $groups[$i]["price"] . '"/>
                                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                                    </form>
                                                                    <form action = "paynowredirect_change_group.php" method = "post">
                                                                        <input type="hidden" name="group" value="' . $groups[$i]["ibs_group_name"] . '">
                                                                        <input type = "hidden" name = "amount" value = "' . $groups[$i]["price"] . '"/>
                                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                        
                        
                        
                        
                        
                        ';
                }
            }
        }
    }

    function populateCurrentGroup($price)
    {
        $groups = $this->getIbsGroups();
        for ($i = 0; $i < count($groups); $i++) {

            if ($groups[$i]['price'] == $price) {
                if ($groups[$i]['popular'] == 0) {

                    echo '
                

                        <div style=" display: inline-block">
                                                    <div class="pricing-box">
                                                        <div class="pricing-body b-l" style="padding-right: 10px; padding-left: 10px;">
                                                            <div class="pricing-header">
                                                                <h4 class="text-center">' . $groups[$i]["quota"] . 'MB</h4>
                                                                <h2 class="text-center"><span class="price-sign">$</span>' . $groups[$i]["price"] . '</h2>
                                                                <p class="uppercase">' . $groups[$i]["quota"] . ' MB</p>
                                                            </div>
                                                            <div class="price-table-content">
                                                                <div class="price-row"><i class="icon-user"></i> up to ' . $groups[$i]["burst"] . 'Mbps</div>
                                                                <div class="price-row"><i class="icon-screen-smartphone"></i> Valid for ' . $groups[$i]["validity"] . ' days</div>
                                                                <div class="price-row"><i class="icon-drawar"></i> ' . $groups[$i]["desc1"] . '</div>
                                                                <div class="price-row"><i class="icon-refresh"></i> ' . $groups[$i]["desc2"] . '</div>
                                                                <div class="price-row">
                                                                    <form action = "renewpackage_using_wallet.php" method = "post">
                                                                        <input type="hidden" name="group" value="' . $groups[$i]["ibs_group_name"] . '">
                                                                        <input type="hidden" name = "amount" value = "' . $groups[$i]["price"] . '"/>
                                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                                    </form>
                                                                    <form action = "paynowredirect_renewpackage.php" method = "post">
                                                                        <input type="hidden" name="group" value="' . $groups[$i]["ibs_group_name"] . '">
                                                                        <input type = "hidden" name = "amount" value = "' . $groups[$i]["price"] . '"/>
                                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                        
                        
                        
                        
                        
                        ';
                } else if ($groups[$i]['popular'] == 1) {


                    echo '
                

                        <div style=" display: inline-block">
                                                    <div class="pricing-box featured-plan">
                                                        <div class="pricing-body" style="padding-right: 10px; padding-left: 10px;">
                                                            <div class="pricing-header">
                                                                <h4 class="price-lable text-white bg-warning"> Popular</h4>
                                                                <h4 class="text-center">' . $groups[$i]["quota"] . 'GB</h4>
                                                                <h2 class="text-center"><span class="price-sign">$</span>' . $groups[$i]["price"] . '</h2>
                                                                <p class="uppercase">' . $groups[$i]["quota"] . ' GB</p>
                                                            </div>
                                                            <div class="price-table-content">
                                                                <div class="price-row"><i class="icon-user"></i> up to ' . $groups[$i]["burst"] . 'Mbps</div>
                                                                <div class="price-row"><i class="icon-screen-smartphone"></i> Valid for ' . $groups[$i]["validity"] . ' days</div>
                                                                <div class="price-row"><i class="icon-drawar"></i> ' . $groups[$i]["desc1"] . '</div>
                                                                <div class="price-row"><i class="icon-refresh"></i> ' . $groups[$i]["desc2"] . '</div>
                                                                <div class="price-row">
                                                                    <form action = "renewpackage_using_wallet.php" method = "post">
                                                                        <input type="hidden" name="group" value="' . $groups[$i]["ibs_group_name"] . '">
                                                                        <input type="hidden" name = "amount" value = "' . $groups[$i]["price"] . '"/>
                                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type="submit">Pay using wallet</button>
                                                                    </form>
                                                                    <form action = "paynowredirect_renewpackage.php" method = "post">
                                                                        <input type="hidden" name="group" value="' . $groups[$i]["ibs_group_name"] . '">
                                                                        <input type = "hidden" name = "amount" value = "' . $groups[$i]["price"] . '"/>
                                                                        <button class="btn btn-success waves-effect waves-light m-t-20" type = "submit">Other methods</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                        ';
                }
            }
        }
    }

    function getFAQs()
    {
// Selecting Database
        $sql = "SELECT * FROM faqs ORDER BY date_uploaded";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function phpGetNumberOfUnreadMessages($user_id)
    {

// Selecting Database
        $sql = "SELECT * FROM messages WHERE receiver_user_id = '" . $user_id . "' AND status = 'unread' ORDER BY date_created";
        $query = mysqli_query($this->link, $sql);
        $rows = mysqli_num_rows($query);
        return $rows;
    }

    function phpGetNumberOfAllMessages($user_id)
    {

// Selecting Database
        $sql = "SELECT * FROM messages WHERE receiver_user_id = '" . $user_id . "' AND ORDER BY date_created";
        $query = mysqli_query($this->link, $sql);
        $rows = mysqli_num_rows($query);
        echo $rows;
    }

    function getMessageDetails($msg_id)
    {


// Selecting Database
        $sql = "SELECT * FROM messages where  msg_id = '" . $msg_id . "' ORDER BY date_created";
        $query = mysqli_query($this->link, $sql);

        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }

        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function changeMessageStatus($msg_id, $status)
    {

        $sql = "UPDATE messages SET status = '" . $status . "' WHERE msg_id = " . $msg_id;
        if (mysqli_query($this->link, $sql) === TRUE) {
            $messageArray = array("code" => 1, "message" => "message status changed");
        } else {

            $messageArray = array("code" => 0, "message" => "Failed tchange message status " . mysqli_error($this->link) . "");
        }
    }

    function phpPopulatePurchaseHistory($user_id)
    {


// Selecting Database
        $sql = "SELECT * FROM transactions where user_id = '" . $user_id . "' ORDER BY txn_date DESC, txn_time DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }


        for ($i = 0; $i < count($array); $i++) {

            $txn_id = $array[$i]["txn_id"];
            $user_id = $array[$i]["user_id"];
            $integration_id = $array[$i]["intergration_id"];
            $txn_ref_no = $array[$i]["txn_reference_number"];
            $txn_amount = $array[$i]["txn_amount"];
            $txn_additional_info = $array[$i]["additional_info"];
            $txn_status = $array[$i]["txn_status"];
            $txn_date = $array[$i]["txn_date"];
            $txn_time = $array[$i]["txn_time"];
            $userInfo = $this->getUserInfoByUserID($user_id);
            $name = $userInfo[0]['firstname'] . " " . $userInfo[0]['surname'];
            $label_color = 'label-info';

            switch ($txn_status) {
                case 'complete':
                    $label_color = 'label-success';
                    break;
                case 'cancelled':
                    $label_color = 'label-danger';
                    break;
                default:
                    $label_color = 'label-info';
                    break;
            }


            echo '
                
                
<tr">
                    <td><a href=txn_info.php?txn_id=' . $txn_id . '><strong>Order' . $txn_id . '</strong></a></td>
                    <td class="txt-oflo">' . $name . '</td>
                    <td><span class="label ' . $label_color . ' label-rouded">' . $txn_status . '</span></td>
                    <td class="txt-oflo">' . $txn_additional_info . '</td>
                    <td>' . $txn_date . '</td>
                    <td><strong">$' . $txn_amount . '</strong></td>
                  </tr>
                      
                      
    ';
        }
    }

    function phpPopulate10PurchaseHistory($user_id)
    {


// Selecting Database
        $sql = "SELECT * FROM transactions where user_id = '" . $user_id . "' ORDER BY txn_date DESC, txn_time DESC LIMIT 10";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }


        for ($i = 0; $i < count($array); $i++) {

            $txn_id = $array[$i]["txn_id"];
            $user_id = $array[$i]["user_id"];
            $integration_id = $array[$i]["intergration_id"];
            $txn_ref_no = $array[$i]["txn_reference_number"];
            $txn_amount = $array[$i]["txn_amount"];
            $txn_additional_info = $array[$i]["additional_info"];
            $txn_status = $array[$i]["txn_status"];
            $txn_date = $array[$i]["txn_date"];
            $txn_time = $array[$i]["txn_time"];
            $userInfo = $this->getUserInfoByUserID($user_id);
            $name = $userInfo[0]['firstname'] . " " . $userInfo[0]['surname'];
            $label_color = 'label-info';

            switch ($txn_status) {
                case 'complete':
                    $label_color = 'label-success';
                    break;
                case 'cancelled':
                    $label_color = 'label-danger';
                    break;
                default:
                    $label_color = 'label-info';
                    break;
            }


            echo '
                
                
<tr">
                    <td><a href=txn_info.php?txn_id=' . $txn_id . '><strong>Order' . $txn_id . '</strong></a></td>
                    <td class="txt-oflo">' . $name . '</td>
                    <td><span class="label ' . $label_color . ' label-rouded">' . $txn_status . '</span></td>
                    <td class="txt-oflo">' . $txn_additional_info . '</td>
                    <td>' . $txn_date . '</td>
                    <td><strong">$' . $txn_amount . '</strong></td>
                  </tr>
                      
                      
    ';
        }
    }

    function getAllTxns($searchBy, $value)
    {

// Selecting Database
        $sql = "SELECT * FROM transactions WHERE " . $searchBy . " = '" . $value . "' ORDER BY txn_date DESC, txn_time DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getAllUsers($searchBy, $value)
    {

// Selecting Database
        $sql = "SELECT * FROM users WHERE " . $searchBy . " = '" . $value . "' AND account_status IN ('active', 'disabled') ORDER BY creation_date DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getUnactivatedUsers($searchBy, $value, $verified='verified')
    {

// Selecting Database
        $sql = "SELECT * FROM users WHERE " . $searchBy . " = '" . $value . "' AND privilege = 'user' AND verified = '$verified' ORDER BY creation_date DESC";
        $query = mysqli_query($this->link, $sql);
// set array
        $array = array();
        if (!$query) {
            die('Could not get data: ' . mysqli_error($this->link));
        }
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
// add each row returned into an array
            $array [$i] = $row;
            $i++;
// OR just echo the data:
//echo $row['thread_id'] . '</br>';; // etc
        }
        return $array;
    }

    function getNumberOfUsers()
    {

        $arr = $this->getAllUsers('1', '1');
        return sizeof($arr);
    }

    function getNumberOfActiveUsers()
    {

        $arr = $this->getAllUsers('privilege', 'user');
        return sizeof($arr);
    }

    function getNumberOfValidUsers()
    {

        $arr = $this->getAllUsers('privilege', 'user');
        return sizeof($arr);
    }

    function getNumberOfTxns()
    {

        $arr = $this->getAllTxns('1', '1');
        return sizeof($arr);
    }

    function getNumberOfUsersToActivate($searchBy, $value)
    {

        $arr = $this->getUnactivatedUsers($searchBy, $value);
        return sizeof($arr);
    }

    function printInvoiceTable($invoices) {
        foreach ($invoices as $invoice){

            switch ($invoice["state"]) {
                case 'complete':
                    $label_color = 'label-success';
                    break;
                case 'cancelled':
                    $label_color = 'label-danger';
                    break;
                case 'open':
                    $label_color = 'label-info';
                    break;
                default:
                    $label_color = 'label-info';
                    break;
            }

            echo('     
                <tr class = "clickable-row"  data-href = "invoice.php?inv_id=' . $invoice["id"] . '">
                  <td style="display:none;" >' . $invoice["id"] . '</td>
                  <td><a href=invoice.php?inv_id=' . $invoice["id"] . '><strong>' . $invoice["display_name"] . '</strong></a></td>  
                  <td><strong">$' . $invoice["amount_total_signed"] . '</strong></td>
                  <td><span class="label ' . $label_color . ' label-rouded">' . $invoice["state"] . '</span></td>
                  <td>' . $invoice["date_invoice"] . '</td>
                  <td>' . $invoice["reference"] . '</td>
                </tr>
');
        }
    }

    function printContractTable($contracts) {
        foreach ($contracts as $contract){

           /* switch ($contract["state"]) {
                case 'complete':
                    $label_color = 'label-success';
                    break;
                case 'cancelled':
                    $label_color = 'label-danger';
                    break;
                case 'open':
                    $label_color = 'label-info';
                    break;
                default:
                    $label_color = 'label-info';
                    break;
            }   */

            echo('     
                <tr class = "clickable-row"  data-href = "contract.php?inv_id=' . $contract["id"] . '">
                  <td style="display:none;" >' . $contract["id"] . '</td>
                  <td><a href=contract.php?inv_id=' . $contract["id"] . '><strong>' . $contract["display_name"] . '</strong></a></td>
                  <td>$' . $contract["balance"] . '</td>
                  <td>$' . $contract["debit"] . '</td> 
                  <td>$' . $contract["credit"] . '</td>
                  <td>' . $contract["recurring_next_date"] . '</td>
                  <td>' . $contract["code"] . '</td>
                </tr>
');
        }
    }

    function populatePurchasesOnFullPurchaseHistoryPage($user_id)
    {


        $infoArray = $this->getAllTxns('user_id', $user_id);


        for ($i = 0; $i < count($infoArray); $i++) {

            $txn_id = $infoArray[$i]["txn_id"];
            $user_id = $infoArray[$i]["user_id"];
            $integration_id = $infoArray[$i]["intergration_id"];
            $txn_ref_no = $infoArray[$i]["txn_reference_number"];
            $txn_amount = $infoArray[$i]["txn_amount"];
            $additional_info = $infoArray[$i]["additional_info"];
            $txn_status = $infoArray[$i]["txn_status"];
            $txn_date = $infoArray[$i]["txn_date"];
            $txn_time = $infoArray[$i]["txn_time"];
            $userInfo = $this->getUserInfoByUserID($user_id);
            $userBothNames = $userInfo[0]['firstname'] . ' ' . $userInfo[0]['surname'];


            switch ($txn_status) {
                case 'complete':
                    $label_color = 'label-success';
                    break;
                case 'cancelled':
                    $label_color = 'label-danger';
                    break;
                default:
                    $label_color = 'label-info';
                    break;
            }


            echo('     
                
                
                
<tr class = "clickable-row"  data-href = "txn_info.php?txn_id=' . $txn_id . '">
    <td style="display:none;" >' . $txn_id . '</td>
                  <td><a href=txn_info.php?txn_id=' . $txn_id . '><strong>Order' . $txn_id . '</strong></a></td>  
                  <td>' . $userBothNames . '</td>
                  <td>' . $txn_ref_no . '</td>
                  <td><strong">$' . $txn_amount . '</strong></td>
                  <td>' . $additional_info . '</td>
                  <td><span class="label ' . $label_color . ' label-rouded">' . $txn_status . '</span></td>
                  <td>' . $txn_date . '</td>
                  <td>' . $txn_time . '</td>
                      
                      
                </tr>
                    
                    
');
        }
    }

    function populateFaqsOnFaqsPage()
    {


        $infoArray = $this->getFAQs();


        for ($i = 0; $i < count($infoArray); $i++) {


            $faq_id = $infoArray[$i]["faq_id"];
            $faq_question = $infoArray[$i]["faq_question"];
            $faq_answer = $infoArray[$i]["faq_answer"];
            $status = $infoArray[$i]["status"];
            $uploader = $infoArray[$i]["uploader"];
            $date_uploaded = $infoArray[$i]["date_uploaded"];


            echo('     
                
                
                
                
                
                
                            <div class="panel panel-default">
                              <div class="panel-heading" role="tab" id="heading' . $faq_id . '">
                                <h4 class="panel-title"> <a class="collapsed font-bold" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' . $faq_id . '" aria-expanded="false" aria-controls="collapse' . $faq_id . '" > ' . $faq_question . ' </a> </h4>
                              </div>
                              <div id="collapse' . $faq_id . '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' . $faq_id . '">
                                <div class="panel-body"> ' . $faq_answer . '  </div>
                              </div>
                            </div>
                                
                                
                                
');
        }
    }

    function populateTxnsOnManageTxnsPage()
    {

        $infoArray = $this->getAllTxns('1', '1');


        for ($i = 0; $i < count($infoArray); $i++) {

            $txn_id = $infoArray[$i]["txn_id"];
            $user_id = $infoArray[$i]["user_id"];
            $integration_id = $infoArray[$i]["intergration_id"];
            $txn_ref_no = $infoArray[$i]["txn_reference_number"];
            $txn_amount = $infoArray[$i]["txn_amount"];
            $additional_info = $infoArray[$i]["additional_info"];
            $txn_status = $infoArray[$i]["txn_status"];
            $txn_date = $infoArray[$i]["txn_date"];
            $txn_time = $infoArray[$i]["txn_time"];
            $userInfo = $this->getUserInfoByUserID($user_id);
            $userBothNames = $userInfo[0]['firstname'] . ' ' . $userInfo[0]['surname'];


            echo('     
                
                
<tr class = "clickable-row"  data-href = "txn_info.php?txn_id=' . $txn_id . '">
    <td style="display:none;" >' . $txn_id . '</td>
                  <td> <a style="display: block; width: 25px;  height: 25px; margin: auto; opacity: 0.8;" href="txn_info.php?txn_id=' . $txn_id . '"><img width="25" height="25" src="../../plugins/images/view.png"></a> </td>
                  <td>' . $txn_id . '</td>
                  <td>' . $userBothNames . '</td>
                  <td>' . $txn_ref_no . '</td>
                  <td>' . $txn_amount . '</td>
                  <td>' . $additional_info . '</td>
                  <td>' . $txn_status . '</td>
                  <td>' . $txn_date . '</td>
                  <td>' . $txn_time . '</td>
                      
                      
                </tr>
                    
                    
');
        }
    }

    function populateUsersOnManageClientsPage()
    {

        $greenColor = 'rgba(0, 255, 0, 0.2)';
        $yellowColor = 'rgba(255, 255, 0, 0.3)';
        $redColor = 'rgba(255, 0, 0, 0.2)';
        $greyColor = 'rgba(0, 0, 0, 0.1)';


        $infoArray = $this->getAllUsers('privilege', 'user');


        for ($i = 0; $i < count($infoArray); $i++) {

            $user_id = $infoArray[$i]["user_id"];
            $firstname = $infoArray[$i]["firstname"];
            $surname = $infoArray[$i]["surname"];
            $bothnames = $firstname . ' ' . $surname;
            $username = $infoArray[$i]["username"];
            $password = $infoArray[$i]["password"];
            $phone = $infoArray[$i]["phone"];
            $email = $infoArray[$i]["email"];
            $address = $infoArray[$i]["address"];
            $city = $infoArray[$i]["city"];
            $country = $infoArray[$i]["country"];
            $privilege = $infoArray[$i]["privilege"];
            $last_login = $infoArray[$i]["last_login"];
            $creation_date = $infoArray[$i]["creation_date"];
            $verified = $infoArray[$i]["verified"];
            $account_status = $infoArray[$i]["account_status"];
            $profile_image_url = $infoArray[$i]["profile_image_url"];
            $ibs_id = $infoArray[$i]["ibs_id"];
            $cst_code = $infoArray[$i]["cst_code"];
            $hash = $infoArray[$i]["hash"];

            switch ($account_status) {
                case 'active':
                    $colorInUse = $greenColor;
                    break;
                case 'disabled':
                    $colorInUse = $yellowColor;
                    break;
                case 'deleted':
                    $colorInUse = $redColor;
                    break;
                default :
                    $colorInUse = $greyColor;
                    break;
            }


            echo('     
                
                
<tr class = "clickable-row" data-href = "client_info.php?user_id=' . $user_id . '">
    <td style="display:none;" >' . $user_id . '</td>
                  <td style="background: ' . $colorInUse . ';" ><a style="display: block; width: 25px;  height: 25px; margin: auto; opacity: 0.8;" href="client_info.php?user_id=' . $user_id . '"><img width="25" height="25" src="../../plugins/images/view.png"></a></td>
                  <td>' . $bothnames . '</td>
                  <td>' . $phone . '</td>
                  <td>' . $address . '</td>
                  <td>' . $city . '</td>
                  <td>' . $email . '</td>
                  <td>' . $cst_code . '</td>
                      
                      
                </tr>
                    
                    
');
        }
    }

    function populateDocumentsOnDocumentationPage()
    {


        $infoArray = $this->getDocuments();


        for ($i = 0; $i < count($infoArray); $i++) {


            $doc_id = $infoArray[$i]["doc_id"];
            $doc_name = $infoArray[$i]["doc_name"];
            $doc_description = $infoArray[$i]["doc_description"];
            $doc_path = $infoArray[$i]["doc_path"];
            $user_id = $infoArray[$i]["user_id"];
            $status = $infoArray[$i]["status"];
            $date_uploaded = $infoArray[$i]["date_uploaded"];
            $uploaded_by = $infoArray[$i]["uploaded_by"];
            $uploaderInfo = $this->getUserInfoByUserID($uploaded_by);
            $uploaderBothNames = $uploaderInfo[0]['firstname'] . ' ' . $uploaderInfo[0]['surname'];
            $uploaderImageUrl = $uploaderInfo[0]['profile_image_url'];
            $uploaderRole = $uploaderInfo[0]['privilege'];


            echo('     
          


<div class="col-md-4  col-lg-3 col-sm-6 col-xs-12">
                       <a href = "' . $doc_path . '" download = "' . $doc_name . '"><div class="bg-theme-dark m-b-15">
                            <div id="myCarouse2" class="carousel vcarousel slide p-20">
                                <!-- Carousel items -->
                                <div class="carousel-inner ">
                                    <div class="active item">
                                        <h3 class="text-white"><span class="font-bold">' . $doc_name . '</span><br>' . substr($doc_description, 0, 30) . '...</h3>
                                        <div class="twi-user"><img src="' . $uploaderImageUrl . '" class="img-circle img-responsive pull-left">
                                            <h4 class="text-white m-b-0">' . $uploaderBothNames . '</h4>
                                            <p class="text-white">' . $uploaderRole . '</p>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div></a>
                    </div>



');
        }
    }

    function populateDocOnManageDocsPage()
    {


        $infoArray = $this->getDocuments();


        for ($i = 0; $i < count($infoArray); $i++) {

            $doc_id = $infoArray[$i]["doc_id"];
            $doc_name = $infoArray[$i]["doc_name"];
            $doc_description = $infoArray[$i]["doc_description"];
            $doc_path = $infoArray[$i]["doc_path"];
            $user_id = $infoArray[$i]["user_id"];
            $status = $infoArray[$i]["status"];
            $date_uploaded = $infoArray[$i]["date_uploaded"];
            $uploaded_by = $infoArray[$i]["uploaded_by"];


            echo('     
                
                
<tr class = "clickable-row" download = "download" data-href = "' . $doc_path . '">
    <td style="display:none;" >' . $doc_id . '</td>
                  <td> <a style="display: block; width: 25px;  height: 25px; margin: auto; opacity: 0.8;" href="' . $doc_path . '" download><img width="25" height="25" src="../../plugins/images/view.png"></a> </td>
                  <td>' . $doc_name . '</td>
                  <td>' . $doc_description . '</td>
                  <td>' . $status . '</td>
                  <td>' . $date_uploaded . '</td>
                  <td>' . $uploaded_by . '</td>
                      
                      
                </tr>
                    
                    
');
        }
    }

    function populateFilesOnManageClientInfoPage($user_id)
    {


        $infoArray = $this->getFilesByUserId($user_id);


        for ($i = 0; $i < count($infoArray); $i++) {

            $file_id = $infoArray[$i]["file_id"];
            $user_id = $infoArray[$i]["user_id"];
            $file_name = $infoArray[$i]["file_name"];
            $file_description = $infoArray[$i]["file_description"];
            $file_path = $infoArray[$i]["file_path"];
            $status = $infoArray[$i]["status"];
            $upload_date = $infoArray[$i]["upload_date"];
            $uploader = $infoArray[$i]["uploader"];


            echo('     
                
                
<tr>
                                                                        <td align="center">' . $i . '</td>
                                                                        <td>' . $file_name . '</td>
                                                                         <td>' . $status . '</td>
                                                                        <td><a href = "' . $file_path . '" download = "' . $file_name . '" ><button class="btn btn-info">Download</button></a> <a href = "remove_user_file.php?user_id=' . $user_id . '&file_id=' . $file_id . '"><button class="btn btn-danger">Remove</button></a></td>
                                                                    </tr>
                    
                    
');
        }
    }

    function populateAdsOnManageAdsPage()
    {


        $infoArray = $this->getAds();


        for ($i = 0; $i < count($infoArray); $i++) {


            $ad_id = $infoArray[$i]["ad_id"];
            $ad_title = $infoArray[$i]["ad_title"];
            $ad_article = $infoArray[$i]["ad_article"];
            $status = $infoArray[$i]["status"];
            $media0 = $infoArray[$i]["media0"];
            $media1 = $infoArray[$i]["media1"];
            $media2 = $infoArray[$i]["media2"];
            $media3 = $infoArray[$i]["media3"];
            $date_uploaded = $infoArray[$i]["date_uploaded"];
            $uploader = $infoArray[$i]["uploader"];


            echo('     
                
                
<tr class = "clickable-row"  data-href = "ad_info.php?ad_id=' . $ad_id . '">
    <td style="display:none;" >' . $ad_id . '</td>
                  <td> <a style="display: block; width: 25px;  height: 25px; margin: auto; opacity: 0.8;" href="ad_info.php?ad_id=' . $ad_id . '"><img width="25" height="25" src="../../plugins/images/view.png"></a> </td>
                      
                  <td>' . $ad_title . '</td>
                  <td>' . $status . '</td>
                  <td>' . $date_uploaded . '</td>
                  <td>' . $uploader . '</td>
                      
                      
                </tr>
                    
                    
');
        }
    }

    function populateNewsOnSettingsPage()
    {

        $infoArray = $this->getAds();


        for ($i = 0; $i < count($infoArray); $i++) {


            $ad_id = $infoArray[$i]["ad_id"];
            $ad_title = $infoArray[$i]["ad_title"];
            $ad_article = $infoArray[$i]["ad_article"];
            $status = $infoArray[$i]["status"];
            $media0 = $infoArray[$i]["media0"];
            $media1 = $infoArray[$i]["media1"];
            $media2 = $infoArray[$i]["media2"];
            $media3 = $infoArray[$i]["media3"];
            $date_uploaded = $infoArray[$i]["date_uploaded"];
            $uploader = $infoArray[$i]["uploader"];

            $uploaderInfo = $this->getUserInfoByUserID($uploader);


            echo('     
                
                
                                        <div class="steamline">
                                            <div class="sl-item">
                                                <div class="sl-left"> <img src="' . $uploaderInfo[0]["profile_image_url"] . '" alt="user" class="img-circle"/> </div>
                                                <div class="sl-right">
                                                    <div class="m-l-40"><a href="#" class="text-info">' . $uploaderInfo[0]["firstname"] . ' ' . $uploaderInfo[0]["surname"] . '</a> <span  class="sl-date">' . $date_uploaded . '</span>
                                                        <p>' . $ad_article . '</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            
                                            
');
        }
    }

    function populateAdsOnAdsWidget()
    {

        $infoArray = $this->getAds();


        for ($i = 0; $i < count($infoArray); $i++) {


            $ad_id = $infoArray[$i]["ad_id"];
            $ad_title = $infoArray[$i]["ad_title"];
            $ad_article = $infoArray[$i]["ad_article"];
            $ad_category = $infoArray[$i]["ad_category"];
            $status = $infoArray[$i]["status"];
            $media0 = $infoArray[$i]["media0"];
            $media1 = $infoArray[$i]["media1"];
            $media2 = $infoArray[$i]["media2"];
            $media3 = $infoArray[$i]["media3"];
            $media4 = $infoArray[$i]["media4"];
            $date_uploaded = $infoArray[$i]["date_uploaded"];
            $uploader = $infoArray[$i]["uploader"];
            $coverImage = $media0;
            strlen($coverImage) < 1 ? $coverImage = $media1 : null;
            strlen($coverImage) < 1 ? $coverImage = $media2 : null;
            strlen($coverImage) < 1 ? $coverImage = $media3 : null;
            strlen($coverImage) < 1 ? $coverImage = $media4 : null;
            strlen($coverImage) < 1 ? $coverImage = 'https://selfservice.telco.co.zw/plugins/images/news/slide1.jpg' : null;
            $i == 0 ? $isActive = 'active' : $isActive = '';


            echo('     
                
                
                                    <div class="' . $isActive . ' item">
                                        <div class="overlaybg" style="max-width: 728px; height:90px;"><img src="' . $coverImage . '" width="728" height="90"></div>
                                        <div class="news-content" style="max-width: 728px; height:90px; padding: 0 10px;"><span class="label label-success label-rounded">' . $ad_category . '</span>
                                            <h6>' . substr($ad_article, 0, 140) . '</h6> 
                                            <a href="https://selfservice.telco.co.zw/portal/ads.php?ad_id=' . $ad_id . '">Read More</a></div>  
                                    </div>
                                        
                                        
');
        }
    }

    function populateFAQsOnManageFAQsPage()
    {


        $infoArray = $this->getFAQs();


        for ($i = 0; $i < count($infoArray); $i++) {


            $faq_id = $infoArray[$i]["faq_id"];
            $faq_answer = $infoArray[$i]["faq_answer"];
            $faq_question = $infoArray[$i]["faq_question"];
            $status = $infoArray[$i]["status"];
            $date_uploaded = $infoArray[$i]["date_uploaded"];
            $uploader = $infoArray[$i]["uploader"];


            echo('     
                
                
<tr class = "clickable-row"  data-href = "faq_info.php?faq_id=' . $faq_id . '">
    <td style="display:none;" >' . $faq_id . '</td>
                  <td> <a style="display: block; width: 25px;  height: 25px; margin: auto; opacity: 0.8;" href="faq_info.php?faq_id=' . $faq_id . '"><img width="25" height="25" src="../../plugins/images/view.png"></a> </td>
                  <td>' . $faq_question . '</td>
                  <td>' . $status . '</td>
                  <td>' . $date_uploaded . '</td>
                  <td>' . $uploader . '</td>
                      
                      
                </tr>
                    
                    
');
        }
    }

    function populateUsersToActivate()
    {

        $infoArray = $this->getUnactivatedUsers('1', '1');


        for ($i = 0; $i < count($infoArray); $i++) {

            $user_id = $infoArray[$i]["user_id"];
            $firstname = $infoArray[$i]["firstname"];
            $surname = $infoArray[$i]["surname"];
            $bothnames = $firstname . ' ' . $surname;
            $username = $infoArray[$i]["username"];
            $password = $infoArray[$i]["password"];
            $phone = $infoArray[$i]["phone"];
            $email = $infoArray[$i]["email"];
            $address = $infoArray[$i]["address"];
            $city = $infoArray[$i]["city"];
            $country = $infoArray[$i]["country"];
            $privilege = $infoArray[$i]["privilege"];
            $last_login = $infoArray[$i]["last_login"];
            $creation_date = $infoArray[$i]["creation_date"];
            $verified = $infoArray[$i]["verified"];
            $profile_image_url = $infoArray[$i]["profile_image_url"];
            $ibs_id = $infoArray[$i]["ibs_id"];
            $cst_code = $infoArray[$i]["cst_code"];
            $hash = $infoArray[$i]["hash"];


            echo('     
                
                
<tr class = "clickable-row"  data-href = "client_info.php?user_id=' . $user_id . '">
    <td style="display:none;" >' . $user_id . '</td>
                  <td> <a style="display: block; width: 25px;  height: 25px; margin: auto; opacity: 0.8;" href="client_info.php?user_id=' . $user_id . '"><img width="25" height="25" src="../../plugins/images/view.png"></a> </td>
                  <td>' . $bothnames . '</td>
                  <td>' . $phone . '</td>
                  <td>' . $address . '</td>
                  <td>' . $city . '</td>
                  <td>' . $email . '</td>
                  <td>' . $privilege . '</td>
                      
                      
                </tr>
                    
                    
');
        }
    }

    function populateUsersForSupportToActivate()
    {

        $infoArray = $this->getUnactivatedUsers('account_status', 'pending_support');


        for ($i = 0; $i < count($infoArray); $i++) {

            $user_id = $infoArray[$i]["user_id"];
            $firstname = $infoArray[$i]["firstname"];
            $surname = $infoArray[$i]["surname"];
            $bothnames = $firstname . ' ' . $surname;
            $username = $infoArray[$i]["username"];
            $password = $infoArray[$i]["password"];
            $phone = $infoArray[$i]["phone"];
            $email = $infoArray[$i]["email"];
            $address = $infoArray[$i]["address"];
            $city = $infoArray[$i]["city"];
            $country = $infoArray[$i]["country"];
            $privilege = $infoArray[$i]["privilege"];
            $last_login = $infoArray[$i]["last_login"];
            $creation_date = $infoArray[$i]["creation_date"];
            $verified = $infoArray[$i]["verified"];
            $profile_image_url = $infoArray[$i]["profile_image_url"];
            $ibs_id = $infoArray[$i]["ibs_id"];
            $cst_code = $infoArray[$i]["cst_code"];
            $hash = $infoArray[$i]["hash"];


            echo('     
                
                
<tr class = "clickable-row"  data-href = "client_info.php?user_id=' . $user_id . '">
    <td style="display:none;" >' . $user_id . '</td>
                  <td> <a style="display: block; width: 25px;  height: 25px; margin: auto; opacity: 0.8;" href="client_info.php?user_id=' . $user_id . '"><img width="25" height="25" src="../../plugins/images/view.png"></a> </td>
                  <td>' . $bothnames . '</td>
                  <td>' . $phone . '</td>
                  <td>' . $address . '</td>
                  <td>' . $city . '</td>
                  <td>' . $email . '</td>
                  <td>' . $privilege . '</td>
                      
                      
                </tr>
                    
                    
');
        }
    }

    function populateUsersForBillingToActivate()
    {

        $greenColor = 'rgba(0, 255, 0, 0.2)';
        $yellowColor = 'rgba(255, 255, 0, 0.3)';
        $redColor = 'rgba(255, 0, 0, 0.2)';
        $greyColor = 'rgba(0, 0, 0, 0.1)';
        $colorInUse = $yellowColor;


        $infoArray = $this->getUnactivatedUsers('account_status', 'inactive');


        for ($i = 0; $i < count($infoArray); $i++) {

            $user_id = $infoArray[$i]["user_id"];
            $firstname = $infoArray[$i]["firstname"];
            $surname = $infoArray[$i]["surname"];
            $bothnames = $firstname . ' ' . $surname;
            $username = $infoArray[$i]["username"];
            $password = $infoArray[$i]["password"];
            $phone = $infoArray[$i]["phone"];
            $email = $infoArray[$i]["email"];
            $address = $infoArray[$i]["address"];
            $city = $infoArray[$i]["city"];
            $country = $infoArray[$i]["country"];
            $privilege = $infoArray[$i]["privilege"];
            $last_login = $infoArray[$i]["last_login"];
            $creation_date = $infoArray[$i]["creation_date"];
            $verified = $infoArray[$i]["verified"];
            $profile_image_url = $infoArray[$i]["profile_image_url"];
            $ibs_id = $infoArray[$i]["ibs_id"];
            $cst_code = $infoArray[$i]["cst_code"];
            $hash = $infoArray[$i]["hash"];


            echo('     
                
                
<tr class = "clickable-row"  data-href = "client_info.php?user_id=' . $user_id . '">
    <td style="display:none;" >' . $user_id . '</td>
                  <td style="background: ' . $colorInUse . ';"> <a style="display: block; width: 25px;  height: 25px; margin: auto; opacity: 0.8;" href="client_info.php?user_id=' . $user_id . '"><img width="25" height="25" src="../../plugins/images/view.png"></a></td>
                  <td>' . $bothnames . '</td>
                  <td>' . $phone . '</td>
                  <td>' . $address . '</td>
                  <td>' . $city . '</td>
                  <td>' . $email . '</td>
                  <td>' . $creation_date . '</td>
                      
                      
                </tr>
                    
                    
');
        }
    }

    function phpPopulateMessagesOnHomePage($user_id)
    {

        $infoArray = $this->getMessagesByUserID($user_id);
        for ($i = 0; $i < count($infoArray); $i++) {
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $msg_id = $infoArray[$i]["msg_id"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $status = $infoArray[$i]["status"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];
            $ReadOrUnread = '';
            if (strcmp($status, 'unread') == 0) {
                $ReadOrUnread = 'online';
            }
            if ($i < 4) {
                echo('<a href="inbox-detail.php?message_id=' . $msg_id . '">
                <div class="user-img"> <img src="' . $sender_profile_image_url . '" alt="user" class="img-circle"> <span class="profile-status ' . $ReadOrUnread . ' pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>' . $sender_both_names . '</h5>
                  <span class="mail-desc">' . $msg_subject . '</span> <span class="time">' . $date_created . '</span> </div>
                </a>');
            }
        }
    }

    function phpPopulateMessagesOnInboxPage($user_id)
    {


        $infoArray = $this->getMessagesByUserID($user_id);
        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_category = $infoArray[$i]["msg_category"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            switch ($msg_category) {

                case 'sales':
                    $color = '#03a9f3';
                    break;
                case 'billing':
                    $color = '#fec107';
                    break;
                case 'support':
                    $color = '#9675ce';
                    break;
                case 'fault':
                    $color = '#fb9678';
                    break;
                case 'general':
                    $color = '#0F3B5F';
                    break;
            }

            echo('
                
                <tr class="' . $status . '">
                        <td><div class="checkbox m-t-0 m-b-0">
                            <input  type="checkbox">
                            <label for="checkbox0"></label>
                          </div></td>
                        <td class="hidden-xs"><i class="fa fa-star-o"></i></td>
                        <td class="hidden-xs">' . $sender_both_names . '</td>
                        <td class="max-texts"> <a href="inbox-detail.php?message_id=' . $msg_id . '" /><span class="label label-info m-r-10" style="background-color: ' . $color . ';" >' . $msg_category . '</span>' . $msg_subject . '</td>    <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                        <td class="text-right">' . $date_created . '</td>
                </tr>
                    
                ');
        }
    }

    function populateFaultsOnNewFaultsPage($user_id)
    {

        $infoArray = $this->getFaults($user_id);

        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_category = $infoArray[$i]["msg_category"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            switch ($msg_category) {

                case 'sales':
                    $color = '#03a9f3';
                    break;
                case 'billing':
                    $color = '#fec107';
                    break;
                case 'support':
                    $color = '#9675ce';
                    break;
                case 'fault':
                    $color = '#fb9678';
                    break;
                case 'general':
                    $color = '#0F3B5F';
                    break;
            }

            if ($status != "resolved") {

                echo('
                    
                <tr class="' . $status . '">
                        <td><div class="checkbox m-t-0 m-b-0">
                            <input  type="checkbox">
                            <label for="checkbox0"></label>
                          </div></td>
                        <td class="hidden-xs"><i class="fa fa-star-o"></i></td>
                        <td class="hidden-xs">' . $sender_both_names . '</td>
                        <td class="max-texts"> <a href="inbox-detail.php?message_id=' . $msg_id . '" /><span class="label label-info m-r-10" style="background-color: ' . $color . ';" >' . $msg_category . '</span>' . $msg_subject . '</td>    <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                        <td class="text-right">' . $date_created . '</td>
                </tr>
                    
            ');
            }
        }
    }

    function sizeOfUnresolvedFaults($user_id)
    {

        $infoArray = $this->getFaults($user_id);
        $counter = 0;
        for ($i = 0; $i < count($infoArray); $i++) {
            if ($infoArray[$i]["status"] != "resolved") {

                $counter++;
            }
        }

        return $counter;
    }

    function sizeOfResolvedFaults($user_id)
    {

        $infoArray = $this->getFaults($user_id);
        $countArray = 0;
        for ($i = 0; $i < count($infoArray); $i++) {
            if ($infoArray[$i]["status"] == "resolved") {

                $countArray++;
            }
        }

        return $countArray;
    }

    function populateFaultsOnResolvedFaultsPage($user_id)
    {


        $infoArray = $this->getFaults($user_id);

        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_category = $infoArray[$i]["msg_category"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            switch ($msg_category) {

                case 'sales':
                    $color = '#03a9f3';
                    break;
                case 'billing':
                    $color = '#fec107';
                    break;
                case 'support':
                    $color = '#9675ce';
                    break;
                case 'fault':
                    $color = '#fb9678';
                    break;
                case 'general':
                    $color = '#0F3B5F';
                    break;
            }

            if ($status == "resolved") {

                echo('
                    
                <tr class="' . $status . '">
                        <td><div class="checkbox m-t-0 m-b-0">
                            <input  type="checkbox">
                            <label for="checkbox0"></label>
                          </div></td>
                        <td class="hidden-xs"><i class="fa fa-star-o"></i></td>
                        <td class="hidden-xs">' . $sender_both_names . '</td>
                        <td class="max-texts"> <a href="inbox-detail.php?message_id=' . $msg_id . '" /><span class="label label-info m-r-10" style="background-color: ' . $color . ';" >' . $msg_category . '</span>' . $msg_subject . '</td>    <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                        <td class="text-right">' . $date_created . '</td>
                </tr>
                    
            ');
            }
        }
    }

    function populateMessagesWithTagOnInboxPage($tag, $user_id)
    {


        $infoArray = $this->getMessagesByTagAndUserID($tag, $user_id);
        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_category = $infoArray[$i]["msg_category"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            switch ($msg_category) {

                case 'sales':
                    $color = '#03a9f3';
                    break;
                case 'billing':
                    $color = '#fec107';
                    break;
                case 'support':
                    $color = '#9675ce';
                    break;
                case 'fault':
                    $color = '#fb9678';
                    break;
                case 'general':
                    $color = '#0F3B5F';
                    break;
            }

            echo('
                
                <tr class="' . $status . '">
                        <td><div class="checkbox m-t-0 m-b-0">
                            <input  type="checkbox">
                            <label for="checkbox0"></label>
                          </div></td>
                        <td class="hidden-xs"><i class="fa fa-star-o"></i></td>
                        <td class="hidden-xs">' . $sender_both_names . '</td>
                        <td class="max-texts"> <a href="inbox-detail.php?message_id=' . $msg_id . '" /><span class="label label-info m-r-10" style="background-color: ' . $color . ';" >' . $msg_category . '</span>' . $msg_subject . '</td>    <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                        <td class="text-right">' . $date_created . '</td>
                </tr>
                    
                ');
        }
    }

    function phpPopulateMessagesOnSentPage($user_id)
    {

        $infoArray = $this->getSentMessagesByUserID($user_id);

        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["receiver_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_category = $infoArray[$i]["msg_category"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            switch ($msg_category) {

                case 'sales':
                    $color = '#03a9f3';
                    break;
                case 'billing':
                    $color = '#fec107';
                    break;
                case 'support':
                    $color = '#9675ce';
                    break;
                case 'fault':
                    $color = '#fb9678';
                    break;
                case 'general':
                    $color = '#0F3B5F';
                    break;
            }

            echo('
                
                <tr class="' . $status . '">
                        <td><div class="checkbox m-t-0 m-b-0">
                            <input  type="checkbox">
                            <label for="checkbox0"></label>
                          </div></td>
                        <td class="hidden-xs"><i class="fa fa-star-o"></i></td>
                        <td class="hidden-xs">' . $sender_both_names . '</td>
                        <td class="max-texts"> <a href="inbox-detail.php?message_id=' . $msg_id . '" /><span class="label label-info m-r-10" style="background-color: ' . $color . ';" >' . $msg_category . '</span>' . $msg_subject . '</td>    <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                        <td class="text-right">' . $date_created . '</td>
                </tr>
                    
                ');
        }
    }

    function phpPopulateMessagesOnTrashPage($user_id)
    {


        $infoArray = $this->getTrashMessagesByUserID($user_id);
        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_category = $infoArray[$i]["msg_category"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            switch ($msg_category) {

                case 'sales':
                    $color = '#03a9f3';
                    break;
                case 'billing':
                    $color = '#fec107';
                    break;
                case 'support':
                    $color = '#9675ce';
                    break;
                case 'fault':
                    $color = '#fb9678';
                    break;
                case 'general':
                    $color = '#0F3B5F';
                    break;
            }

            echo('
                
                <tr class="' . $status . '">
                        <td><div class="checkbox m-t-0 m-b-0">
                            <input  type="checkbox">
                            <label for="checkbox0"></label>
                          </div></td>
                        <td class="hidden-xs"><i class="fa fa-star-o"></i></td>
                        <td class="hidden-xs">' . $sender_both_names . '</td>
                        <td class="max-texts"> <a href="inbox-detail.php?message_id=' . $msg_id . '" /><span class="label label-info m-r-10" style="background-color: ' . $color . ';" >' . $msg_category . '</span>' . $msg_subject . '</td>    <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                        <td class="text-right">' . $date_created . '</td>
                </tr>
                    
                ');
        }
    }

    function populateMessagesWithTagOnSentPage($tag, $user_id)
    {


        $infoArray = $this->getSentMessagesWithTagByUserID($tag, $user_id);

        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["receiver_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_category = $infoArray[$i]["msg_category"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];;

            switch ($msg_category) {

                case 'sales':
                    $color = '#03a9f3';
                    break;
                case 'billing':
                    $color = '#fec107';
                    break;
                case 'support':
                    $color = '#9675ce';
                    break;
                case 'fault':
                    $color = '#fb9678';
                    break;
                case 'general':
                    $color = '#0F3B5F';
                    break;
            }

            echo('
                
                <tr class="' . $status . '">
                        <td><div class="checkbox m-t-0 m-b-0">
                            <input  type="checkbox">
                            <label for="checkbox0"></label>
                          </div></td>
                        <td class="hidden-xs"><i class="fa fa-star-o"></i></td>
                        <td class="hidden-xs">' . $sender_both_names . '</td>
                        <td class="max-texts"> <a href="inbox-detail.php?message_id=' . $msg_id . '" /><span class="label label-info m-r-10" style="background-color: ' . $color . ';" >' . $msg_category . '</span>' . $msg_subject . '</td>    <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                        <td class="text-right">' . $date_created . '</td>
                </tr>
                    
                ');
        }
    }

    function populateMessagesWithTagOnTrashPage($tag, $user_id)
    {


        $infoArray = $this->getTrashMessagesWithTagByUserID($tag, $user_id);
        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_category = $infoArray[$i]["msg_category"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            switch ($msg_category) {

                case 'sales':
                    $color = '#03a9f3';
                    break;
                case 'billing':
                    $color = '#fec107';
                    break;
                case 'support':
                    $color = '#9675ce';
                    break;
                case 'fault':
                    $color = '#fb9678';
                    break;
                case 'general':
                    $color = '#0F3B5F';
                    break;
            }

            echo('
                
                <tr class="' . $status . '">
                        <td><div class="checkbox m-t-0 m-b-0">
                            <input  type="checkbox">
                            <label for="checkbox0"></label>
                          </div></td>
                        <td class="hidden-xs"><i class="fa fa-star-o"></i></td>
                        <td class="hidden-xs">' . $sender_both_names . '</td>
                        <td class="max-texts"> <a href="inbox-detail.php?message_id=' . $msg_id . '" /><span class="label label-info m-r-10" style="background-color: ' . $color . ';" >' . $msg_category . '</span>' . $msg_subject . '</td>    <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                        <td class="text-right">' . $date_created . '</td>
                </tr>
                    
                ');
        }
    }

    function populateDocumentsOnHomePage()
    {


        $infoArray = $this->getDocuments();

        for ($i = 0; $i < count($infoArray); $i++) {

            $doc_name = $infoArray[$i]["doc_name"];
            $doc_path = $infoArray[$i]["doc_path"];
            $date_uploaded = $infoArray[$i]["date_uploaded"];

            if ($i < 4) {

                echo('
                    
                    
      <li> <a href="' . $doc_path . '" download = "' . $doc_name . '">
              <div>
                <p> <strong>' . $doc_name . '</strong> <span class="pull-right text-muted">' . $date_uploaded . '</span> </p>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                </div>
              </div>
              </a> </li>
                  
            <li class="divider"></li>
                
                
      ');
            }
        }
    }

    function addMessagesOnSettingsPage($user_id)
    {


        $infoArray = $this->getMessagesByUserID($user_id);
        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_content = $infoArray[$i]["msg_content"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            echo('
                
                 <div class="sl-item">
                    <div class="sl-left"> <img src="' . $sender_profile_image_url . '" alt="user" class="img-circle"/> </div>
                    <div class="sl-right">
                      <div class="m-l-40"> <a href="#" class="text-info">' . $msg_subject . '</a> <span  class="sl-date">' . $date_created . '</span>
                        <div class="m-t-20 row">
                            
                   <!-- add attachments here
                       
                          <div class="col-md-2 col-xs-12"><img src="../plugins/images/img1.jpg" alt="user" class="img-responsive" /></div>
                              
                          -->
                              
                          <div class="col-md-9 col-xs-12">
                            <p>' . $msg_content . '</p>
                            <a href="inbox-detail.php?message_id=' . $msg_id . '" class="btn btn-success"> Reply</a></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr>
                      
                      
                ');
        }
    }

    function addMessagesOnSettingsPageByCriteria($searchBy, $value)
    {


        $infoArray = $this->getMessagesByUserIDByCriteria($searchBy, $value);
        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_content = $infoArray[$i]["msg_content"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            echo('
                
                 <div class="sl-item">
                    <div class="sl-left"> <img src="' . $sender_profile_image_url . '" alt="user" class="img-circle"/> </div>
                    <div class="sl-right">
                      <div class="m-l-40"> <a href="#" class="text-info">' . $msg_subject . '</a> <span  class="sl-date">' . $date_created . '</span>
                        <div class="m-t-20 row">
                            
                   <!-- add attachments here
                       
                          <div class="col-md-2 col-xs-12"><img src="../plugins/images/img1.jpg" alt="user" class="img-responsive" /></div>
                              
                          -->
                              
                          <div class="col-md-9 col-xs-12">
                            <p>' . $msg_content . '</p>
                            <a href="inbox-detail.php?message_id=' . $msg_id . '" class="btn btn-success"> Reply</a></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr>
                      
                      
                ');
        }
    }

    function addFaultsOnSettingsPageByCriteria($user_id, $searchBy, $value)
    {


        $infoArray = $this->getFaultsByUserIDByCriteria($user_id, $searchBy, $value);
        for ($i = 0; $i < count($infoArray); $i++) {
            $msg_id = $infoArray[$i]["msg_id"];
            $sender_user_id = $infoArray[$i]["sender_user_id"];
            $senderInfoArray = $this->getUserInfoByUserID($sender_user_id);
            $sender_firstname = $senderInfoArray[0]["firstname"];
            $sender_surname = $senderInfoArray[0]["surname"];
            $sender_profile_image_url = $senderInfoArray[0]["profile_image_url"];
            $sender_both_names = $sender_firstname . ' ' . $sender_surname;
            $status = $infoArray[$i]["status"];
            $msg_subject = $infoArray[$i]["msg_subject"];
            $msg_content = $infoArray[$i]["msg_content"];
            $date_created = $infoArray[$i]["date_created"];
            $time_created = $infoArray[$i]["time_created"];

            echo('
                
                 <div class="sl-item">
                    <div class="sl-left"> <img src="' . $sender_profile_image_url . '" alt="user" class="img-circle"/> </div>
                    <div class="sl-right">
                      <div class="m-l-40"> <a href="#" class="text-info">' . $msg_subject . '</a> <span  class="sl-date">' . $date_created . '</span>
                        <div class="m-t-20 row">
                            
                   <!-- add attachments here
                       
                          <div class="col-md-2 col-xs-12"><img src="../plugins/images/img1.jpg" alt="user" class="img-responsive" /></div>
                              
                          -->
                              
                          <div class="col-md-9 col-xs-12">
                            <p>' . $msg_content . '</p>
                            <a href="inbox-detail.php?message_id=' . $msg_id . '" class="btn btn-success"> Reply</a></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr>
                      
                      
                ');
        }
    }

    function populateIbsGroupsInDropdown()
    {
        $groups = $this->getIbsGroups();
        for ($i = 0; $i < count($groups); $i++) {
            echo "<option value = '" . $groups[$i]['ibs_group_name'] . "'>" . $groups[$i]['group_name'] . "</option>";
        }
    }

    function resetPassword($username, $password, $hash)
    {
        $username = mysqli_escape_string($this->link, $username); // Set email variable
        $hash = mysqli_escape_string($this->link, $hash); // Set hash variable

        mysqli_query($this->link, "UPDATE users SET login_attempts = 0, password = '" . md5($password) . "' WHERE username='" . $username . "' AND hash='" . $hash . "'");

        return mysqli_affected_rows($this->link) > 0 ? true : false;

    }

    function reEnableLogin($email, $hash)
    {
        $email = mysqli_escape_string($this->link, $email); // Set email variable
        $hash = mysqli_escape_string($this->link, $hash); // Set hash variable

        mysqli_query($this->link, "UPDATE users SET login_attempts = 0 WHERE email='" . $email . "' AND hash='" . $hash . "' AND login_attempts > 3 ");
        return mysqli_affected_rows($this->link) > 0 ? true : false;
    }

    function autoRenewCustomers()
    {
        $sql = "SELECT * FROM auto_renewals";

        $query = mysqli_query($this->link, $sql);

        while ($row = mysqli_fetch_assoc($query)) {

            $ibs_id = $row['ibs_id'];
            $user_id = $row['user_id'];


            //get user information from ibs
            $token = 't3lc0zss';
            $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';
            $data = array('token' => $token, 'user_id' => $ibs_id);
            $json = $this->CallAPI('GET', $service_address, $data);
            $ibsUserInfo = json_decode($json, true);


            //get the user's group info from ibs
            $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';
            $data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);
            $json = $this->CallAPI('GET', $service_address, $data);
            $ibsGroupInfo = json_decode($json, true);


            $credit = $ibsUserInfo['basic_info']['credit'];
            $deposit = $ibsUserInfo['basic_info']['deposit'];
            $creditToAdd = $ibsGroupInfo['group_info'][1]['raw_attrs']['group_credit'];


            //check that the account we are about to auto renew is a valid account
            $portalUserInfo = $this->getUserInfoByUserID($user_id);

            if ($portalUserInfo[0]['verified'] != 'verified') {
                $this->logger('user id: ' . $user_id . ' - account_not_verifed');
                continue;
            }
            if ($portalUserInfo[0]['account_status'] == 'inactive') {
                $this->logger('user id: ' . $user_id . ' - account_still_needs_billing_activation');
                continue;
            }
            if ($portalUserInfo[0]['account_status'] == 'pending_support') {
                $this->logger('user id: ' . $user_id . ' - account_still_needs_support_activation');
                continue;
            }
            if ($portalUserInfo[0]['account_status'] == 'deleted') {
                $this->logger('user id: ' . $user_id . ' - account_has_been_deleted');
                continue;
            }
            if ($portalUserInfo[0]['account_status'] == 'disabled') {
                $this->logger('user id: ' . $user_id . ' - account_has_been_disabled');
                continue;
            }
            if ($portalUserInfo[0]['account_status'] != 'active') {
                $this->logger('user id: ' . $user_id . ' - account_status_shows_an_unknown_error');
                continue;
            }
            if ($portalUserInfo[0]['ibs_id'] == '' || $portalUserInfo[0]['ibs_id'] == null) {
                $this->logger('user id: ' . $user_id . ' - account_does_not_have_an_ibs_id');
                continue;
            }


// check if their wallet balance is even enough

            if ($ibsUserInfo['basic_info']['deposit'] < $creditToAdd) {
                continue;
            }

            if ($credit != 0) {
                continue;
            }


// renew the package below here

            $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_renew_package.php';
            $data = array('token' => $token, 'user_ids' => $ibs_id, 'comment' => 'empty comment');
            $json = $this->CallAPI('POST', $service_address, $data);
            $response = json_decode($json, true);


            if ($response[0] != 'true') {
                $this->logger('user id: ' . $user_id . ' - auto_renew_failed_to_renew_package');
                exit();
            }


            //deduct amount used to renew the package
            $topup_type = "ADD";
            $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';
            $data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => -$creditToAdd, 'topup_type' => $topup_type, 'comment' => 'empty comment');
            $json = $this->CallAPI('POST', $service_address, $data);
            $reply = json_decode($json, true);


            if ($reply[0] != 'true') {
                $this->logger('user id: ' . $user_id . ' - auto_renew_failed_to_add_negative_deposit');
                exit();
            }

            $date = date('Y-m-d');
            $time = date('h:i:sa');


            $sql = "INSERT INTO transactions (user_id, txn_amount, additional_info, txn_status, txn_date, txn_time) VALUES ('" . $user_id . "','" . $creditToAdd . "', 'Credit Auto Renewal', 'complete','" . $date . "', '" . $time . "')";
            mysqli_query($this->link, $sql);
            $_id = mysqli_insert_id($this->link);
            $insertId = "AR: " . $_id;

            $sql = "UPDATE transactions SET txn_reference_number = '$insertId' WHERE txn_id = $_id";
            $bool = mysqli_query($this->link, $sql);

            if ($bool != true) {
                $this->logger('user id: ' . $user_id . ' - auto_renew_failed_to_save_renew_transaction_to_db');
                exit();
            }
        }

    }

    function verifyAccount($email, $hash)
    {

        $email = mysqli_escape_string($this->link, $email); // Set email variable
        $hash = mysqli_escape_string($this->link, $hash); // Set hash variable

        mysqli_query($this->link, "UPDATE users SET verified='verified', account_status = 'inactive' WHERE email='" . $email . "' AND hash='" . $hash . "' AND verified = 'unverified'");
        return mysqli_affected_rows($this->link) > 0 ? true : false;
    }

    function backFromPaynowUpdate($txn_id)
    {
        $sql = "UPDATE transactions SET txn_status = 'returned from paynow' WHERE txn_id = '" . $txn_id . "'";
        mysqli_query($this->link, $sql);
        return mysqli_affected_rows($this->link) > 0 ? true : false;
    }

}
