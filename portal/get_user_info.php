<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$searchBy = $_GET['search_criteria'];
$value = $_GET['value'];

include ("dbFunctions.php");
$operator = new DatabaseFunctionsClass();


switch ($searchBy) {

    case 'cst_code':
        $info = $operator->getUserInfoByCSTCode($value);
        break;
    case 'username':
        $info = $operator->getUserInfoByUsername($value);
        break;
    case 'phone':
        $info = $operator->getUserInfoByPhone($value);
        break;
    case 'email':
        $info = $operator->getUserInfoByEmail($value);
        break;
    default:
        $info = array();
        break;
}



echo json_encode($info, JSON_UNESCAPED_SLASHES);
