<!-- Top Navigation -->
<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
        <div class="top-left-part"><a class="logo" href="home.php"><b><img src="../../plugins/images/eliteadmin-logo.png" alt="home" /></b><span class="hidden-xs"><img src="../../plugins/images/eliteadmin-text.png" alt="home" /></span></a></div>
        <ul class="nav navbar-top-links navbar-left hidden-xs">
            <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
            <li>
                <!--   <form role="search" class="app-search hidden-xs">
                       <input type="text" placeholder="Search..." class="form-control">
                       <a href=""><i class="fa fa-search"></i></a>  

                   </form>   -->
            </li>
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>

                    <?php
                    $countMessages = $operator->phpGetNumberOfUnreadMessages($user_id);

                    if ($countMessages > 0) {

                        echo '<div class="notify"><span class="heartbit"></span><span class="point"></span></div>';
                    }
                    ?>

                </a>
                <ul class="dropdown-menu mailbox animated bounceInDown">
                    <li>
                        <div class="drop-title">You have <?php echo $operator->phpGetNumberOfUnreadMessages($user_id); ?> new messages</div>
                    </li>
                    <li>
                        <div class="message-center">

                            <!--add message rows here-->

                            <?php
                            $operator->phpPopulateMessagesOnHomePage($user_id);
                            ?>

                            <!--stop adding message row here-->



                        </div>
                    </li>
                    <li> <a class="text-center" href="inbox.php"> <strong>See all messages</strong> <i class="fa fa-angle-right"></i> </a></li>
                </ul>



                <!-- /.dropdown-messages ends here -->
            </li>
            <!-- /.dropdown documents starts here-->



            <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-doc"></i>
                 <!--   <div class="notify"><span class="heartbit"></span><span class="point"></span></div> -->
                </a>

                <ul class="dropdown-menu dropdown-tasks animated bounceInDown">


                    <!--   start adding documents rows here -->

                    <?php
                    $operator->populateDocumentsOnHomePage();
                    ?>

                    <!--   stop adding documents rows here -->


                    <li> <a class="text-center" href="documentation.php"> <strong>See All Documents</strong> <i class="fa fa-angle-right"></i> </a> </li>
                </ul>
                <!-- /.dropdown-tasks -->
            </li>
            <!-- /.dropdown documents ends here -->


            <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img id = "profileImageTopBar" src="../../plugins/images/users/default_profile_image.png" alt="user-img" width="36" class="img-circle"><b class="hidden-xs" id = "nameTopBar"><?php echo $name; ?></b> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li><a href="profile.php"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="inbox.php"><i class="ti-email"></i> Inbox</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="profile.php"><i class="ti-settings"></i> Account Setting</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="logoutredirect.php"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <li class="right-side-toggle"> <a class="waves-effect waves-light" href="profile.php"><i class="ti-settings"></i></a></li>
            <!-- /.dropdown -->
        </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>
<!-- End Top Navigation -->
<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <!--   <div class="input-group custom-search-form">
                       <input type="text" class="form-control" placeholder="Search...">
                       <span class="input-group-btn">
                           <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                       </span> </div>   -->
                <!-- /input-group -->
            </li>
            <li class="user-pro"> <a href="#" class="waves-effect"><img id = "profileImageSideBar" src="../../plugins/images/users/default_profile_image.png" alt="user-img"  class="img-circle"> <span class="hide-menu" id = "nameSideBar"><?php echo $name . " " . $surname; ?><span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="profile.php"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="inbox.php"><i class="ti-email"></i> Inbox</a></li>
                    <li><a href="profile.php"><i class="ti-settings"></i> Account Setting</a></li>
                    <li><a href="logoutredirect.php"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
            <li class="nav-small-cap m-t-10">--- Main Menu</li>
            <li> <a href="index.html" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right">5</span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="home.php">Home</a> </li>                          
                    <li> <a href="activate_accounts.php">Activate Accounts</a> </li>            
                    <li> <a href="manage_clients.php">Manage Clients</a> </li>

                </ul>
            </li>
            <li><a href="inbox.html" class="waves-effect"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Messaging <span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="compose.php">Compose</a></li>
                    <li> <a href="inbox.php">Inbox <span class="label label-rouded label-success pull-right"><?php echo $operator->phpGetNumberOfUnreadMessages($_SESSION['user_id']); ?></span> </a></li>
                    <li> <a href="sent.php">Sent</a></li>
                </ul>
            </li>


            <li><a href="logoutredirect.php" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
            <li class="nav-small-cap">--- Support</li>
            <li><a href="documentation.php" class="waves-effect"><i class="fa fa-circle-o text-danger"></i> <span class="hide-menu">Documentation</span></a></li>
            <li><a href="faqs.php" class="waves-effect"><i class="fa fa-circle-o text-success"></i> <span class="hide-menu">Faqs</span></a></li>
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->
