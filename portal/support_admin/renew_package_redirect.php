<?php

include '../dbFunctions.php';
$operator = new DatabaseFunctionsClass();

$token = 't3lc0zss';
$ibs_id = $_POST['ibsId'];
$su_user_id = $_POST['userId'];
$preserveCredit = $_POST['preserveCredit'];




//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);



//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);


$credit = $ibsUserInfo['basic_info']['credit'];
$deposit = $ibsUserInfo['basic_info']['deposit'];


// renew the package below here

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_renew_package.php';

$data = array('token' => $token, 'user_ids' => $ibs_id, 'comment' => 'empty comment');

$json = $operator->CallAPI('POST', $service_address, $data);

$response = json_decode($json, true);


if ($response[0] == 'true' && $preserveCredit == true) {


    $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';

    $data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => round($credit, 2), 'topup_type' => $topup_type, 'comment' => 'empty comment');

    $json = $operator->CallAPI('POST', $service_address, $data);

    $reply = json_decode($json, true);


    if ($reply[0] == 'true') {

        header('location: client_info.php?user_id=' . $su_user_id . '&notify=12');
    } else {

        header('location: client_info.php?user_id=' . $su_user_id . '&notify=56');
    }
} else {

    header('location: client_info.php?user_id=' . $su_user_id . '&notify=17');
}