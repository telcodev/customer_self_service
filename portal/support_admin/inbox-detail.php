<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//first check if the user is even allowed to view the message with the set id since we are sending the id as a GET variable

$messagedetails = $operator->getMessageDetails($_GET['message_id']);
$senderInfo = $operator->getUserInfoByUserID($messagedetails[0]['sender_user_id']);
$receiverInfo = $operator->getUserInfoByUserID($_SESSION['user_id']);


if ($messagedetails[0]['msg_category'] != 'fault')
    $operator->changeMessageStatus($messagedetails[0]['msg_id'], 'read');

//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}

// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];


// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}


//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);

//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);


//get user info from our db

$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];

//get info about the message from out portal DB
$portalMsgInfo = $operator->getMsgInfoByMsgID($_GET['message_id']);

$color = '#0F3B5F';
switch ($portalMsgInfo[0]['msg_category']) {

    case 'sales':
        $color = '#03a9f3';
        break;
    case 'billing':
        $color = '#fec107';
        break;
    case 'support':
        $color = '#9675ce';
        break;
    case 'fault':
        $color = '#fb9678';
        break;
    case 'general':
        $color = '#0F3B5F';
        break;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- morris CSS -->
        <link href="../../plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fix-sidebar fix-header">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <?php
            require './_nav.php';
            ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Inbox Detail</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="home.php">Home</a></li>
                                <li><a href="inbox.php">Inbox</a></li>
                                <li class="active">Inbox Detail</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- row -->
                    <div class="row">
                        <!-- Left sidebar -->
                        <div class="col-md-12">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3  col-sm-4 col-xs-12 inbox-panel">
                                        <div>
                                            <a href="compose.php" class="btn btn-custom btn-block waves-effect waves-light">Compose</a>
                                            <div class="list-group mail-list m-t-20">
                                                <a href="inbox.php" class="list-group-item active">Inbox <span class="label label-rouded label-success pull-right"><?php echo count($operator->getMessagesByUserID($_SESSION['user_id'])); ?></span></a>
                                                <a href="sent.php" class="list-group-item">Sent <span class="label label-rouded label-primary pull-right"><?php echo count($operator->getSentMessagesByUserID($_SESSION['user_id'])); ?></span></a>
                                                <a href="trash.php" class="list-group-item">Trash <span class="label label-rouded label-default pull-right"><?php echo count($operator->getTrashMessagesByUserID($_SESSION['user_id'])); ?></span></a>
                                            </div>
                                            <h3 class="panel-title m-t-40 m-b-0">Labels</h3>
                                            <hr class="m-t-5">
                                            <div class="list-group b-0 mail-list">   <a href="#" class="list-group-item"><span class="fa fa-circle text-success m-r-10" style="color: <?php echo $color; ?>;" ></span><?php echo $portalMsgInfo[0]['msg_category']; ?></a> </div>


                                            <?php
                                            $isResolved = "";

                                            $isResolved = $portalMsgInfo[0]['status'] == 'resolved' ? 'checked' : '';

                                            if ($portalMsgInfo[0]['msg_category'] == 'fault') {


                                                echo '<div class="checkbox checkbox-primary pull-left p-t-0">
                                                <input onclick="markFaultAsResolved(' . $portalMsgInfo[0]["msg_id"] . ')" id="checkbox-mark-resolved" type="checkbox" ' . $isResolved . ' >
                                                <label for="checkbox-mark-resolved">Mark as Resolved</label>
                                            </div>';
                                            }
                                            ?>


                                        </div>
                                    </div>

                                    <!-- this here is the actual message -->

                                    <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                                        <div class="media m-b-30 p-t-20">
                                            <h4 class="font-bold m-t-0"><?php echo $messagedetails[0]['msg_subject']; ?></h4>
                                            <hr>
                                            <a class="pull-left" href="#"> <img class="media-object thumb-sm img-circle" src="../../plugins/images/users/pawandeep.jpg" alt=""> </a>
                                            <div class="media-body"> <span class="media-meta pull-right"><?php echo $messagedetails[0]['date_created'] . " | <b>" . $messagedetails[0]['time_created'] . "</b>"; ?></span>
                                                <h4 class="text-danger m-0"><?php echo $senderInfo[0]['firstname'] . " " . $senderInfo[0]['surname'] ?></h4>
                                                <small class="text-muted">From: <a href="<?php echo 'client_info.php?user_id=' . $senderInfo[0]['user_id']; ?>"><?php echo $senderInfo[0]['username']; ?></small> </div>
                                        </div>

                                        <?php echo $messagedetails[0]['msg_content']; ?>

                                        <hr>




                                        <div class="b-all p-20">
                                            <p class="p-b-20">click here to <a href="compose.php?replyto=<?php echo $_GET['message_id']; ?>">Reply</a> <!-- or <a href="">Forward</a> --></p>
                                        </div>
                                    </div>

                                    <!-- this here is the actual message ending--> 

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <!-- .right-sidebar -->

                    <!-- /.right-sidebar -->
                </div>
                <!-- /.container-fluid -->
                 
                <?php
                    require './_notifyier.php'; require './_footer.php';
                    ?>
            </div>
            <!-- /#page-wrapper -->

            <!-- /#wrapper -->

            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

            <script type = "text/javascript" >

                function markFaultAsResolved(msg_id) {
                    var setorunset = document.getElementById('checkbox-mark-resolved').checked;
                    var msg_id = msg_id;

                    var change = null;

                    if (setorunset) {
                        change = 'MARK';
                    } else {
                        change = 'UNMARK';
                    }




                    $.ajax({//create an ajax request to load_page.php
                        type: "GET",
                        url: "https://selfservice.telco.co.zw/portal/support_admin/mark_fault_as_resolved_ajax.php",
                        data: {change: change, msg_id: msg_id},
                        dataType: "json", //expect html to be returned                
                        success: function (response) {

                            ress = JSON.stringify(response);
                            res = JSON.parse(ress);
                            result = res['result'];

                        }

                    });


                }

            </script>
        </div>
    </body>
</html>
