<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


$change = $_GET['change'];
$msg_id = $_GET['msg_id'];

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();


$reply = $operator->markMessageAsResolved($change, $msg_id);

echo json_encode(array('result' => $reply), JSON_UNESCAPED_SLASHES);
