<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}

// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];


// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}

//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);

//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);


//get tickets from otrs
$service_address = 'https://hotspot.openaccess.co.zw/otrs_api/csp_portal/get_closed_tickets_in_past_week.php';
$data = array();
$json = $operator->CallAPI('GET', $service_address, $data);
$tickets_info = json_decode($json, true);


//get user info from our db

$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- morris CSS -->
        <link href="../../plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <?php
            require './_nav.php';
            ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Inbox</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="home.php">Home</a></li>
                                <li class="active">Inbox</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- row -->
                    <div class="row">
                        <!-- Left sidebar -->
                        <div class="col-md-12">
                            <div class="white-box">
                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-2 col-md-3  col-sm-12 col-xs-12 inbox-panel">
                                        <div> <a href="compose.php" class="btn btn-custom btn-block waves-effect waves-light">Compose</a>
                                            <div class="list-group mail-list m-t-20"> <a href="inbox.php" class="list-group-item active">Inbox <span class="label label-rouded label-success pull-right">5</span></a>  <a href="sent.php" class="list-group-item">Sent</a> <a href="trash.php" class="list-group-item">Trash <span class="label label-rouded label-default pull-right">55</span></a> </div>
                                            <h3 class="panel-title m-t-40 m-b-0">Labels</h3>
                                            <hr class="m-t-5">
                                            <div class="list-group b-0 mail-list"> <a href="#" class="list-group-item"><span class="fa fa-circle text-info m-r-10"></span>Sales</a> <a href="#" class="list-group-item"><span class="fa fa-circle text-warning m-r-10"></span>Billing</a> <a href="#" class="list-group-item"><span class="fa fa-circle text-purple m-r-10"></span>Support</a> <a href="#" class="list-group-item"><span class="fa fa-circle text-danger m-r-10"></span>Faults</a> <a href="#" class="list-group-item"><span class="fa fa-circle text-success m-r-10"></span>General</a> </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 mail_listing">
                                        <div class="inbox-center">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th width="30"><div class="checkbox m-t-0 m-b-0 ">
                                                                <input id="checkbox0" type="checkbox" class="checkbox-toggle" value="check all">
                                                                <label for="checkbox0"></label>
                                                            </div></th>
                                                        <th colspan="4"> <div class="btn-group">
                                                                <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light m-r-5" data-toggle="dropdown" aria-expanded="false"> Filter <b class="caret"></b> </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="#fakelink">Read</a></li>
                                                                    <li><a href="#fakelink">Unread</a></li>
                                                                    <li><a href="#fakelink">Something else here</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#fakelink">Separated link</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-default waves-effect waves-light  dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-refresh"></i> </button>
                                                            </div></th>
                                                        <th class="hidden-xs" width="100"><div class="btn-group pull-right">
                                                                <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-left"></i></button>
                                                                <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-right"></i></button>
                                                            </div></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <!-- add messages rows here-->

                                                    <?php 
                                                    foreach ($tickets_info['payload'] as $ticket) {
                                                        
                                                        switch ($ticket["Type"]) {
                                                            case 'sales':
                                                                $color = '#03a9f3';
                                                                break;
                                                            case 'billing':
                                                                $color = '#fec107';
                                                                break;
                                                            case 'support':
                                                                $color = '#9675ce';
                                                                break;
                                                            case 'fault':
                                                                $color = '#fb9678';
                                                                break;
                                                            case 'general':
                                                                $color = '#0F3B5F';
                                                                break;
                                                        }

                                                        echo ('
                                                            <tr class="' . $ticket["StateType"] . '">
                                                                    <td><div class="checkbox m-t-0 m-b-0">
                                                                        <input  type="checkbox">
                                                                        <label for="checkbox0"></label>
                                                                      </div></td>
                                                                    <td class="hidden-xs"><i class="fa fa-star-o"></i></td>
                                                                    <td class="hidden-xs">' . substr($ticket["CustomerUserID"], 0, 6) . '</td>
                                                                    <td class="max-texts"> <a href="inbox-detail.php?ticket_id=' . $ticket["TicketID"] . '" /><span class="label label-info m-r-10" style="background-color: ' . $color . ';" >' . $ticket["Queue"] . '</span>' . $ticket["Title"] . '</td>    <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                                                                    <td class="text-right">' . $ticket["Created"] . '</td>
                                                            </tr>

                                                            ');
                                                    }
                                                    //$operator->populateFaultsOnResolvedFaultsPage($_SESSION['user_id']) 
                                                            
                                                            
                                                            ?>


                                                </tbody>
                                            </table>
                                        </div>
                                        <!--  <div class="row">
                                            <div class="col-xs-7 m-t-20"> Showing 1 - 15 of 200 </div>
                                            <div class="col-xs-5 m-t-20">
                                              <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-right"></i></button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>      -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                        <!-- .right-sidebar -->

                        <!-- /.right-sidebar -->
                    </div>
                    <!-- /.container-fluid -->
                     
                    <?php
                    require './_notifyier.php'; require './_footer.php';
                    ?>
                </div>
                <!-- /#page-wrapper -->


            </div>
            <!-- /#wrapper -->

            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

            <script type="text/javascript">
                //Alerts

                $(".myadmin-alert .closed").click(function (event) {
                    $(this).parents(".myadmin-alert").fadeToggle(350);

                    return false;
                });

                /* Click to close */

                $(".myadmin-alert-click").click(function (event) {
                    $(this).fadeToggle(350);

                    return false;
                });


            </script>
        </div>
    </body>
</html>
