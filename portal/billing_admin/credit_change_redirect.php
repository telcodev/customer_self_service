<?php

include '../dbFunctions.php';
$operator = new DatabaseFunctionsClass();

$token = 't3lc0zss';
$ibs_id = $_POST['ibsId'];
$su_user_id = $_POST['userId'];
$topup_type = $_POST['radio'];
$comment = $_POST['comment'];
$amount = round($_POST['amount'], 2);



switch ($topup_type) {

    case 'ADD':

        $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_credit_to_user.php';

        $data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => $amount, 'topup_type' => $topup_type, 'comment' => $comment);

        $json = $operator->CallAPI('POST', $service_address, $data);

        $result = json_decode($json, true);


        if ($result[0] == 'true') {

            header('location: client_info.php?user_id=' . $su_user_id . '&notify=58');
        } else {

            header('location: client_info.php?user_id=' . $su_user_id . '&notify=59');
        }


        break;






    case 'SET':


        //get user information from ibs

        $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

        $data = array('token' => $token, 'user_id' => $ibs_id);

        $json = $operator->CallAPI('GET', $service_address, $data);

        $ibsUserInfo = json_decode($json, true);

        $currentCredit = round($ibsUserInfo['basic_info']['credit'], 2);


        //remove all the current credit they do have 

        $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_credit_to_user.php';

        $data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => -$currentCredit, 'topup_type' => $topup_type, 'comment' => $comment);

        $json = $operator->CallAPI('POST', $service_address, $data);

        $result1 = json_decode($json, true);


        if ($result1[0] == true) {

            // add the credit now, its setting it, but a longer route to do it 

            $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_credit_to_user.php';

            $data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => round($amount, 2), 'topup_type' => $topup_type, 'comment' => $comment);

            $json = $operator->CallAPI('POST', $service_address, $data);

            $result = json_decode($json, true);



            if ($result[0] == 'true') {

                header('location: client_info.php?user_id=' . $su_user_id . '&notify=60');
            } else {

                header('location: client_info.php?user_id=' . $su_user_id . '&notify=61');
            }
        } else {

            header('location: client_info.php?user_id=' . $su_user_id . '&notify=68');
        }

        break;

    default:

        break;
}
