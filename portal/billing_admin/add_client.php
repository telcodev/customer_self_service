<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}






// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];


// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}




//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);







//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);







//get the further's group info from ibs in order to get the data left

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsDataInfo = json_decode($json, true);







//get user info from our db

$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];
?>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>  

    <body class="fix-sidebar">
        <!-- Preloader            -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php
            require './_nav.php';
            ?>

            <!-- Page Content -->
            <div id="page-wrapper">

                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Add New Client</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="home.php">Home</a></li>
                                <li class="active">Add New Client</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- page content begins here -->
                    <!--
                    <div class="row">
                        <form class="form-actions">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="display: block;" class="control-label">Link to an existing IBS Customer</label> <input style="display: inline-block; width: 60%;" type="number" id="ibs_link_id" name = "ibs_link_id" class="form-control" placeholder="eg: 4153">
                                    <button onclick="prePopulate()" style="display: inline-block; width: 80px; overflow: hidden;"  type="button"  class="btn btn-info" id = "pre-pop-btn">Link</button> 
                                    <button onclick="removePrePopulation()" style="display: inline-block; width: 80px; overflow: hidden;"  type="button"  class="btn btn-danger" id = "remove-pre-pop-btn" disabled="disabled">Unlink</button> 
                                </div><!--/span
                            </div>
                        </form>
                    </div>
                    -->
                    <div class="row">
                        <form class="form-actions">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="display: block;" class="control-label">Link to an existing Odoo Customer</label> <input style="display: inline-block; width: 60%;" type="text" id="odoo_cst" name = "odoo_cst" class="form-control" placeholder="eg: CST007">
                                    <button onclick="prePopulateOdoo()" style="display: inline-block; width: 80px; overflow: hidden;"  type="button"  class="btn btn-info" id = "odoo-pre-pop-btn">Link</button> 
                                    <button onclick="removeOdooPrePopulation()" style="display: inline-block; width: 80px; overflow: hidden;"  type="button"  class="btn btn-danger" id = "odoo-remove-pre-pop-btn" disabled="disabled">Unlink</button> 
                                </div><!--/span--> 
                            </div>
                        </form>
                    </div>


                    <div class="white-box">
                        <form id="loginform" action="addclientredirect.php" method="post" onsubmit="return validateRegisterForm();" name="loginform">
                            <div class="form-body">
                                <h3 class="box-title">Person Info</h3>
                                <hr> 
                                <input type="hidden" id="ibs_link_id_to_pass" name = "ibs_link_id_to_pass" class="form-control" value="">
                                <div class="row"> 
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" id="firstnameLabel">First Name</label> <input oninput="checkFirstnameEmpty();" type="text" id="firstname" name = "firstname" class="form-control" placeholder="eg: Tom" >
                                        </div><!--/span-->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" id="lastnameLabel">Last Name</label> <input oninput="checkLastnameEmpty();" type="text" id="lastname" name="lastname" class="form-control" placeholder="eg: Chibaya"  >
                                        </div>
                                    </div><!--/span-->
                                </div><!--/row-->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" id="usernameLabel">Username</label> <input oninput="checkUsernameEmpty(); checkUsername();" type="text" class="form-control" placeholder="eg: tom_chibaya" id="username" name="username" >
                                        </div>                                              
                                    </div><!--/span-->


                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Role</label> 
                                            <select class="form-control" name = "privilege" id="privilege">

                                                <option>
                                                    user
                                                </option>
                                                <option>
                                                    sales_admin
                                                </option>
                                                <option>
                                                    support_admin
                                                </option>
                                                <option>
                                                    billing_admin
                                                </option>

                                            </select>
                                        </div>

                                    </div>


                                </div><!--/row-->


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group" id="cst-container" style="display: none;">
                                            <label id="cstLabel" class="control-label">CST Code</label> <input oninput="checkCstCodeEmpty(); checkCstCode();" id="cst" type="text" id="cst_link_code" name = "cst_link_code" class="form-control" placeholder="eg: cst444">
                                        </div>
                                    </div><!--/span-->

                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group" id = "email_form_group">
                                            <label class="control-label" id="emailLabel">Email</label> <input oninput="checkEmailEmpty(); checkEmail();" type="text" class="form-control" placeholder="eg: tomchibaya@telco.co.zw" id = "email" name="email" >
                                        </div>
                                    </div><!--/span-->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" id="phoneLabel">Phone Number</label> <input oninput = 'checkPhoneEmpty(); checkPhone();' type="text" class="form-control" placeholder="eg: 0772000123" id = "phone" name="phone"  >
                                        </div>
                                    </div><!--/span-->
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group" id = "password_form_group">
                                            <label class="control-label" id="passwordLabel">Password</label> <input oninput="checkPwdsEmpty();" type="password" id="password" name = "password" class="form-control"  >
                                        </div>
                                    </div><!--/span-->

                                    <div class="col-md-6">
                                        <div class="form-group" id = "repeat_password_form_group">
                                            <label class="control-label" id="repeatPasswordLabel">Repeat Password</label> <input oninput="checkPwdsEmpty();" type="password" id="repeatpassword" name = "repeatpassword" class="form-control" >
                                        </div>
                                    </div><!--/span-->
                                </div><!--/row-->

                                <h3 class="box-title m-t-40">Address</h3>
                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label id="addressLabel">Address Line</label> <input oninput="checkAddressEmpty();" type="text" name = "address" id = "address"  class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label id="cityLabel">City</label> <input oninput="checkCityEmpty();" type="text" name = "city" id = "city" class="form-control" >
                                        </div>
                                    </div><!--/span-->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Country</label> <select class="form-control" name = "country" >

                                                <option>
                                                    Zimbabwe
                                                </option>

                                                <option>
                                                    South Africa
                                                </option>


                                            </select>
                                        </div>
                                    </div><!--/span-->
                                </div><!--/row-->

                                <hr>

                                <div class="form-actions">
                                    <button type="submit"  class="btn btn-default" id = "submitbtn"><i class="fa fa-check"></i> Save</button> 
                                    <span id="creating" style="display: none;" ><strong>  Creating your account ...   Please wait.</strong></span>

                                </div>

                                <hr/>


                            </div>
                        </form>
                    </div>





                    <!-- page content ends here -->
                    <!-- .right-side bar -->

                    <!-- /.right-side bar -->
                </div>
                <!-- /.container-fluid -->


                <footer class="footer text-center"> &copy Copyright TeleContract 2016 <

                    <?php
                    require './_notifyier.php';
                    require './_footer.php';
                    ?>


            </div>



            <!-- /#wrapper -->

            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <script src="../../plugins/bower_components/toast-master/js/jquery.toast.js"></script>
            <script src="../js/toastr.js"></script>


            <!-- Sweet-Alert  -->
            <script src="../../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
            <script src="../../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>


            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
            <script>

                            var beenSubmitted = false;

                            var ajaxEmailValid = false;
                            var ajaxPhoneValid = false;
                            var ajaxUsernameValid = false;
                            var ajaxCstValid = false;

                            function prePopulate() {
                                ibs_id = document.getElementById('ibs_link_id').value;
                                token = 't3lc0zss';
                                $.ajax({//create an ajax request to a page
                                    type: "GET",
                                    url: "https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php",
                                    data: {token: token, user_id: ibs_id},
                                    dataType: "json", //expect html to be returned                
                                    success: function (response) {

                                        ress = JSON.stringify(response);
                                        res = JSON.parse(ress);

                                        console.log(res);


                                        if (res['basic_info']['user_id'] !== "I" && res['basic_info']['user_id'] !== null) {
                                            document.getElementById('remove-pre-pop-btn').removeAttribute('disabled');
                                            document.getElementById('pre-pop-btn').setAttribute('disabled', 'disabled');
                                            document.getElementById('ibs_link_id').setAttribute('readonly', 'readonly');
                                            document.getElementById('ibs_link_id_to_pass').value = res['basic_info']['user_id'];
//                                            document.getElementById('phone').value = res['attrs']['cell_phone'];
//                                            document.getElementById('email').value = res['attrs']['email'];
                                            document.getElementById('privilege').value = 'user';
                                            document.getElementById('privilege').setAttribute('readonly', 'readonly');
                                            document.getElementById('cst-container').style.display = 'block';

                                        } else {
                                            document.getElementById('remove-pre-pop-btn').setAttribute('disabled', 'disabled');
                                            document.getElementById('ibs_link_id').value = '';
                                            document.getElementById('ibs_link_id').setAttribute('placeholder', 'Invalid IBS ID');
//                                            document.getElementById('privilege').removeAttribute('disabled');
//                                            document.getElementById('cst-container').style.display = 'none';

                                        }
                                    },
                                    error: function (response) {
                                        document.getElementById('remove-pre-pop-btn').setAttribute('disabled', 'disabled');
                                        document.getElementById('ibs_link_id').value = '';
                                        document.getElementById('ibs_link_id').setAttribute('placeholder', 'Invalid IBS ID');
//                                        document.getElementById('privilege').removeAttribute('disabled');
//                                        document.getElementById('cst-container').style.display = 'none';

                                    }

                                });
                            }

                            function removePrePopulation() {
                                document.getElementById('ibs_link_id').removeAttribute('readonly');
                                document.getElementById('pre-pop-btn').removeAttribute('disabled');
                                document.getElementById('remove-pre-pop-btn').setAttribute('disabled', 'disabled');
                                document.getElementById('ibs_link_id_to_pass').value = null;
                                document.getElementById('phone').value = null;
                                document.getElementById('email').value = null;
                                document.getElementById('cst-container').style.display = 'none';
                            }

                            function prePopulateOdoo() {
                                odoo_cst = document.getElementById('odoo_cst').value;
                                $.ajax({//create an ajax request to a page
                                    type: "GET",
                                    url: "https://selfservice.telco.co.zw/portal/getOdooUserInfo.php",
                                    data: {cst_code: odoo_cst},
                                    dataType: "json", //expect html to be returned                
                                    success: function (response) {

                                        ress = JSON.stringify(response);
                                        res = JSON.parse(ress);

                                        console.log(res);

                                        if (res['ref'] != null && res['ref'] != '') {

                                            var yes = confirm('Are you sure you want to link this customer to the Odoo Account: ' + res['display_name']);

                                            if (yes) {

                                                document.getElementById('firstname').value = res['display_name'].split(' ')[0];
                                                document.getElementById('lastname').value = res['display_name'].split(' ')[1];
                                                document.getElementById('username').value = res['display_name'].split(' ')[0].toLowerCase() + "_" + res['display_name'].split(' ')[1].toLowerCase();
                                                document.getElementById('email').value = res['email'];
                                                document.getElementById('privilege').value = 'user';
                                                document.getElementById('privilege').setAttribute('readonly', 'readonly');
                                                document.getElementById('odoo_cst').setAttribute('readonly', 'readonly');
                                                document.getElementById('phone').value = res['phone'];
                                                document.getElementById('address').value = res['street'];
                                                document.getElementById('city').value = res['city'];
                                                document.getElementById('odoo-pre-pop-btn').setAttribute('disabled', 'disabled');
                                                document.getElementById('odoo-remove-pre-pop-btn').removeAttribute('disabled');
                                                document.getElementById('cst-container').style.display = 'block';
                                                document.getElementById('cst').value = res['ref'];
                                                document.getElementById('cst').setAttribute('readonly', 'readonly');
                                            }

                                        } else {
//                                            document.getElementById('odoo-remove-pre-pop-btn').setAttribute('disabled', 'disabled');
//                                            document.getElementById('odoo_cst').value = '';
//                                            document.getElementById('odoo_cst').setAttribute('placeholder', 'eg: CST007');
//                                            document.getElementById('privilege').removeAttribute('disabled');
//                                            document.getElementById('odoo_cst').removeAttribute('disabled');
//                                            document.getElementById('cst-container').style.display = 'none';
//                                            document.getElementById('cst').value = '';
//                                            document.getElementById('cst').removeAttribute('disabled');
//                                            document.getElementById('odoo-pre-pop-btn').removeAttribute('disabled');
                                        }
                                    },
                                    error: function (response) {


                                    }

                                });
                            }

                            function removeOdooPrePopulation() {
                                document.getElementById('odoo-remove-pre-pop-btn').setAttribute('disabled', 'disabled');
                                document.getElementById('odoo_cst').value = '';
                                document.getElementById('odoo_cst').setAttribute('placeholder', 'eg: CST007');
                                document.getElementById('privilege').removeAttribute('readonly');
                                document.getElementById('cst-container').style.display = 'none';
                                document.getElementById('cst').value = '';
                                document.getElementById('cst').removeAttribute('readonly');
                                document.getElementById('odoo-pre-pop-btn').removeAttribute('disabled');
                                document.getElementById('odoo_cst').removeAttribute('readonly');
                            }



                            function checkFirstnameEmpty() {

                                !beenSubmitted ? document.getElementById('username').value = document.getElementById('firstname').value.toLowerCase().replace(/\s/g, '') + "_" + document.getElementById('lastname').value.toLowerCase().replace(/\s/g, '') : null;
                                document.getElementById('username').value.length >= 8 ? checkUsername() : null;

                                firstname = document.getElementById('firstname');
                                if (firstname.value === "" || firstname.value === undefined) {
                                    beenSubmitted ? setFieldToError('firstname', 'firstnameLabel', 'First Name - You cannot leave this empty.') : null;
                                    return false;
                                } else {
                                    setFieldToValid('firstname', 'firstnameLabel', 'First Name');
                                    return true;
                                }
                            }

                            function checkLastnameEmpty() {
                                !beenSubmitted ? document.getElementById('username').value = document.getElementById('firstname').value.toLowerCase().replace(/\s/g, '') + "_" + document.getElementById('lastname').value.toLowerCase().replace(/\s/g, '') : null;
                                document.getElementById('username').value.length >= 8 ? checkUsername() : null;

                                lastname = document.getElementById('lastname');
                                if (lastname.value === "" || lastname.value === undefined) {

                                    beenSubmitted ? setFieldToError('lastname', 'lastnameLabel', 'Last Name - You cannot leave this empty.') : null;
                                    return false;
                                } else {
                                    setFieldToValid('lastname', 'lastnameLabel', 'Last Name');
                                    return true;
                                }
                            }

                            function checkUsernameEmpty() {
                                username = document.getElementById('username');
                                if (username.value === "" || username.value === undefined || username.value.length < 8) {

                                    beenSubmitted ? setFieldToError('username', 'usernameLabel', 'Username - Must be 8 characters long or more.') : null;
                                    return false;
                                } else {
                                    ajaxUsernameValid ? setFieldToValid('username', 'usernameLabel', 'Username') : null;
                                    ;
                                    return true;
                                }


                            }

                            function checkEmailEmpty() {
                                email = document.getElementById('email');
                                if (email.value === "" || email.value === undefined) {
                                    beenSubmitted ? setFieldToError('email', 'emailLabel', 'Email - You cannot leave this empty.') : null;
                                    return false;
                                } else {
                                    ajaxEmailValid ? setFieldToValid('email', 'emailLabel', 'Email') : null;
                                    return true;
                                }
                            }

                            function checkCstCodeEmpty() {
                                cst = document.getElementById('cst');
                                if (cst.value === "" || cst.value === undefined) {

                                    beenSubmitted ? setFieldToError('cst', 'cstLabel', 'CST Code - You cannot leave this empty.') : null;
                                    return false;
                                } else {
                                    ajaxCstValid ? setFieldToValid('cst', 'cstLabel', 'CST Code') : null;
                                    return true;
                                }
                            }

                            function checkPhoneEmpty() {
                                phone = document.getElementById('phone');
                                if (phone.value === "" || phone.value === undefined) {

                                    beenSubmitted ? setFieldToError('phone', 'phoneLabel', 'Phone Number - You cannot leave this empty.') : null;
                                    return false;
                                } else {
                                    ajaxPhoneValid ? setFieldToValid('phone', 'phoneLabel', 'Phone Number') : null;
                                    return true;
                                }
                            }

                            function checkPwdsEmpty() {
                                pwd = document.getElementById('password');
                                rptPwd = document.getElementById('repeatpassword');
                                if (pwd.value === "" || pwd.value === undefined || pwd.value !== rptPwd.value || pwd.value.length < 8) {

                                    beenSubmitted ? setFieldToError('password', 'passwordLabel', 'Password - Ensure your passwords match and are at least 8 characters long.') : null;
                                    beenSubmitted ? setFieldToError('repeatpassword', 'repeatPasswordLabel', 'Repeat Password - Ensure your passwords match and are at least 8 characters long.') : null;
                                    return false;
                                } else {
                                    setFieldToValid('password', 'passwordLabel', 'Password');
                                    setFieldToValid('repeatpassword', 'repeatPasswordLabel', 'Repeat Password');
                                    return true;
                                }
                            }

                            function checkAddressEmpty() {
                                add = document.getElementById('address');
                                if (add.value === "" || add.value === undefined) {
                                    beenSubmitted ? setFieldToError('address', 'addressLabel', 'Address - Cannot be empty') : null;
                                    return false;
                                } else {
                                    setFieldToValid('address', 'addressLabel', 'Address');
                                    return true;
                                }
                            }

                            function checkCityEmpty() {
                                city = document.getElementById('city');
                                if (city.value === "" || city.value === undefined) {
                                    beenSubmitted ? setFieldToError('city', 'cityLabel', 'City - Cannot be empty') : null;
                                    return false;
                                } else {
                                    setFieldToValid('city', 'cityLabel', 'City');
                                    return true;
                                }
                            }

                            function checkUsername() {

                                setFieldToValid('username', 'usernameLabel', 'Username');

                                var val = document.getElementById('username').value.replace(/\s/g, '').replace(/\W/g, '').toLowerCase();
                                var res, ress = null;

                                document.getElementById('username').value = val;
                                document.getElementById('username').setAttribute('style', 'transition: none; background-image: url(../../plugins/images/loading.gif); background-position: 98%; background-size: 20px; background-repeat: no-repeat;')


                                $.ajax({//create an ajax request to a page
                                    type: "GET",
                                    url: "https://selfservice.telco.co.zw/portal/get_user_info.php",
                                    data: {search_criteria: 'username', value: val},
                                    dataType: "json", //expect html to be returned                
                                    success: function (response) {

                                        ress = JSON.stringify(response);
                                        res = JSON.parse(ress);


                                        if (res.length > 0) {
                                            setFieldToError('username', 'usernameLabel', 'Username (username already exists)');
                                            ajaxUsernameValid = false;
                                            document.getElementById('username').removeAttribute('style');
                                            return false;
                                        } else {
                                            setFieldToValid('username', 'usernameLabel', 'Username');
                                            ajaxUsernameValid = true;
                                            document.getElementById('username').removeAttribute('style');
                                            return true;
                                        }
                                    }
                                    ,
                                    error(e) {
                                        ajaxUsernameValid = false;
                                        document.getElementById('username').removeAttribute('style');
                                        return false;
                                    }

                                });
                            }

                            function checkPhone() {

                                setFieldToValid('phone', 'phoneLabel', 'Phone Number');

                                var val = document.getElementById('phone').value;
                                var res, ress, name = null;
                                document.getElementById('phone').setAttribute('style', 'transition: none; background-image: url(../../plugins/images/loading.gif); background-position: 98%; background-size: 20px; background-repeat: no-repeat;')


                                $.ajax({//create an ajax request to a page
                                    type: "GET",
                                    url: "https://selfservice.telco.co.zw/portal/get_user_info.php",
                                    data: {search_criteria: 'phone', value: val},
                                    dataType: "json", //expect json to be returned                
                                    success: function (response) {

                                        ress = JSON.stringify(response);
                                        res = JSON.parse(ress);

                                        if (res.length > 0) {

                                            setFieldToError('phone', 'phoneLabel', 'Phone Number (Phone Number already exists)');
                                            ajaxPhoneValid = false;
                                            document.getElementById('phone').removeAttribute('style');
                                            return false;

                                        } else {
                                            setFieldToValid('phone', 'phoneLabel', 'Phone Number');
                                            ajaxPhoneValid = true;
                                            document.getElementById('phone').removeAttribute('style');
                                            return true;
                                        }


                                    },
                                    error() {
                                        console.log('AJAX returned an error so we assume phone is invalid');
                                        ajaxPhoneValid = false;
                                        document.getElementById('phone').removeAttribute('style');
                                        return false;
                                    }

                                });
                            }

                            function checkCstCode() {

                                setFieldToValid('cst', 'cstLabel', 'CST Code');

                                var val = document.getElementById('cst').value;
                                var res, ress, name = null;
                                document.getElementById('cst').setAttribute('style', 'transition: none; background-image: url(../../plugins/images/loading.gif); background-position: 98%; background-size: 20px; background-repeat: no-repeat;')


                                $.ajax({//create an ajax request to a page
                                    type: "GET",
                                    url: "https://selfservice.telco.co.zw/portal/get_user_info.php",
                                    data: {search_criteria: 'cst_code', value: val},
                                    dataType: "json", //expect json to be returned                
                                    success: function (response) {

                                        ress = JSON.stringify(response);
                                        res = JSON.parse(ress);

                                        if (res.length > 0) {

                                            setFieldToError('cst', 'cstLabel', 'CST Code (This CST code already exists)');
                                            ajaxCstValid = false;
                                            document.getElementById('cst').removeAttribute('style');
                                            return false;

                                        } else {
                                            setFieldToValid('cst', 'cstLabel', 'CST Code');
                                            ajaxCstValid = true;
                                            document.getElementById('cst').removeAttribute('style');
                                            return true;
                                        }


                                    },
                                    error() {
                                        console.log('AJAX returned an error so we assume cst is invalid');
                                        ajaxCstValid = false;
                                        document.getElementById('cst').removeAttribute('style');
                                        return false;
                                    }

                                });
                            }

                            function checkEmail() {

                                setFieldToValid('email', 'emailLabel', 'Email');

                                var val = document.getElementById('email').value;
                                var res, ress = null;

                                document.getElementById('email').setAttribute('style', 'transition: none; background-image: url(../../plugins/images/loading.gif); background-position: 98%; background-size: 20px; background-repeat: no-repeat;')

                                var atpos = val.indexOf("@");
                                var dotpos = val.lastIndexOf(".");
                                if (beenSubmitted && (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= val.length)) {
                                    //email failed regex test
                                    setFieldToError('email', 'emailLabel', 'Email (this does not seem to be a valid email address)');
                                    ajaxEmailValid = false;
                                    return false;
                                }


                                $.ajax({//create an ajax request to a page
                                    type: "GET",
                                    url: "https://selfservice.telco.co.zw/portal/get_user_info.php",
                                    data: {search_criteria: 'email', value: val},
                                    dataType: "json", //expect html to be returned                
                                    success: function (response) {

                                        ress = JSON.stringify(response);
                                        res = JSON.parse(ress);
                                        console.log('email');
                                        console.log(res);

                                        if (res.length > 0) {
                                            //email already exists in our db
                                            setFieldToError('email', 'emailLabel', 'Email (email address already exists)');
                                            ajaxEmailValid = false;
                                            document.getElementById('email').removeAttribute('style');
                                            return false;
                                        } else {
                                            setFieldToValid('email', 'emailLabel', 'Email');
                                            ajaxEmailValid = true;
                                            document.getElementById('email').removeAttribute('style');
                                            return true;
                                        }
                                    },
                                    error(e) {
                                        console.log('AJAX returned an error so we assume email is invalid');
                                        ajaxEmailValid = false;
                                        document.getElementById('email').removeAttribute('style');
                                        return false;
                                    }
                                });
                            }

                            function validateRegisterForm() {

                                var submittable = true;
                                beenSubmitted = true;

                                ibs_link_id = document.getElementById('ibs_link_id_to_pass');
                                if (ibs_link_id.value !== null && ibs_link_id.value !== undefined && ibs_link_id.value !== "") {

                                    if (!ajaxCstValid) {
                                        submittable = false;
                                        setFieldToError('cst', 'cstLabel', 'CST Code (This CST code already exists)');
                                    }
                                }

                                checkEmail();
                                if (!ajaxEmailValid) {
                                    submittable = false;
                                    setFieldToError('email', 'emailLabel', 'Email (email address already exists)');
                                }
                                checkPhone();
                                if (!ajaxPhoneValid) {
                                    submittable = false;
                                    setFieldToError('phone', 'phoneLabel', 'Phone Number (Phone Number already exists)');
                                }
                                checkUsername();
                                if (!ajaxUsernameValid) {
                                    submittable = false;
                                    setFieldToError('username', 'usernameLabel', 'Username (username already exists)');
                                }

                                checkFirstnameEmpty() ? null : submittable = false;
                                checkLastnameEmpty() ? null : submittable = false;
                                checkUsernameEmpty() ? null : submittable = false;
                                checkEmailEmpty() ? null : submittable = false;
                                checkPhoneEmpty() ? null : submittable = false;
                                checkPwdsEmpty() ? null : submittable = false;
                                checkAddressEmpty() ? null : submittable = false;
                                checkCityEmpty() ? null : submittable = false;

                                if (ibs_link_id.value !== null && ibs_link_id.value !== undefined && ibs_link_id.value !== "") {
                                    checkCstCodeEmpty() ? null : submittable = false;
                                }



                                submittable ? document.getElementById('creating').setAttribute('style', 'display: inline-block;') : null;
                                return submittable;
                            }

                            function setFieldToError(name, labelName, labelErrorText) {

                                field = document.getElementById(name);
                                label = document.getElementById(labelName);

                                field.setAttribute('style', 'border-color: #ff0000;');
                                label.setAttribute('style', 'color: #ff0000;');
                                label.innerHTML = labelErrorText;

                            }

                            function setFieldToValid(name, labelName, labelValidText) {
                                field = document.getElementById(name);
                                label = document.getElementById(labelName);

                                field.setAttribute('style', 'border-color: #e4e7ea;');
                                label.setAttribute('style', 'color: #686868;');
                                label.innerHTML = labelValidText;
                            }

            </script>
        </div>
    </body>
</html>
