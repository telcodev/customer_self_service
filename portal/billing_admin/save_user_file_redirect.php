<?php

$target_dir = "../personal_files/";
$target_file = $target_dir . basename($_FILES["file"]["name"]);
$uploadOk = 1;
$fileType = pathinfo($target_file, PATHINFO_EXTENSION);
$name = preg_replace("/[^A-Z0-9._-]/i", "_", $_FILES["file"]["name"]);
$date = date('Y-m-d');
$user_id = $_POST['userId'];
$title = $_POST['filename'];


if (isset($title)) {
    // ensure a safe filename
    $title = preg_replace("/[^A-Z0-9._-]/i", "_", $title);
}

$uniqueName = hash("md5", date("Y-m-d") . date("h:i:sa") . $user_id . $title);


$_FILES['file']['name'] = $title;
$target_file = $target_dir . basename($uniqueName . '.' . $fileType);
$halfpath = substr($target_file, 2);
$absolutePath = 'https://selfservice.telco.co.zw/portal' . $halfpath;

if (file_exists($target_file)) {
    $uploadOk = 0;
}

if ($_FILES["file"]["size"] > 26214400) {
    $uploadOk = 0;
}



// Check if $uploadOk is set to 0 by an error
$uploadOk == 0 ? header('location: home.php?notify=70') : uploadFile();

function uploadFile() {
    global $target_file;
    move_uploaded_file($_FILES["file"]["tmp_name"], $target_file) ? saveFilePathToDB() : header('location: home.php?notify=71');
}

function saveFilePathToDB() {

    global $user_id, $title, $absolutePath, $date, $fileType;

    include ("../dbconnect.php");
// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "INSERT INTO files (user_id, file_name, file_description, file_path, status, upload_date, uploader) VALUES ('" . $user_id . "','" . $title . "', null ,'" . $absolutePath . "','new','$date','" . $_SESSION['user_id'] . "')";
    $bool = mysql_query($sql);

    $bool ? header('location: client_info.php?user_id=' . $user_id . '&notify=72') : header('location: home.php?notify=73' . mysql_error());
}
