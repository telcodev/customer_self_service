<?php

include ("../dbFunctions.php");
$operator = new DatabaseFunctionsClass();

if ($_POST['ibs_link_id_to_pass'] != null && $_POST['ibs_link_id_to_pass'] != "") {

    $bool = $operator->linkClientToIBS($_POST['firstname'], $_POST['lastname'], $_POST['username'], $_POST['privilege'], $_POST['cst_link_code'], $_POST['email'], $_POST['phone'], $_POST['password'], $_POST['address'], $_POST['city'], $_POST['country'], $_POST['ibs_link_id_to_pass']);
    $bool ? header('location: home.php?notify=29') : header('location: home.php?notify=31');
} else {

    $bool = $operator->addNewUser(null, $_POST['firstname'], $_POST['lastname'], $_POST['username'], $_POST['privilege'], $_POST['cst_link_code'], $_POST['email'], $_POST['phone'], $_POST['password'], $_POST['address'], $_POST['city'], $_POST['country'], 'home.php');
    $bool ? header('location: home.php?notify=30') : header('location: home.php?notify=32');
}
?>