<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

//perform reversal using API

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/txn_reversal.php';

$data = array('token' => $token, 'cst_code' => $_POST['cst_code'], 'topup_type' => 'reversal', 'telco_ref_id' => $_POST['txn_id']);

$response = $operator->CallAPI('POST', $service_address, $data);

$obj = json_decode($response, true);

//print_r($obj);

if ($obj['result'] == true) {

    header('location: https://selfservice.telco.co.zw/portal/billing_admin/home.php?notify=74');
} else {

    header('location: https://selfservice.telco.co.zw/portal/billing_admin/home.php?notify=75');
}

