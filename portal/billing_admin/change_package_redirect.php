<?php

include '../dbFunctions.php';
$operator = new DatabaseFunctionsClass();

$token = 't3lc0zss';
$ibs_id = $_POST['ibsId'];
$su_user_id = $_POST['userId'];
$preserveCredit = $_POST['preserveExistingCredit'];
$group = $_POST['group'];


//get user information from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('POST', $service_address, $data);

$ibsUserInfo = json_decode($json, true);

$currentCredit = round($ibsUserInfo['basic_info']['credit'], 2);


// add to wallet
If (isset($_POST['preserveExistingCredit'])) {


    $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_add_deposit_to_user.php';

    $data = array('token' => $token, 'user_id' => $ibs_id, 'credit' => round($currentCredit, 2), 'topup_type' => $topup_type, 'comment' => 'empty comment');

    $json = $operator->CallAPI('POST', $service_address, $data);

    $reply = json_decode($json, true);
}


//chnage the user group here now

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_change_user_group.php';

$data = array('token' => $token, 'user_id' => $ibs_id, 'group_name' => $group);

$json = $operator->CallAPI('POST', $service_address, $data);

$result1 = json_decode($json, true);


if ($result1['0'] == true) {

    header('location: client_info.php?user_id=' . $su_user_id . '&notify=66');
} else {

    header('location: client_info.php?user_id=' . $su_user_id . '&notify=67');
}

