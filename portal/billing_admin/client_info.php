<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();



//get variables from the sessions table in the database if the session exists

if (isset($_GET['session_id'])) {

// Selecting Database
    $db = mysql_select_db($dbname, $conn);
    $sql = "SELECT * FROM sessions where session_id = '" . $_GET['session_id'] . "' AND status = 'active'";
    $query = mysql_query($sql, $conn);

    if (!$query) {
        die('Could not get data: ' . mysql_error());
    }

    while ($row = mysql_fetch_assoc($query)) {

        $_SESSION['ibs_id'] = $row['ibs_id'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['session_id'] = $row['session_id'];
    }
    mysql_close($conn);
}


// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];

// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}


//get user information from ibs
$token = 't3lc0zss';
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';
$data = array('token' => $token, 'user_id' => $ibs_id);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsUserInfo = json_decode($json, true);

//get the user's group info from ibs
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';
$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsGroupInfo = json_decode($json, true);

//get the further's group info from ibs in order to get the data left
$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';
$data = array('token' => $token, 'user_id' => $ibs_id);
$json = $operator->CallAPI('GET', $service_address, $data);
$ibsDataInfo = json_decode($json, true);

//get user info from our db
$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$password = $portalUserInfo[0]["password"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$city = $portalUserInfo[0]["city"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];

//get all groups from our portal
$localGroups = $operator->getIbsGroups();


//get the ID and info of the selected user.
if (isset($_GET['user_id'])) {
    $su_user_id = $_GET['user_id'];
    $su_localInfo = $operator->getUserInfoByUserID($_GET['user_id']);

    // try to get odoo contract information
    $service_address = 'https://hotspot.openaccess.co.zw/odoo_api/cs_portal/get_customer_contracts.php';
    $data = array('cst_code' => $su_localInfo[0]["cst_code"]);
    $json = $operator->CallAPI('GET', $service_address, $data);
    $odooContractInfo = json_decode($json, true);

    // try to get odoo invoice information
    $service_address = 'https://hotspot.openaccess.co.zw/odoo_api/cs_portal/get_customer_invoices.php';
    $data = array('cst_code' => $su_localInfo[0]["cst_code"]);
    $json = $operator->CallAPI('GET', $service_address, $data);
    $odooInvoiceInfo = json_decode($json, true);

    // get all service mappings
    $all_invoice_lines = array();
    $invoice_ids = array();
    foreach ($odooContractInfo['payload'] as $contract){
        foreach ($contract['line_items'] as $item) {
            array_push($all_invoice_lines, $item);
            array_push($invoice_ids, $item['id']);
        }
    }
    foreach ($odooInvoiceInfo['payload'] as $invoice){
        foreach ($invoice['line_items'] as $item) {
            array_push($all_invoice_lines, $item);
            array_push($invoice_ids, $item['id']);
        }
    }


    //get user information from ibs
    $token = 't3lc0zss';
    $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';
    $data = array('token' => $token, 'user_id' => $su_localInfo[0]['ibs_id']);
    $json = $operator->CallAPI('GET', $service_address, $data);
    $su_info = json_decode($json, true);


    //get the user's group info from ibs
    $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';
    $data = array('token' => $token, 'group_name' => $su_info['basic_info']['group_name']);
    $json = $operator->CallAPI('GET', $service_address, $data);
    $su_groupinfo = json_decode($json, true);


    //get the further's group info from ibs in order to get the data left
    $service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';
    $data = array('token' => $token, 'user_id' => $su_localInfo[0]['ibs_id']);
    $json = $operator->CallAPI('GET', $service_address, $data);
    $su_datainfo = json_decode($json, true);
}




?>




<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">  
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!--alerts CSS -->
        <link href="../../plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fix-sidebar">
        <!-- Preloader            -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php
            require './_nav.php';
            ?>

            <!-- Page Content -->
            <div id="page-wrapper">

                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title"><?php echo $su_localInfo[0]['firstname'] . "'s Info"; ?></h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="home.php">Home</a></li>
                                <li><a href="manage_clients.php">Manage Clients</a></li>
                                <li class="active">Client Info</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <!-- page content begins here -->

                    <div class="row">


                        <div class="col-md-4 col-xs-12">
                            <div class="white-box">
                                <div class="user-bg"> <img width="100%" alt="user" src="<?php echo $su_localInfo[0]["profile_image_url"]; ?>">
                                    <div class="overlay-box">
                                        <div class="user-content"> <a href="javascript:void(0)"><img src="<?php echo $su_localInfo[0]["profile_image_url"]; ?>" class="thumb-lg img-circle" alt="img"></a>
                                            <h4 class="text-white">

                                                <?php
                                                echo $su_localInfo[0]["firstname"] . " " . $su_localInfo[0]["surname"];
                                                ?>

                                            </h4>
                                            <h5 class="text-white">

                                                <?php
                                                echo $su_localInfo[0]["email"];
                                                ?>

                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="user-btm-box">
                                    <div class="stats-row col-md-12 m-t-20 m-b-0 text-center">
                                        <div class="stat-item">
                                            <h6>Contact info</h6>
                                            <hr/>
                                            <b id = "phoneDashboard"><i class="ti-mobile"></i><?php echo $su_localInfo[0]["phone"]; ?></b></div>
                                        <hr>
                                        <div class="">
                                            <?php
                                            if ($su_localInfo[0]['account_status'] == 'inactive') {
                                                echo "<a data-toggle='modal' data-target='.supply-ip-modal' ><button class='btn btn-success'>Activate</button> </a>";
                                            }
                                            if ($su_localInfo[0]['account_status'] != 'deleted') {
                                                echo "<a href = delete_accounts_redirect.php?data=" . $_GET['user_id'] . "><button class='btn btn-danger'>Delete</button> </a>";
                                            }
                                            if ($su_localInfo[0]['account_status'] != 'disabled') {
                                                echo "<a href = disable_accounts_redirect.php?data=" . $_GET['user_id'] . "><button class='btn btn-warning'>Disable</button></a>";
                                            }
                                            if ($su_localInfo[0]['account_status'] == 'disabled') {
                                                echo "<a href = enable_accounts_redirect.php?data=" . $_GET['user_id'] . "><button class='btn btn-info'>Enable</button></a>";
                                            }
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- the modal to supply an IP address sits here -->
                            <div class="modal fade supply-ip-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-md">
                                    <div class="modal-width modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                            <h4 class="modal-title" id="mySmallModalLabel">Please supply additional information for the client's account</h4>
                                        </div>
                                        <div class="modal-body"> 
                                            <div class="row">
                                                <form class="form-actions">
                                                        <input type = 'hidden' name = 'userId' value="<?php echo $_GET['user_id']; ?>">
                                                        <div class="col-md-12">
                                                                <div class="form-group">
                                                                <!-- Contract Item Lines -->
                                                                <div class="row">
                                                               <div class="col-md-12">
                                                                  <h3 class="font-bold"><?php echo $contract['display_name']; ?></h3><br/><hr width="100%">
                                                                     <div class="table-responsive m-t-40">
                                                                        <table class="table table-hover">
                                                                           <thead>
                                                                              <tr>
                                                                                 <th class="text-center">Product</th>
                                                                                 <th>Description</th>
                                                                                 <th class="text-right">Quantity</th>
                                                                                 <th class="text-right">Sub Total</th>
                                                                              </tr>
                                                                            </thead>
                                                                             <tbody>
                                                                                 <?php
                                                                                         $i = 1;
                                                                                     $grand_total = 0;
                                                                                     foreach ($contract['line_items'] as $item) {

                                                                                             if(is_numeric($item['price_subtotal'])){
                                                                                             $grand_total += floatval($item['price_subtotal']);
                                                                                         }
                                                                                         echo "
                                                                                              <tr>
                                                                                                  <td class='text-center'>" . $item['product_id'][0] . "</td>
                                                                                                  <td>" . $item['product_id'][1] . "</td>
                                                                                                  <td class='text-right'>" . $item['quantity'] . " </td>
                                                                                                  <td class='text-right'> $" . $item['price_subtotal'] . " </td>
                                                                                                  <td class='text-right'>
                                                                                                          <input style='display: inline-block; width: 100px;' type='text' name = 'ibs_link_id' class='form-control link-ibs-id' placeholder='eg: 4153'>
                                                                                                  </td>
                                                                                                  <td class='text-right'>
                                                                                                      <button  style='display: inline-block; width: 40px; overflow: hidden;'  type='button'  class='btn btn-info link-button'><i class='fa fa-link' aria-hidden='true'></i></button> 
                                                                                                      <button style='display: inline-block; width: 40px; overflow: hidden;'  type='button'  class='btn btn-danger unlink-button' disabled='disabled'><i class='fa fa-unlink square' aria-hidden='true'></i></button> 
                                                                                                  </td>
                                                                                              </tr>
                                                                                              
                                                                                         ";
                                                                                         $i++;
                                                                                       }
                                                                                     ?>
                                                                                    </tbody>
                                                                                  </table>
                                                                                  <span class="button-align">
                                                                                      <button type='button' onclick="fetchIBSData(<?php echo $_GET['user_id']; ?>)" style="width: 150px;"  class='btn btn-info submit-button font-bold'><i class='fa fa-save' aria-hidden='true'> SAVE</i></button> 
                                                                                  </span>
                                                                     </div>
                                                                </div>
                                                             </div>                               
                                                         </div><!--/span--> 
                                                     </div>
                                              </form>
                                        </div>
                                        </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- the modal to supply IP address ends here -->
                            </div>

                            <div class="white-box">
                                <h3 class="box-title">Package</h3>
                                <div class="text-right"> <span class="text-muted"><?php
                                        if (strcmp($su_info['basic_info']['status'], 'Recharged') == 0)
                                            $additionalText = "(Out-of-Bundle)";
                                        if (strcmp($su_info['basic_info']['status'], 'Package') == 0)
                                            $additionalText = "(In-Bundle)";

                                        echo "<b>" . $su_info['basic_info']['group_name'] . "</b> " . $additionalText;
                                        ?></span>
                                    <h1><sup><i class="fa fa-money text-success"> </i></sup>$<?php
                                        if ($su_localInfo[0]['ibs_id'] != '') {
                                            echo round($su_info['basic_info']['credit'], 2);
                                        } else {
                                            echo '--';
                                        }
                                        ?></h1>
                                </div>
                                <span class="text-success"><?php
                                    if ($su_localInfo[0]['ibs_id'] != '') {
                                        //  echo $info['basic_info']['credit']/$groupinfo['group_info'][1]['raw_attrs']['group_credit']*100; 
                                        echo round($su_datainfo[1][$su_groupinfo['group_info'][1]['attrs']['charge']][1] / 1024 / 1024, 2); // to convert to megabytes; 
                                    } else {
                                        echo '--';
                                    }
                                    ?>MB remaining</span>  
                                <div class="progress m-b-0">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $su_info['basic_info']['credit'] / $su_groupinfo['group_info'][1]['raw_attrs']['group_credit'] * 100; ?>%;"> <span class="sr-only">20% Complete</span> </div>
                                </div> 

                                <hr>

                                <div class="checkbox checkbox-primary p-t-0">
                                    <input onclick="setAutoRenew(<?php echo "'" . $su_user_id . "'" . "," . "'" . $su_localInfo[0]['ibs_id'] . "'"; ?>);" id="checkbox-autorenew" type="checkbox" name = "autorenewcheckbox" <?php
                                    $arr = $operator->getAutoRenewUserByUserId($su_user_id);
                                    if (sizeof($arr) == 1) {
                                        echo 'checked = "checked"';
                                    }
                                    ?> > <label for="checkbox-autorenew"><a href="#">Auto Renew</a> on credit finish</label>
                                </div>

                                <hr/>           

                                <h3 class="box-title">Wallet Balance</h3>
                               <div class="text-right"> <!--  <span class="text-muted"><?php echo $su_info['basic_info']['group_name']; ?></span> -->
                                    <h1><sup><i class="fa fa-credit-card text-success"> </i></sup>$<?php
                                        if ($su_localInfo[0]['ibs_id'] != '') {
                                            echo round($su_info['basic_info']['deposit'], 2);
                                        } else {
                                            echo '--';
                                        }
                                        ?></h1>
                                </div>
                               <!-- <span class="text-success">20%</span> -->
                                <div class="progress m-b-0">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">20% Complete</span> </div>
                                </div>            
                                <hr>
                                <div class = "text-right"><span><a href = "" data-toggle="modal" data-target=".renew-package-modal" ><b>Renew Package</b></a></span> |
                                    <span><a href = "" data-toggle="modal" data-target=".change-package-modal">Change Package</a></span>
                                </div>
                                <hr>
                                <div class = "text-right"><span><a href = "" data-toggle="modal" data-target=".set-credit-modal" ><b>Set Credit Balance</b></a></span> |
                                    <span><a href = "" data-toggle="modal" data-target=".set-deposit-modal">Set Wallet Balance</a></span>
                                </div>
                            </div>


                        </div>

                        <!-- the modal to change package sits here -->
                        <div class="modal fade change-package-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                        <h4 class="modal-title" id="mySmallModalLabel">Please select the package you would like to switch to</h4>
                                    </div>
                                    <div class="modal-body"> 
                                        <form>
                                            <input type = 'hidden' name = 'userId' value="<?php echo $_GET['user_id']; ?>">
                                            <input type = 'hidden' name = 'ibsId' value="<?php echo $su_localInfo[0]["ibs_id"]; ?>">
                                            <div class="form-group">
                                                <label for="credit">Credit</label>
                                                <input class="form-control input-lg" id='credit' type = 'text' name = 'credit' placeholder="eg: 25.00" disabled="disabled" >
                                                <div class="checkbox checkbox-primary p-t-0 " > <input type="checkbox" onclick="disableCreditField()" id='useDefaultCredit' name="useDefaultCredit" checked="checked" ><label for="useDefaultCredit">Use Default Credit?</label></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="group">Group</label>
                                                <select name = 'group'class = 'form-control input-lg'>

                                                    <?php
                                                    for ($i = 0; $i < count($localGroups); $i++) {
                                                        echo ' 
                                                            <option value = "' . $localGroups[$i]["ibs_group_name"] . '">' . $localGroups[$i]["group_name"] . '</option>
                                                        ';
                                                    }
                                                    ?>
                                                </select>
                                                <div class="checkbox checkbox-primary p-t-0 " > <input type="checkbox"  id='preserveExistingCredit' name="preserveExistingCredit"  ><label for="preserveExistingCredit">Preserve Existing Credit?</label></div>
                                            </div>

                                            <button  type="submit" formaction="change_package_redirect.php" formmethod="post"  class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                            <button type="button" data-dismiss="modal" class="btn btn-inverse waves-effect waves-light">Cancel</button>

                                        </form>



                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- the modal to change package ends here -->

                        <!-- the modal to set renew package sits here -->
                        <div class="modal fade renew-package-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                        <h4 class="modal-title" id="mySmallModalLabel">Are you sure you want to perform this action?</h4>
                                    </div>
                                    <div class="modal-body"> 
                                        <form>
                                            <input type = 'hidden' name = 'ibsId' value="<?php echo $su_localInfo[0]["ibs_id"]; ?>">
                                            <input type = 'hidden' name = 'userId' value="<?php echo $_GET['user_id']; ?>">
                                            <div class="form-group">

                                                <div class="sa-icon sa-warning pulseWarning" style="display: block;">
                                                    <p style="color: #F34141; font-size: 800%; text-align: center;">!</p>
                                                </div>
                                            </div>  
                                            <div class = 'form-group'>
                                                <h2  style=" text-align: center;">Are you sure you ?</h2>
                                            </div>
                                            <div class="form-group">
                                                <p>You will not be able to reverse this action so make sure you want to renew this subscribers package</p>
                                            </div>
                                            <div class = 'form-group'>
                                                <div class="checkbox checkbox-primary p-t-0 ">
                                                    <input id="checkbox-preserve-credit" type="checkbox" name = 'preserveCredit' checked="checked" value = 'true'>
                                                    <label for="checkbox-preserve-credit"> Preserve Credit? </label>
                                                </div>  
                                            </div>

                                            <button  type="submit" formaction="renew_package_redirect.php" formmethod="post"  class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                            <button type="button" data-dismiss="modal" class="btn btn-inverse waves-effect waves-light">Cancel</button>

                                        </form>



                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- the modal to renew package modal ends here -->



                        <!-- the modal to set credit modal sits here -->
                        <div class="modal fade set-credit-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                        <h4 class="modal-title" id="mySmallModalLabel">How would you like to affect the Credit?</h4>
                                    </div>
                                    <div class="modal-body"> 



                                        <form>
                                            <input type = 'hidden' name = 'ibsId' value="<?php echo $su_localInfo[0]["ibs_id"]; ?>">
                                            <input type = 'hidden' name = 'userId' value="<?php echo $_GET['user_id']; ?>">
                                            <div class="form-group">
                                                <label for="amount">Amount</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-user"></i></div>
                                                    <input type="number" name ='amount' step="0.01" class="form-control" id="exampleInputuname" placeholder="eg: 25.30">
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <label for="comment">Comment</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-lock"></i></div>
                                                    <textarea name = 'comment' rows="3" class="form-control" placeholder="Enter an option comment that will follow the credit change"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Top Up Type</label>
                                                <div class="radio-list">
                                                    <label class="radio-inline p-0">
                                                        <div class="radio radio-info">
                                                            <input type="radio" name="radio" value="ADD" checked="checked">
                                                            <label for="radio1">Add</label>
                                                        </div>
                                                    </label>
                                                    <label class="radio-inline">
                                                        <div class="radio radio-info">
                                                            <input type="radio" name="radio" value="SET">
                                                            <label for="radio2">Set</label>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <button type="submit" formaction="credit_change_redirect.php" formmethod="post" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                            <button type="button" data-dismiss="modal" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                        </form>




                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- the modal to set credit modal ends here -->

                        <!-- the modal to set wallet modal sits here -->
                        <div class="modal fade set-deposit-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                        <h4 class="modal-title" id="mySmallModalLabel">How would you like to affect the Wallet Balance?</h4>
                                    </div>
                                    <div class="modal-body"> 



                                        <form>
                                            <input type = 'hidden' name = 'ibsId' value="<?php echo $su_localInfo[0]["ibs_id"]; ?>">
                                            <input type = 'hidden' name = 'userId' value="<?php echo $_GET['user_id']; ?>">
                                            <div class="form-group">
                                                <label for="amount">Amount</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-user"></i></div>
                                                    <input type="number" step="0.01" name ='amount' class="form-control" placeholder="eg: 25.30">
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <label for="comment">Comment</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-lock"></i></div>
                                                    <textarea rows="3" name = 'comment' class="form-control" id="exampleInputpwd2" placeholder="Enter an option comment that will follow the wallet change"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Top Up Type</label>
                                                <div class="radio-list">
                                                    <label class="radio-inline p-0">
                                                        <div class="radio radio-info">
                                                            <input type="radio" name="radio" id="radio1" value="ADD" checked="checked">
                                                            <label for="radio1">Add</label>
                                                        </div>
                                                    </label>
                                                    <label class="radio-inline">
                                                        <div class="radio radio-info">
                                                            <input type="radio" name="radio" id="radio2" value="SET" >
                                                            <label for="radio2">Set</label>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <button type="submit" formaction="deposit_change_redirect.php" formmethod="post"  class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                            <button type="button" data-dismiss="modal" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                        </form>




                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- the modal to set wallet modal ends here -->




                        <div class="col-md-8 col-xs-12">
                            <div class="white-box">
                                <ul class="nav nav-tabs tabs customtab">
                                    <li class="active tab"><a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Personal Documents</span> </a> </li>
                                    <li class="tab"><a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Purchases</span> </a> </li>
                                    <li class="tab"><a href="#messages" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Messages</span> </a> </li>
                                    <li class="tab"><a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Profile Information</span> </a> </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="home">



                                        <div class="steamline">  
                                            <div class="row">
                                                <form>
                                                    <input type="hidden" name ="userId" value="<?php echo $_GET['user_id'] ?>">
                                                    <div class="col-sm-12 ol-md-12 col-xs-12">
                                                        <div class="white-box">
                                                            <h3 class="box-title">New File</h3>                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-12 col-lg-12"> File Title </label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name = "filename" class="form-control" placeholder="eg: payment contract" required>
                                                                </div>
                                                            </div>
                                                            <hr> 
                                                            <div class="form-group">
                                                                <label class="col-sm-12 col-lg-12">File upload</label>
                                                                <div class="col-sm-12">
                                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                        <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                        <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                                            <input type="file" name="file" required>
                                                                        </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                                                    <button class="btn btn-success" type="submit" formenctype="multipart/form-data" formaction="save_user_file_redirect.php" formmethod="post">Save</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>   
                                            <br>
                                            <hr>


                                            <div class ="row">                                                
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">Existing documents</div>
                                                        <div class="panel-wrapper collapse in">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">#</th>
                                                                        <th> Title </th>
                                                                        <th> Status </th>
                                                                        <th> ... </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php $operator->populateFilesOnManageClientInfoPage($_GET['user_id']); ?>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>                               
                                        </div>


                                    </div>
                                    <div class="tab-pane" id="profile">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong> <br>
                                                <p class="text-muted"><?php echo $su_localInfo[0]['firstname'] . ' ' . $su_localInfo[0]['surname']; ?></p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong> <br>
                                                <p class="text-muted"><?php echo $su_localInfo[0]['phone']; ?></p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong> <br>
                                                <p class="text-muted"><?php echo $su_localInfo[0]['email']; ?></p>
                                            </div>
                                            <div class="col-md-3 col-xs-6"> <strong>CST Code</strong> <br>
                                                <p class="text-muted"><?php echo strtoupper($su_localInfo[0]['cst_code']); ?></p>
                                            </div>
                                        </div>
                                        <hr>

                                        <h4 class="font-bold m-t-30">Purchases</h4>





                                        <div class="white-box">

                                            <div class="row sales-report">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <h2><?php echo date('M Y'); ?></h2>
                                                    <p>PURCHASE HISTORY</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 ">
                                                    <h1 class="text-right text-success m-t-20">$<?php echo round($su_info['basic_info']['credit'], 2); ?></h1>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>NAME</th>
                                                            <th>STATUS</th>
                                                            <th>DATE</th>
                                                            <th>PRICE</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>


                                                        <!--Start adding purchase history rows here-->



                                                        <?php $operator->phpPopulate10PurchaseHistory($su_localInfo[0]['user_id']); ?>



                                                        <!--Stop adding purchase history rows here-->


                                                    </tbody>
                                                </table>
                                                <a href="full_purchase_history.php?user_id=<?php echo $su_localInfo[0]['user_id']; ?>" class = "showbottom">View all purchase history</a> </div>

                                        </div>






                                    </div>

                                    <div class="tab-pane" id="messages">

                                        <ul class="nav nav-tabs tabs customtab">
                                            <li class="active tab"><a href="#sentmessages" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Sent</span> </a> </li>
                                            <li class="tab"><a href="#receivedmessages" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Received</span> </a> </li>
                                        </ul>
                                        <div class="tab-content">


                                            <div class="tab-pane active" id="sentmessages">


                                                <div class="steamline">


                                                    <?php $operator->addMessagesOnSettingsPageByCriteria('sender_user_id', $su_localInfo[0]['user_id']); ?>

                                                </div>


                                            </div><div class="tab-pane" id="receivedmessages">

                                                <div class="steamline">


                                                    <?php $operator->addMessagesOnSettingsPageByCriteria('receiver_user_id', $su_localInfo[0]['user_id']); ?>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="settings">
                                        <form id="loginform" action="updateotherprofileredirect.php" method="post" name="loginform">
                                            <input type="hidden" name ='user_id' value="<?php echo $_GET['user_id']; ?>">
                                            <div class="form-body">
                                                <h3 class="box-title">Client's Info</h3>
                                                <hr>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">First Name</label> <input readonly type="text" id="firstname" name = "firstname" value ='<?php echo $su_localInfo[0]['firstname']; ?>' class="form-control" placeholder="Tom" required = "required">
                                                        </div><!--/span-->
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Last Name</label> <input readonly type="text" id="lastname" name="lastname" class="form-control" value ='<?php echo $su_localInfo[0]['surname']; ?>' placeholder="Chibaya"  required = "required">
                                                        </div>
                                                    </div><!--/span-->
                                                </div><!--/row-->

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Username</label> <input readonly type="text" class="form-control" placeholder="tom.chibaya" id = "username" value ='<?php echo $su_localInfo[0]['username']; ?>' name="username" required = "required">
                                                        </div>                                              
                                                    </div><!--/span-->


                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label>Role</label> <select disabled class="form-control" id='sRole' name = "privilege" required = "required">

                                                                <option>
                                                                    user
                                                                </option>
                                                                <option>
                                                                    sales_admin
                                                                </option>
                                                                <option>
                                                                    support_admin
                                                                </option>
                                                                <option>
                                                                    billing_admin
                                                                </option>

                                                            </select>
                                                        </div>

                                                    </div>


                                                </div><!--/row-->

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group" id = "email_form_group">
                                                            <label class="control-label">Email</label> <input readonly type="text" class="form-control" value ='<?php echo $su_localInfo[0]['email']; ?>' placeholder="tomchibaya@telco.co.zw" id = "email" name="email" required = "required">
                                                        </div>
                                                    </div><!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone Number</label> <input readonly type="text" class="form-control" value ='<?php echo $su_localInfo[0]['phone']; ?>' placeholder="077200123" id = "phone" name="phone"  required = "required">
                                                        </div>
                                                    </div><!--/span-->
                                                </div><!--/row-->

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group" id = "password_form_group">
                                                            <label class="control-label">Password</label> <input readonly type="password" id="password" value ='<?php echo $su_localInfo[0]['password']; ?>' name = "password" class="form-control"  required = "required">
                                                        </div>
                                                    </div><!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group" id = "repeat_password_form_group">
                                                            <label class="control-label">Repeat Password</label> <input type="password" id="repeatpassword" value ='<?php echo $su_localInfo[0]['password']; ?>' name = "repeatpassword" class="form-control" readonly>
                                                        </div>
                                                    </div><!--/span-->
                                                </div><!--/row-->

                                                <h3 class="box-title m-t-40">Address</h3>
                                                <hr>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Address Line</label> <input readonly type="text" name = "address" id = "address"  class="form-control" value ='<?php echo $su_localInfo[0]['address']; ?>' required = "required">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>City</label> <input readonly type="text" name = "city" id = "city" class="form-control" required = "required" value ='<?php echo $su_localInfo[0]['city']; ?>'>
                                                        </div>
                                                    </div><!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Country</label> <select disabled class="form-control" id='sCountry' name = "country" required = "required">

                                                                <option>
                                                                    Zimbabwe
                                                                </option>

                                                                <option>
                                                                    South Africa
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div><!--/span-->
                                                </div><!--/row-->





                                                <hr>

                                                <div class="form-actions">
                                                    <button type="button" onclick="openFields()"  class="btn btn-info" id = "editbtn"><i class="fa fa-pencil"></i> Edit</button> 
                                                    <button disabled type="submit"  class="btn btn-default" id = "submitbtn"><i class="fa fa-check"></i> Save</button> 
                                                </div>

                                                <hr/>


                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- page content ends here -->
                    <!-- .right-side bar -->

                    <!-- /.right-side bar -->
                </div>
                <!-- /.container-fluid -->




                <?php
                require './_notifyier.php';
                require './_footer.php';
                ?>


            </div>


            <!-- /#wrapper -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <script src="../../plugins/bower_components/toast-master/js/jquery.toast.js"></script>
            <script src="../js/toastr.js"></script>


            <script type="text/javascript">
                                                        document.getElementById('sCountry').value = '<?php echo $su_localInfo[0]['country']; ?>';
                                                        document.getElementById('sRole').value = '<?php echo $su_localInfo[0]['privilege']; ?>';
            </script>

            <!-- Sweet-Alert  -->
            <script src="../../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
            <script src="../../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js">
            </script>

            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
            <script type="text/javascript">


                                                        function checkCSTinOdoo() {
                                                            odoo_cst = document.getElementById('cstcode').value; 
                                                            $.ajax({//create an ajax request to a page
                                                                type: "GET",
                                                                url: "https://selfservice.telco.co.zw/portal/getOdooUserInfo.php",
                                                                data: {cst_code: odoo_cst},
                                                                dataType: "json", //expect html to be returned                
                                                                success: function (response) {
                                                                    ress = JSON.stringify(response);
                                                                    res = JSON.parse(ress);
                                                                    console.log(res);

                                                                    if (res['ref'] != null && res['ref'] != '') {

                                                                        var yes = confirm('Are you sure you want to link this customer to the Odoo Account: ' + res['display_name']);

                                                                        if (yes) {
                                                                            document.getElementById('create-ibs-form').submit();
                                                                        }

                                                                    } else {
                                                                        return false;
                                                                    }
                                                                },
                                                                error: function (response) {
                                                                    return false;

                                                                }

                                                            });
                                                        }

                                                        function fetchFromIBS(ibs_id, clicked_element) {
                           
                                                            token = 't3lc0zss';
                                                            $.ajax({//create an ajax request to a page
                                                                type: "GET",
                                                                async: false,
                                                                url: "https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php",
                                                                data: {token: token, user_id: ibs_id},
                                                                dataType: "json", //expect html to be returned                
                                                                success: function (response) {
                                                                    populateInvoiceLine(response, clicked_element);
                                                                },
                                                                error: function (response) {
                                                                    alert('Oops, something wrong, Try another ID');
                                                                }
                                                            });
                                                        }

                                                        function unlinkFromIBS() {
                                                            document.getElementById('pre-pop-btn').removeAttribute('disabled');
                                                            document.getElementById('ibs_link_id').value = '';
                                                            document.getElementById('ibs_link_id').setAttribute('placeholder', 'Invalid IBS ID');
                                                            document.getElementById('hide-these').style.display = 'block';
                                                            document.getElementById('hidden-ibs-id').value = 0;
                                                        }

                                                        function disableCreditField() {
                                                            if (document.getElementById('useDefaultCredit').checked) {
                                                                document.getElementById('credit').setAttribute('disabled', 'disabled');
                                                            } else {
                                                                document.getElementById('credit').removeAttribute('disabled');
                                                            }
                                                        }

                                                        function setAutoRenew(user_id, ibs_id) {
                                                            var setorunset = document.getElementById('checkbox-autorenew').checked;
                                                            var userid = user_id;
                                                            var ibsid = ibs_id;
                                                            var change = null;

                                                            if (setorunset) {
                                                                change = 'ADD';
                                                            } else {
                                                                change = 'REMOVE';
                                                            }

                                                            $.ajax({//create an ajax request to load_page.php
                                                                type: "GET",
                                                                url: "https://selfservice.telco.co.zw/portal/user/set_auto_renew.php",
                                                                data: {change: change, user_id: userid, ibs_id: ibsid},
                                                                dataType: "json", //expect html to be returned                
                                                                success: function (response) {

                                                                    ress = JSON.stringify(response);
                                                                    res = JSON.parse(ress);
                                                                    result = res['result'];

                                                                    console.log(ress);



                                                                }

                                                            });


                                                        }

                                                        function openFields() {

                                                            document.getElementById('firstname').removeAttribute("readonly");

                                                            document.getElementById('lastname').removeAttribute("readonly");

                                                            document.getElementById('sRole').removeAttribute("disabled");

                                                            document.getElementById('username').removeAttribute("readonly");

                                                            document.getElementById('password').removeAttribute("readonly");

                                                            document.getElementById('email').removeAttribute("readonly");

                                                            document.getElementById('phone').removeAttribute("readonly");

                                                            document.getElementById('city').removeAttribute("readonly");

                                                            document.getElementById('address').removeAttribute("readonly");

                                                            document.getElementById('sCountry').removeAttribute("disabled");

                                                            document.getElementById('submitbtn').removeAttribute('disabled');
                                                        }

                                                        function checkcst() {

                                                            if (document.getElementById('cstcode').value == "" || document.getElementById('cstcode') == undefined)
                                                                return false;

                                                            document.getElementById('cstcode').setAttribute('style', 'border-color: #e4e7ea;');
                                                            document.getElementById('cstLabel').setAttribute('style', 'color: #686868;');
                                                            document.getElementById('cstLabel').innerHTML = 'CST Code';

                                                            var val = document.getElementById('cstcode').value;
                                                            var res, ress, name = null;


                                                            $.ajax({//create an ajax request to a page
                                                                type: "GET",
                                                                url: "https://selfservice.telco.co.zw/portal/get_user_info.php",
                                                                data: {search_criteria: 'cst_code', value: val},
                                                                dataType: "json", //expect json to be returned                
                                                                success: function (response) {

                                                                    ress = JSON.stringify(response);
                                                                    res = JSON.parse(ress);

                                                                    if (res.length > 0) {

                                                                        document.getElementById('cstcode').setAttribute('style', 'border-color: #ff0000;');
                                                                        document.getElementById('cstLabel').setAttribute('style', 'color: #ff0000;');
                                                                        document.getElementById('cstLabel').innerHTML = 'CST Code (CST Code already exists)';

                                                                        return false;

                                                                    } else {
                                                                        document.getElementById('cstcode').setAttribute('style', 'border-color: #e4e7ea;');
                                                                        document.getElementById('cstLabel').setAttribute('style', 'color: #686868;');
                                                                        document.getElementById('cstLabel').innerHTML = 'CST Code';

                                                                        return checkOdooCst();
                                                                    }


                                                                }

                                                            });
                                                            return false;

                                                        }

                                                        function checkOdooCst() {
                                                            odoo_cst = document.getElementById('cstcode').value;
                                                            $.ajax({//create an ajax request to a page
                                                                type: "GET",
                                                                url: "https://selfservice.telco.co.zw/portal/getOdooUserInfo.php",
                                                                data: {cst_code: odoo_cst},
                                                                dataType: "json", //expect html to be returned                
                                                                success: function (response) {

                                                                    ress = JSON.stringify(response);
                                                                    res = JSON.parse(ress);


                                                                    if (res['ref'] != null && res['ref'] != '') {
                                                                        document.getElementById('cstcode').setAttribute('style', 'border-color: #ff0000;');
                                                                        document.getElementById('cstLabel').setAttribute('style', 'color: #ff0000;');
                                                                        document.getElementById('cstLabel').innerHTML = 'CST Code (CST Code already exists in odoo)';
                                                                        return true;

                                                                    } else {
                                                                        document.getElementById('cstcode').setAttribute('style', 'border-color: #e4e7ea;');
                                                                        document.getElementById('cstLabel').setAttribute('style', 'color: #686868;');
                                                                        document.getElementById('cstLabel').innerHTML = 'CST Code';
                                                                        return false;
                                                                    }
                                                                },
                                                                error: function (response) {
                                                                    document.getElementById('cstcode').setAttribute('style', 'border-color: #e4e7ea;');
                                                                    document.getElementById('cstLabel').setAttribute('style', 'color: #686868;');
                                                                    document.getElementById('cstLabel').innerHTML = 'CST Code';
                                                                    return true;
                                                                }
                                                            });
                                                        }

            </script>
            <!-- jQuery file upload -->
            <script src="../../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
            <script src="../js/jasny-bootstrap.js"></script>
            <script type="text/javascript">
                $('.link-button').click(function(){
                    var input_element =  $(this).parent().parent().find('input[type=text]');
                    var ibs_id = input_element.val();
                     if (isNaN(ibs_id)){
                        input_element.val("");
                        alert("Sorry, please enter numbers only in the ID field!");
                        return;
                   }
                   var ibs_result = fetchFromIBS(ibs_id, $(this));
                });
                
                function populateInvoiceLine(response, clicked_element) {
                    var username = response.attrs.normal_username;
                    var input_element =  clicked_element.parent().parent().find('input[type=text]');
                    var ibs_id = input_element.val();
                    if (!username || isNaN(response.basic_info.user_id)){
                        alert('Sorry, we couldn\'t find that ID in IBS');
                        input_element.val("");
                        return;
                    }
                     input_element.val(username);
                     $(input_element).prop( "disabled", true );
                     $(clicked_element).prop( "disabled", true);
                     var unlink_button = clicked_element.parent().parent().find('.unlink-button');
                     $(unlink_button).prop( "disabled", false );
 
                }
                
                $('.unlink-button').click(function(){
                     var input_element =  $(this).parent().parent().find('input[type=text]');
                     input_element.val("");
                     $(input_element).prop( "disabled", false );
                     $(this).prop( "disabled", true);
                     var link_button = $(this).parent().parent().find('.link-button');
                     $(link_button).prop( "disabled", false );
 
                });
                
                function fetchIBSData(ibs_id, invoice_id, user_id) {
                    token = 't3lc0zss';
                    $.ajax({
                        type: "GET",
                        url: "https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php",
                        data: {  
                                user_id:user_id,
                                invoice_id:<?php echo ($item['product_id'][0]); ?>,
                                ibs_id: $('.link-ibs-id').val(),
                         },
                        dataType: "json",
                        success: function(data){
                            console.log(user_id, invoice_id, ibs_id);
                        },
                     });
                }
            </script>
        </div>
    </body>
</html>
