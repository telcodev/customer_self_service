<?php
session_start();

include ("../dbFunctions.php");
include ("../dbconnect.php");
$operator = new DatabaseFunctionsClass();

// assign the seesion variables to local variables

$ibs_id = $_SESSION['ibs_id'];
$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];



// we need to log the person out if we realise the session is not longer active

if ($user_id == null) {
    header('location: logoutredirect.php');
    die();
}



//get user information from ibs

$token = 't3lc0zss';

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_user_info.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsUserInfo = json_decode($json, true);



//get the user's group info from ibs

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_group_info.php';

$data = array('token' => $token, 'group_name' => $ibsUserInfo['basic_info']['group_name']);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsGroupInfo = json_decode($json, true);



//get the further's group info from ibs in order to get the data left

$service_address = 'https://prov1.telco.co.zw/ibs_rest/api/test_get_remaining_data.php';

$data = array('token' => $token, 'user_id' => $ibs_id);

$json = $operator->CallAPI('GET', $service_address, $data);

$ibsDataInfo = json_decode($json, true);



//get user info from our db
$portalUserInfo = $operator->getUserInfoByUserID($user_id);
$name = $portalUserInfo[0]["firstname"];
$surname = $portalUserInfo[0]["surname"];
$username = $portalUserInfo[0]["username"];
$email = $portalUserInfo[0]["email"];
$phone = $portalUserInfo[0]["phone"];
$profileimageurl = $portalUserInfo[0]["profile_image_url"];
?>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Telco - Online Personal Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Morris CSS -->
        <link href="../../plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <link href="../css/colors/blue.css" id="theme"  rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>  

    <body class="fix-sidebar">
        <!-- Preloader            -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php
            require './_nav.php';
            ?>

            <!-- Page Content -->
            <div id="page-wrapper">

                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Home</h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="logoutredirect.php">Log Out</a></li>
                                <li class="active">Home Page</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- page content begins here -->


                    <div class="row">
                        <div class="col-sm-12">
                            <div class="white-box">
                                <h3 class="box-title">Line Chart</h3>
                                <div>
                                    <canvas id="chart1" height="100"></canvas>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="white-box">
                                <h3 class="box-title">Bar Chart</h3>
                                <div>
                                    <canvas id="chart2" height="100"></canvas>
                                </div>
                            </div>
                        </div>

                    </div>



                    <!-- page content ends here -->
                    <!-- .right-side bar -->

                    <!-- /.right-side bar -->
                </div>
                <!-- /.container-fluid -->


                 
                <?php
                    require './_notifyier.php'; require './_footer.php';
                    ?>

            </div>

            <!-- /#wrapper -->


            <!-- jQuery -->
            <script src="../../plugins/bower_components/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap Core JavaScript -->
            <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="../../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="../js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="../js/waves.js"></script>
            <script src="../../plugins/bower_components/toast-master/js/jquery.toast.js"></script>
            <script src="../js/toastr.js"></script>



            <!-- Sweet-Alert  -->
            <script src="../../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
            <script src="../../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
            <!-- Chart JS -->

            <script src="../../plugins/bower_components/Chart.js/Chart.min.js"></script>

            <script type="text/javascript" id="populating charts script">

                var incompleteTxns = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var completeTxns = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                var oReq = new XMLHttpRequest();
                oReq.open("get", "get_txns_ajax.php", true);
                oReq.send();
                oReq.onload = function () {
                    //This is where you handle what to do with the response.
                    //The actual data is found on this.responseText
                    var allTxnsJsonArray = this.responseText;
                    var jsonArrayOfTxns = JSON.parse(allTxnsJsonArray);

                    for (var i = 0; i < jsonArrayOfTxns.length; i++) {

                        var txnObj = jsonArrayOfTxns[i];
                        var objectYear = txnObj.txn_date.toString().substr(0, 4);
                        var objectMonth = parseInt(txnObj.txn_date.toString().substr(5, 2));
                        var todayYear = new Date().getFullYear();



                        if (objectYear == todayYear) {

                            txnObj.txn_status != 'complete' ? incompleteTxns[objectMonth - 1] += txnObj.txn_amount : completeTxns[objectMonth - 1] += txnObj.txn_amount;
                        }

                    }


                    var ctx1 = document.getElementById("chart1").getContext("2d");
                    var data1 = {
                        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        datasets: [
                            {
                                label: "Incomplete",
                                fillColor: "rgba(194,194,194,0.4)",
                                strokeColor: "rgba(194,194,194,0.8)",
                                pointColor: "rgba(194,194,194,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(152,235,239,1)",
                                data: incompleteTxns
                            },
                            {
                                label: "Resolved",
                                fillColor: "rgba(152,235,239,0.4)",
                                strokeColor: "rgba(152,235,239,0.8)",
                                pointColor: "rgba(152,235,239,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(152,235,239,1)",
                                data: completeTxns
                            }

                        ]
                    };
                    var chart1 = new Chart(ctx1).Line(data1, {
                        scaleShowGridLines: true,
                        scaleGridLineColor: "rgba(0,0,0,.005)",
                        scaleGridLineWidth: 0,
                        scaleShowHorizontalLines: true,
                        scaleShowVerticalLines: true,
                        bezierCurve: true,
                        bezierCurveTension: 0.4,
                        pointDot: true,
                        pointDotRadius: 4,
                        pointDotStrokeWidth: 1,
                        pointHitDetectionRadius: 2,
                        datasetStroke: true,
                        tooltipCornerRadius: 2,
                        datasetStrokeWidth: 2,
                        datasetFill: true,
                        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                        responsive: true
                    });


                    var ctx2 = document.getElementById("chart2").getContext("2d");
                    var data2 = {
                        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        datasets: [
                            {
                                label: "Unresolved",
                                fillColor: "rgba(194,194,194,0.4)",
                                strokeColor: "rgba(194,194,194,0.8)",
                                highlightFill: "rgba(252,201,186,0.4)",
                                highlightStroke: "rgba(252,201,186,0.4)",
                                data: incompleteTxns
                            },
                            {
                                label: "Resolved",
                                fillColor: "rgba(152,235,239,0.4)",
                                strokeColor: "rgba(152,235,239,0.8)",
                                highlightFill: "rgba(252,201,186,0.4)",
                                highlightStroke: "rgba(252,201,186,0.4)",
                                data: completeTxns
                            }
                        ]
                    };
                    var chart2 = new Chart(ctx2).Bar(data2, {
                        scaleBeginAtZero: true,
                        scaleShowGridLines: true,
                        scaleGridLineColor: "rgba(0,0,0,.005)",
                        scaleGridLineWidth: 0,
                        scaleShowHorizontalLines: true,
                        scaleShowVerticalLines: true,
                        barShowStroke: true,
                        barStrokeWidth: 0,
                        tooltipCornerRadius: 2,
                        barDatasetSpacing: 3,
                        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                        responsive: true
                    });


                };
            </script>

            <!-- Custom Theme JavaScript -->
            <script src="../js/custom.js"></script>
            <!--Style Switcher -->
            <script src="../../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

        </div>
    </body>
</html>
