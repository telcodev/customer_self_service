<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include ("../dbFunctions.php");
include ("../dbconnect.php");

$operator = new DatabaseFunctionsClass();

$allFaults = $operator->getTxns();

echo json_encode($allFaults);
